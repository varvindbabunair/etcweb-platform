<div class="content-wrapper">

    <!-- Stunning Header -->

    <div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
        <div class="container">
            <div class="stunning-header-content">
                <div class="inline-items">
                    <h4 class="stunning-header-title">Registration Successful</h4>
                </div>
                <div class="breadcrumbs-wrap inline-items">
                    <a href="#" class="btn btn--primary btn--round">
                        <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                    </a>

                </div>
            </div>
        </div>
    </div>

    <!-- ... end Stunning Header -->


    <!-- Buttons -->

    <div class="container" style="margin-bottom: 75px">
        <div class="row" style="margin-bottom: 120px">
            <div class="col-lg-12" style="text-align: center;">
                <h4>Verify email address to start using etcweb.entranceengine.com</h4>
                <p>The registration of your institute is completed and successful. However, to start using <strong>ETCWEB</strong> platform and train your students, you will have to verify your email address first.</p>
                <p>
                    A confirmation email with a unique verification link have been sent to the personal email address you have specified during the time of registration.
                </p>

                <p> If you have already verified your email address, <strong><a href="http://etcweb.entranceengine.com/login">... click here ...</a></strong> to login to your account.</p>
            </div>

            
        </div>


    </div>

    <!-- ... end Buttons -->



</div>