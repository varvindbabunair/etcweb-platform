<div class="content-wrapper">
    <section class="crumina-module crumina-module-slider bg-blue-lighteen background-contain bg-11 medium-padding100">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="crumina-module crumina-heading">
                                <h6 class="heading-sup-title">Register Now </h6>
                                <h2 class="heading-title">Start Coaching FREE</h2>
                                <p class="heading-text">
                                    Register Now and earn <strong>FREE CREDITS WORTH <span class="fa fa-rupee"></span> <?= $this->loggedinuser->creditworth() ?></strong>
                                    and <strong>FREE Mobile App</strong>
                                    to kickstart your coaching with ETCWEB.
                                </p>
                        </div>
                        <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="crumina-module crumina-info-box info-box--standard">
                                                <div class="info-box-image display-flex">
                                                        <svg class="utouch-icon utouch-icon-google-play"><use xlink:href="#utouch-icon-google-play"></use></svg>
                                                        <h6 class="info-box-title">FREE App</h6>
                                                </div>
                                            <p class="info-box-text">
                                                <strong>FREE</strong> Android app for your institute in Google Playstore for easy course management.
                                            </p>
                                        </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="crumina-module crumina-info-box info-box--standard">
                                                <div class="info-box-image display-flex">
                                                        <svg class="utouch-icon utouch-icon-lnr-star"><use xlink:href="#utouch-icon-lnr-star"></use></svg>
                                                        <h6 class="info-box-title">Free Credits</h6>
                                                </div>
                                                <p class="info-box-text">
                                                    Start coaching with ETCWEB for <b>FREE</b> and experience the power of
                                                    innovative technology and cloud.
                                                </p>
                                        </div>
                                </div>
                        </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <?= form_open(base_url().'register') ?>
                    <div class="inst-details">
                        <div class="crumina-module crumina-heading">
                            <h6 class="heading-sup-title">Some basic details about you and your Institute</h6>
                            <h2 class="heading-title">Register your Institute</h2>
                        </div>
                        <div class="with-icon">
                            <input name="inst-name" class="login-field inst-name" placeholder="Name of Institute" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-icon-1"><use xlink:href="#utouch-icon-icon-1"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="inst-phone" class="login-field inst-phone" placeholder="Phone Number to contact Institute" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="inst-email" class="login-field inst-email" placeholder="Email to contact Institute" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="inst-website" class="login-field inst-website" placeholder="Website of Institute (include http://)" type="text" >
                            <svg class="utouch-icon utouch-icon-worlwide"><use xlink:href="#utouch-icon-worlwide"></use></svg>
                        </div>
                        <div style="text-align: center">
                            <div class="btn btn--green-light login-btn form-control regn-continue"> Continue </div>
                        </div>
                    </div>
                    <div class="person-details" style="display: none">
                        <div class="crumina-module crumina-heading">
                            <h6 class="heading-sup-title">Your Personal details to complete your registration</h6>
                            <h2 class="heading-title">Personal Details</h2>
                        </div>
                        <div class="with-icon">
                            <input name="name" class="login-field inst-name" placeholder="Full Name" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-user"><use xlink:href="#utouch-icon-user"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="phone" class="login-field inst-name" placeholder="Phone Number" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="email" class="login-field inst-name" placeholder="Email" type="email" required="required">
                            <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="password" class="login-field inst-name" placeholder="Password" type="password" required="required">
                            <svg class="utouch-icon utouch-icon-settings-5"><use xlink:href="#utouch-icon-settings-5"></use></svg>
                        </div>
                        <div class="with-icon">
                            <input name="cnf-password" class="login-field inst-name" placeholder="Confirm Password" type="text" required="required">
                            <svg class="utouch-icon utouch-icon-settings-5"><use xlink:href="#utouch-icon-settings-5"></use></svg>
                        </div>
                        <input type="checkbox" required="required" name="" style="float: left;width: auto;margin:10px;">
                        I have read and accept to the <a target="_blank" href="/terms">Terms and Conditions</a> and <a href="/privacy" target="_blank">Privacy Policy</a> of etcweb.entranceengine.com and I wish to continue with the registration.
                        <div style="text-align: center">
                            <input name="admin-regn" type="submit" class="btn btn--green-light login-btn form-control" value="Complete Registration" >
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
                
            </div>
        </div>
    </section>
</div>


	<!-- Counters -->

	<section class="bg-secondary-color background-contain bg-10">

			<div class="container">
				<div class="row">
					<div class="counters">
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="25" data-from="2">25</span>
                                                                        <div class="units">K+</div>
								</div>
								<h5 class="counter-title">Quality Questions</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="40" data-from="2">40</span>
                                                                        <div class="units">+</div>
								</div>
								<h5 class="counter-title">Institutes Benefited</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="50" data-from="2">50</span>
									<div class="units">K+</div>
								</div>
								<h5 class="counter-title">Benefited Students</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="60" data-from="2">6</span>
									<div class="units">+</div>
								</div>
								<h5 class="counter-title">Books Published</h5>
							</div>
						</div>

						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<h5 class="c-white">Numbers speak of us</h5>
							<p class="c-semitransparent-white"></p>
						</div>

					</div>
				</div>
			</div>

	</section>

	<!-- ... end  Counters -->


	<!-- Subscribe Form -->

	<section class="bg-primary-color background-contain bg-14 crumina-module crumina-module-subscribe-form">
			<div class="container">
				<div class="row">
					<div class="subscribe-form">
						<div class="subscribe-main-content">
							<img class="subscribe-img" src="img/subscribe-img.png" alt="image">

							<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
								<div class="crumina-module crumina-heading">
									<h2 class="heading-title">Need Assistance ?</h2>
									<p class="heading-text">
                                                                            Feels like you need an assistance in using ETCWEB ?
                                                                            <br>
                                                                            Our experts will help you in setting up etcweb for your institute.
									</p>
                                                                        
						<a href="#" class="btn btn-small btn--icon-right btn-border btn--with-shadow c-primary js-message-popup cd-nav-trigger">
							<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
							<div class="text">
								<span class="title">Request a Callback</span>
							</div>
						</a>
								</div>

							</div>

						</div>
						<div class="subscribe-layer"></div>
					</div>
				</div>
			</div>
	</section>

	<!-- End Subscribe Form -->

