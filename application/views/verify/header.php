<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>ETCWEB - Complete Solution to manage your Education, Training and Curriculum</title>

	<link rel="stylesheet" type="text/css" href="/css/theme-styles.css">
	<link rel="stylesheet" type="text/css" href="/css/blocks.css">
	<link rel="stylesheet" type="text/css" href="/css/widgets.css">


	<!--External fonts-->

	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Paaji" rel="stylesheet">

	<!-- Styles for Plugins -->
	<link rel="stylesheet" type="text/css" href="/css/swiper.min.css">

	<!--Styles for RTL-->

	<!--<link rel="stylesheet" type="text/css" href="css/rtl.css">-->


</head>
<body class="crumina-grid">

<!-- Preloader -->

<div id="hellopreloader" style="display: block; position: fixed;z-index: 99999;top: 0;left: 0;width: 100%;height: 100%;min-width: 100%;background: #66b5ff url(svg/preload.svg) center center no-repeat;background-size: 41px;opacity: 1;"></div>

<!-- ... end Preloader -->


<!-- Header -->

<header class="header header--menu-rounded header--blue-lighteen" id="site-header">

	<div class="header-lines-decoration">
		<span class="bg-secondary-color"></span>
		<span class="bg-blue"></span>
		<span class="bg-blue-light"></span>
		<span class="bg-orange-light"></span>
		<span class="bg-red"></span>
		<span class="bg-green"></span>
		<span class="bg-secondary-color"></span>
	</div>

	<div class="container">

		<a href="#" id="top-bar-js" class="top-bar-link"><svg class="utouch-icon utouch-icon-arrow-top"><use xlink:href="#utouch-icon-arrow-top"></use></svg></a>
			<div class="header-content-wrapper">

			<div class="site-logo">
				<a href="<?= base_url() ?>" class="full-block"></a>
                                <img style="width: 50px" src="/img/etc-logo.png" alt="Utouch">
				<div class="logo-text">
					<div class="logo-title">ETCWEB</div>
					<div class="logo-sub-title">Education . Training . Curriculum</div>
				</div>
			</div>

			<nav id="primary-menu" class="primary-menu" data-dropdown-animation="fade">

				<!-- menu-icon-wrapper -->

				<a href='javascript:void(0)' id="menu-icon-trigger" class="menu-icon-trigger showhide">
					<span class="mob-menu--title">Menu</span>
					<span id="menu-icon-wrapper" class="menu-icon-wrapper">
						<svg width="1000px" height="1000px">
							<path id="pathD" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
							<path id="pathE" d="M 300 500 L 700 500"></path>
							<path id="pathF" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
						</svg>
					</span>
				</a>

				<ul class="primary-menu-menu">
                                    <?php
                                    if(!$this->loggedinuser->isLoggedin()){
                                    ?>
					<li class="">
                                            <a href="<?= base_url() ?>partner">Partner With Us</a>
					</li>
					<li class="">
                                            <a href="<?= base_url() ?>login">Login</a>
					</li>
                                    <?php
                                    }else{
                                    ?>
                                        <li></li>
                                        <?php
                                    }
                                        ?>
				</ul>
				<ul class="nav-add">
                                    <?php
                                    if(!$this->loggedinuser->isLoggedin()){
                                    ?>
					<li class="">
                                            <a href="register" class="btn btn--lime" >
                                                Register Free
                                            </a>
					</li>
                                    <?php
                                    }else{
                                    ?>
					<li class="">
                                            <a href="dashboard" class="btn btn--lime" >
                                                Dashboard
                                            </a>
					</li>
                                    <?php
                                    }
                                    ?>
				</ul>
				</nav>

			</div>

		</div>

</header>

<div class="header-spacer"></div>

<!-- ... End Header -->