<div class="content-wrapper">

    <!-- Stunning Header -->

    <div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
        <div class="container">
            <div class="stunning-header-content">
                <div class="inline-items">
                    <?php
                    if($verify){
                    ?>
                    <h4 class="stunning-header-title">Verification Successful</h4>
                    <?php
                    }else{
                    ?>
                    <h4 class="stunning-header-title">Verification Failed</h4>
                    <?php
                    }
                    ?>
                </div>
                <div class="breadcrumbs-wrap inline-items">
                    <a href="#" class="btn btn--primary btn--round">
                        <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                    </a>

                </div>
            </div>
        </div>
    </div>

    <!-- ... end Stunning Header -->


    <!-- Buttons -->
    <?php
    if($verify){
    ?>

    <div class="container" style="margin-bottom: 75px">
        <div class="row" style="margin-bottom: 120px">
            <div class="col-lg-12" style="text-align: center;">
                <h4>Successfully verified your email id</h4>
                <p>Your email id have been successfully verified and now you can start using etcweb platform to create new cources and start training your students.</p>
                

                <p>Now you can <strong><a href="http://etcweb.entranceengine.com/dashboard">... click here ...</a></strong> to go to the dashboard of your account.</p>
            </div>

            
        </div>


    </div>

    <?php
    }else{
    ?>
    <div class="container" style="margin-bottom: 75px">
        <div class="row" style="margin-bottom: 120px">
            <div class="col-lg-12" style="text-align: center;">
                <h4>Failed to verify your email address</h4>
                <p>We failed to verify your email address as the link you have mentioned is either used or deactivated.</p>

                <p> If you have already a user and have verified your email address, <strong><a href="http://etcweb.entranceengine.com/login">... click here ...</a></strong> to login to your account.</p>
            </div>

            
        </div>


    </div>

    <?php
    }
    ?>

    <!-- ... end Buttons -->



</div>