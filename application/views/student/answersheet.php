
        

            <?php
            // echo $this->input->post('answers');
            $answer = json_decode(str_replace('\'', '"', $this->input->post('answers')));
            // echo str_replace('\'', '"', $this->input->post('answers'));
            // $answer = json_decode(preg_replace("/[\r\n]+/", " ", str_replace('\'', '"', $this->input->post('answers'))));
            // print_r($answer);
            $test = $this->input->post('test');
            
            $test_get_query = $this->db->query("SELECT * FROM test_list WHERE id = $test");
            $questions_list = $test_get_query->result_array();
            $questions_list_get = $questions_list[0];
            $original_answer = json_decode($questions_list_get['questions']);

            $qn_qry = "SELECT * FROM question_list WHERE";
            $section_num = 0;
            foreach ($original_answer as $section){
                $individual_answer_num =0;
                foreach ($section as $qn){
                    $qn_id = $original_answer[$section_num][$individual_answer_num][0];
                    $qn_qry .= " id = '$qn_id' OR";
                    $individual_answer_num++;
                }
                $section_num++;
            }
            $qn_qry = trim($qn_qry,' OR');

            $question_get_query = $this->db->query($qn_qry);

            $qn_arr = array();
            $qn_infos = $question_get_query->result_array();
            foreach($qn_infos as $qn_info){
                $qn_arr[$qn_info['id']] = array('question_type'=>$qn_info['question_type'],'question'=>$qn_info['question'],'answer_option'=>$qn_info['answer_option'],'correct_answer'=>$qn_info['correct_answer'],'hint_type'=>'text','hint'=>$qn_info['hint']);
            }
            
            $section_list = json_decode($questions_list_get['section_title']);
            
            echo '<h3 style="font-weight:bold;text-align:center;">Answersheet : '.$questions_list_get['title'].'</h3>';
            
            $section_num = 0;
            foreach ($original_answer as $answer_section){
                $qn_num = 0;
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h4 style="font-weight:bold;text-align:center;" >Section  : '.$section_list[$section_num].'</h4><hr></div>';
                foreach($answer_section as $ans_org){
                    ?>
        <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="font-size: 16px;">
                <strong><?php echo $qn_num+1; ?></strong>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11" style="font-size: 16px;">
                <?php
                echo $qn_arr[$ans_org[0]]['question'];
                ?>
                <h4 style="font-weight: bold;margin-top:50px;">Answer & Options:</h4>
                <!-- <ul style="list-style-type: none;"> -->
                    <table class="table table-condensed table-hover" >
                    <?php
                    $answer_list = json_decode(preg_replace("/[\r\n]+/", " ", $qn_arr[$ans_org[0]]['answer_option']));
                    foreach ($answer_list as $answer_option){
                        echo '<tr>';
                        echo '<td style="text-align: left;width: 30px;">';
                        if($answer[$section_num][$qn_num][1] == $answer_option[0]){

                            if($answer_option[0] == $qn_arr[$ans_org[0]]['correct_answer']){
                                // echo 'ans option = '.$answer_option[0];
                                echo '<i class="fa fa-check-square-o" style="color:green;"></i> &nbsp;';
                            }else{
                                echo '<span class="fa fa-remove" style="color:red;"></span>';
                            }
                        }else{
                            if($answer_option[0] == $qn_arr[$ans_org[0]]['correct_answer']){
                                echo '<span class="fa fa-square" style="color:green"></span> &nbsp;';
                            }else{
                                echo '<span class="fa fa-square" ></span> &nbsp;';
                            }
                        }
                        echo '</td>';
                        echo '<td style="text-align: left">';
                        echo $answer_option[1];
                        echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </table>
                <!-- </ul> -->
                <?php
                if(trim($qn_arr[$ans_org[0]]['hint']) !=''){
                ?>
                <h4 style="font-weight: bold;border-bottom:1px solid #ccc;">Hint:</h4>
                <?php
                echo $qn_arr[$ans_org[0]]['hint'];
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><hr></div>
        </div>
            <?php
            $qn_num++;
                }
                $section_num++;
            }
            ?>
