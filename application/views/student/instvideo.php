<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Videos</h4>
                <p class="card-category">Select chapter from the list</p>
            </div>
            
            <div class="container">
                <div class="row">
                    <?= form_open(base_url().'video/suboption',array('id'=>'sub-list-form')) ?>
                        <input type="hidden" name="class_id" id="bth-sub-id" value="" />
                    <?= form_close() ?>
                </div>
                <div class="row">
                    <?= form_open(base_url().'video/chapoption',array('id'=>'chap-list-form')) ?>
                        <input type="hidden" name="subject_id" id="bth-chap-id" value="" />
                    <?= form_close() ?>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="form-group"> 
                        <select name="class_id" class="form-control sub-fetch">
                          <option>Select Class</option>
                          <?php foreach ($title as $key) { ?>
                          <option value="<?= $key['id']; ?>"><?= $key['title']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 pull-left" >
                        <div class="form-group subject-list">
                        
                        </div>
                    </div>
            
                    <div class="col-lg-3 col-md-3 pull-left" >
                        <div class="form-group">
                            
                            <?= form_open('/video/videoget',array('style'=>'width:100%','id'=>'synopsis-get-form')) ?>
                            <div class="chapter-list"></div>
                        </div>
                    </div>
                    
                    <!--<div class="col-lg-3 col-md-3 pull-left syllabus-fetch-btn" style="display: none">-->
                    <!--  <button class="btn btn-primary syllabus-fetch">Fetch Syllabus</button>-->
                    <!--</div>-->
                    <?= form_close() ?>
                <div class="clearfix"></div>
      <!-- <div class="row"><hr></div> -->
      </div>
        <div class=" syllabus-list">

        </div>

        <div class="card-footer">
                
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<!--<div class="row syllabus-container" style="overflow: hidden;">-->
<!--    <div class="col-lg-12">-->
<!--        <div class="card">-->
<!--            <div class="card-header card-header-warning">-->
<!--                <h4 class="card-title"><span class="selected-syllabus"></span> Course Syllabus</h4>-->
<!--                <p class="card-category">Availiable Syllabus for <span class="selected-syllabus"></span> Coaching</p>-->
<!--            </div>-->
<!--            <div class="card-body syllabus-content-display">-->
<!--                <h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->