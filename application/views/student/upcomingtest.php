
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title">Tests</h4>
            <p class="card-category">Active Tests</p>
          </div>
            <div class="card-body table-responsive active-courses" style="height: 300px;overflow-y: scroll;">

              <table class="table table-hover">
                  <thead>
                        <th>Test Name</th>
                        <th>Start Time</th>
                        <th>Entry Time</th>
                        <!-- <th>Course</th> -->
                        <th>Options</th>
                  </thead>
                  <tbody class="active-list">
                      <?php
                      $tests = $this->coursemodel->get_unattended_test($this->loggedinuser->user['id'],$this->loggedinuser->user['institute'],$this->loggedinuser->user['course'],$this->loggedinuser->user['batch']);
                      if($tests){
                      foreach($tests as $test){
                      ?>
                      <tr>
                          <td><?= $test['title'] ?></td>
                          <td><?= date('d M Y g:i:s A', strtotime($test['exam_start'])) ?></td>
                          <td><?= date('d M Y g:i:s A', strtotime($test['exam_entry'])) ?></td>
                          <!-- <td><?php 
                          $course = $this->coursemodel->get_course($this->loggedinuser->user['institute'], $test['course'] );
                          echo $course['course_name'];
                            ?></td> -->
                          <td>
                            <a href="/student/test?testid=<?= $test['id'] ?>">
                              <button class="btn btn-sm btn-primary"  title="Edit"><span class="fa fa-edit"></span> Start Test</button>
                            </a>
                          </td>
                      </tr>
                      <?php
                      }
                      }else{
                          echo '<tr ><td colspan="4">No active tests</td></tr>';
                      }
                      ?>
                  </tbody>
              </table>
          </div>
            <div class="card-footer">
                
            </div>
        </div>
      </div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Students to Batches</h4>
      </div>
      <div class="modal-body">
      <?= form_open(base_url().'instadmin/batchlist',array('id'=>'course-list-form')) ?>
      <input type="hidden" name="course-id" id="course-id" value="">
        <?= form_close() ?>
      <?= form_open_multipart(base_url().'instadmin/batchlist') ?>
        <div class="form-group">
            <select name="course-id" class="form-control course-list" name="syllabus" required="" onchange="get_batch_list()" style="width: 100%">
              <option value="">Select Course</option>
              <?php
              $batches = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'], 'active');
              foreach ($batches as $batch) {
                ?>
                <option value="<?= $batch['id'] ?>"><?= $batch['course_name'] ?></option>
                <?php
              }
              ?>
        </select>
      </div>
<div class="clear-fix"></div>
      <div class="batch-content" style="overflow: hidden;margin-top:25px;">

      </div>
      <?= form_close() ?>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" style="float: left;" >Download Student Data Format</button>
        &nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-success" ><span class="fa fa-plus"></span> Add Students</button>
      </div>
    </div>

  </div>
</div>