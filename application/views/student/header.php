<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Dashboard
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  
  
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  
  <link href="<?= base_url() ?>css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
  <!--<link href="<?= base_url() ?>css/bootstrap-datetimepicker.css" rel="stylesheet" />-->
  <link href="<?= base_url() ?>css/dashboard.css" rel="stylesheet" />
  <link href="<?= base_url() ?>css/bootstrap-tagsinput.css" rel="stylesheet" />
  
  
</head>

<body class="">
    
    <div class="message-loader" style="background-color: #000;height: 100%;width: 100%;z-index: 9999;top:0px;left:0px;opacity:0.6;position: fixed;">
        <h1 style="margin-top:20%;text-align: center;color: #fff;"><span class="fa fa-spinner fa-spin"></span></h1>
        <h2 class="global-message" style="text-align:center;color: #fff;">
        </h2>
    </div>
  <div class="wrapper ">
      <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?= base_url() ?>img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo" style="text-align: center;">
        <a href="#" class="simple-text logo-normal">
          <img style="width:50px" src="https://etcweb.entranceengine.com/images/institutes/<?= $this->loggedinuser->institute['logo'] ?>" />
          <?= $this->loggedinuser->institute['name'] ?>
        </a>
      </div>
      <div class="sidebar-wrapper">
          
          <div class="user">
            <div class="photo">
                <img src="/images/user/<?= $this->loggedinuser->user['image'] ?>">
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username collapsed" aria-expanded="false">
                    <span>
                       <?= $this->loggedinuser->user['name'] ?>
                      <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample" style="">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/profile">
                              <i class="material-icons">assignment_ind</i>
                              <span class="sidebar-normal"> My Profile </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/password">
                              <i class="material-icons">settings</i>
                              <span class="sidebar-normal"> Update Password </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">
                              <i class="material-icons">assignment_return</i>
                              <span class="sidebar-normal"> Signout </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
          
          
        <ul class="nav">
          <li class="nav-item <?= (end($this->uri->segments) === '' || end($this->uri->segments) === 'dashboard' ) ?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>dashboard">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'upcomingtest')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>student/upcomingtest">
              <i class="material-icons">view_list</i>
              <p>Active Test</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'attendedtest')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>student/attendedtest">
              <i class="material-icons">assignment_turned_in</i>
              <p>Attended Test</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'syllabus')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>student/syllabus">
              <i class="material-icons">assignment_turned_in</i>
              <p>Syllabus and Synopsys</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'instvideo')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>video/instvideo">
                <i class="material-icons">ondemand_video</i>
              <p>Tutorial Videos</p>
            </a>
          </li>
          <!-- <li class="nav-item <?= (end($this->uri->segments) === 'notification')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>instadmin/notification">
                <i class="material-icons">info</i>
              <p>Notifications</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'dates')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>instadmin/dates">
                <i class="material-icons">calendar_today</i>
              <p>Important Dates</p>
            </a>
          </li>
          <li class="nav-item <?= (end($this->uri->segments) === 'credits')?'active':'' ?>">
            <a class="nav-link" href="<?= base_url() ?>instadmin/credits">
              <i class="material-icons">album</i>
              <p>Credit Report</p>
            </a>
          </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel card">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo"><?= $breadtext ?></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            
            
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content" style="margin-top:0px;">
        <div class="container-fluid">
