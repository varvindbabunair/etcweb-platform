        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <a href="#">etcweb.entranceengine.com</a>
          </div>
        </div>
      </footer>
</div>
  </div>
  <!--   Core JS Files   -->
  
  <script src="<?= base_url() ?>js/jquery-3.2.0.min.js" type="text/javascript"></script>  
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="<?= base_url() ?>js/popper.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>js/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="<?= base_url() ?>js/plugins/chartist.min.js"></script>
  <script src="<?= base_url() ?>js/plugins/bootstrap-notify.js"></script>
  <script src="<?= base_url() ?>js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <script src="<?= base_url() ?>js/demo.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML" async=""></script>
  <script src="<?= base_url() ?>js/student/<?= end($this->uri->segments) ?>.js"></script>
</body>

</html>