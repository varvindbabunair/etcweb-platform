<html>
    <head>
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/font-awesome.css" />
        <link rel="stylesheet" href="/css/exam.css" />
        <script src="/js/jquery-3.2.0.min.js" ></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.countdownTimer.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML" async=""></script>
        
        <script src="/js/exam-script.js"></script>
    </head>
    <body>
        <div class="col-lg-12" style="text-align: center;margin-top:20%">
            <span class="fa fa-spinner fa-spin"></span> Loading...
            
        </div>
        <?php echo '<input id="getid" type="hidden" value="'.$this->input->get('testid').'" />'; ?>
    </body>
    
    
</html>