<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Edit Profile</h4>
        <p class="card-category">Complete your profile</p>
      </div>
      <div class="card-body">
        <form method="post" action="/profile/updatepassword">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="bmd-label-floating">New Password</label>
                <input type="password" name="password" class="pass form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="bmd-label-floating">Confirm Password</label>
                <input type="text" name="cnf-password" class="cnf-pass form-control">
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary pull-right">Update Password</button>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </div>
</div>