<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Edit Profile</h4>
        <p class="card-category">Update your profile details</p>
      </div>
      <div class="card-body">
        <?= form_open_multipart('/profile/profilesubmit') ?>
            <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img class="img img-thumbnail" src="/images/user/<?= $this->loggedinuser->user['image'] ?>" />
                <input type="file" name="image" />
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Name</label>
                <input type="text" name="name" class="form-control" value="<?= $this->loggedinuser->user['name'] ?>">
              </div>
            </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label class="bmd-label-floating">Date of Birth</label>
                      <input type="date" name="dob" class="form-control" value="<?= date('Y-m-d',strtotime(str_replace('-','/',$this->loggedinuser->user['dob']))) ?>">
                  </div>
              </div>
          </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="bmd-label-floating">Email</label>
                        <input type="text" name="email" class="form-control" value="<?= $this->loggedinuser->user['email'] ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="bmd-label-floating">Phone</label>
                        <input type="text" name="phone" class="form-control" value="<?= $this->loggedinuser->user['phone'] ?>">
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Address</label>
                            <input type="text" name="address" class="form-control" value="<?= $this->loggedinuser->user['address'] ?>">
                        </div>
                    </div>
                </div>
            </div>

            </div>

          <button type="submit profile-submit" type="submit" class="btn btn-primary pull-right">Update Profile</button>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card card-profile">
      <div class="card-avatar">
        <a href="#pablo">
          <img class="img" src="/images/user/<?= $this->loggedinuser->user['image'] ?>" />
        </a>
      </div>
      <div class="card-body">
        <h6 class="card-category text-gray">
            <?php
            if($this->loggedinuser->user['status'] === 'admin')
                echo 'Institute Administrator';
            else if($this->loggedinuser->user['status'] === 'parent')
                echo 'Parent';
            else if($this->loggedinuser->user['status'] === 'student')
                echo 'Student';
            ?>
        </h6>
        <h4 class="card-title"><?= $this->loggedinuser->user['name'] ?></h4>
        <p class="card-description align-left">
            <span class="fa fa-university"></span>
            <?= $this->loggedinuser->institute['name'] ?><br>

            <span class="fa fa-at"></span>
            <?= $this->loggedinuser->user['email'] ?><br>

            <span class="fa fa-phone"></span>
            <?= $this->loggedinuser->user['phone'] ?><br>

            <span class="fa fa-envelope"></span>
            <?= $this->loggedinuser->user['address'] ?><br>

        </p>
      </div>
    </div>
  </div>
</div>
