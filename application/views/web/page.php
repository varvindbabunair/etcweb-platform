<div class="content-wrapper">

	<!-- Main Slider -->

	<div class="crumina-module crumina-module-slider container-full-width">
		<div class="swiper-container main-slider navigation-center-both-sides" data-effect="fade">

			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
				<!-- Slides -->
				<div class="swiper-slide bg-1 main-slider-bg-light">

					<div class="container">
						<div class="row table-cell">

							<div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12">

								<div class="slider-content align-center">

									<h1 class="slider-content-title with-decoration" data-swiper-parallax="-100">
										Course Management System For Entrance Coaching Institute

										<svg class="first-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

										<svg class="second-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

									</h1>
									<h6 class="slider-content-text" data-swiper-parallax="-200">
										Advanced Course Management system for <strong>NEET, JEE Mains and Advanced</strong> Coaching Institutes
									</h6>

									<div class="main-slider-btn-wrap" data-swiper-parallax="-300">
										<a href="register" class="btn btn-border btn--with-shadow c-primary">
											Get Started Free
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600" style="text-align: center;margin-top: -20px;">
                                    <img src="img/slides1.png" alt="slider">
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="swiper-slide bg-2 main-slider-bg-light">

					<div class="container table">
						<div class="row table-cell">

							<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
								<div class="slider-content align-both">
									<h2 class="slider-content-title" data-swiper-parallax="-100">
										Detailed <span class="c-primary">Analytics</span>
									</h2>
									<h6 class="slider-content-text" data-swiper-parallax="-300">
                                                                                An intutive dashboard with detailed analytics of each students performance
                                                                                enabling teachers and parents to effectively guide their ward to success.
									</h6>
									<div class="main-slider-btn-wrap" data-swiper-parallax="-700">
                                                                                <a href="register" class="btn btn-border btn--with-shadow c-primary">
											Start Coaching for FREE
										</a>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="swiper-slide thumb-left bg-3 main-slider-bg-light">

					<div class="container table full-height">
						<div class="row table-cell">
							<div class="col-lg-6 col-sm-12 table-cell">

								<div class="slider-content align-both">

									<h2 class="slider-content-title" data-swiper-parallax="-100">Rise Up with high quality Content</h2>

									<h6 class="slider-content-text" data-swiper-parallax="-300">
                                                                                Our faculties are experts in the domain which enables us to provide you with high quality content
                                                                                and our strict quality control measures enables your students to stand above the 
                                                                                tide by practicing with the highest quality materials.
                                                                                
									</h6>

									<div class="main-slider-btn-wrap" data-swiper-parallax="-600">

										<a href="register" class="btn btn--lime btn--with-shadow">
											Get Started Free
										</a>

									</div>

								</div>

							</div>

							<div class="col-lg-6 col-sm-12 table-cell">
								<div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
									<img src="img/slides2.png" alt="slider">
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="swiper-slide bg-4 main-slider-bg-light">

					<div class="container table full-height">
						<div class="row table-cell">
							<div class="col-lg-6 col-sm-12 table-cell">

								<div class="slider-content align-both">

									<h2 class="slider-content-title" data-swiper-parallax="-100">Rise Up with high quality Content</h2>

									<h6 class="slider-content-text" data-swiper-parallax="-300">
                                                                                Our faculties are experts in the domain which enables us to provide you with high quality content
                                                                                and our strict quality control measures enables your students to stand above the 
                                                                                tide by practicing with the highest quality materials.
                                                                                
									</h6>

									<div class="main-slider-btn-wrap" data-swiper-parallax="-600">

										<a href="register" class="btn btn--lime btn--with-shadow">
											Get Started Free
										</a>

									</div>

								</div>

							</div>

							<div class="col-lg-6 col-sm-12 table-cell">
								<div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
									<img src="img/slides2.png" alt="slider">
								</div>
							</div>

						</div>
					</div>
				</div>


			</div>

			<!--Prev next buttons-->

			<div class="btn-prev with-bg">
				<svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
				<svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
			</div>

			<div class="btn-next with-bg">
				<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
				<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
			</div>

		</div>
	</div>

	<!-- ... end Main Slider -->

	<!-- Info Boxes -->

	<section class="medium-padding100">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-info-box info-box--standard-hover">

							<div class="info-box-image">
								<img class="utouch-icon" src="svg-icons/cloud-computing.svg" alt="cloud">
								<img class="cloud" src="img/clouds8.png" alt="cloud">
							</div>

							<div class="info-box-content">
								<a href="#" class="h5 info-box-title">Power of Cloud</a>
                                                                <p class="info-box-text">
                                                                    <strong>Free Mobile App</strong> for your institute and secure API's provides
                                                                    your students a seameless <br>experience without any technology and <br>infrastructure barrier.
								</p>
							</div>

							<a href="#" class="btn-next">
								<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
								<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
							</a>

						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-info-box info-box--standard-hover">

							<div class="info-box-image">
								<img class="utouch-icon" src="svg-icons/rocket.svg" alt="advance">
								<img class="cloud" src="img/clouds9.png" alt="cloud">
							</div>

							<div class="info-box-content">
								<a href="#" class="h5 info-box-title">High Quality Content</a>
								<p class="info-box-text">
                                                                    Our contents and questions are prepared and filtered by expert faculties 
                                                                    who are experts in the industry, to ensure you get the 
                                                                    highest quality content.
								</p>
							</div>

							<a href="#" class="btn-next">
								<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
								<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
							</a>

						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-info-box info-box--standard-hover">

							<div class="info-box-image">
								<img class="utouch-icon" src="svg-icons/settings%20(4).svg" alt="smartphone">
								<img class="cloud" src="img/clouds10.png" alt="cloud">
							</div>

							<div class="info-box-content">
								<a href="#" class="h5 info-box-title">Easy to use</a>
								<p class="info-box-text">
                                                                    Our easy to use platform helps you effortless manageent of tests and student performance,
                                                                    and helps you to effectively guide them to the path of success.
								</p>
							</div>

							<a href="#" class="btn-next">
								<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
								<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
							</a>

						</div>
					</div>
				</div>
			</div>
	</section>

	<!-- ... end Info Boxes -->


	<!-- Slider with vertical tabs -->

	<section class="crumina-module crumina-module-slider slider-tabs-vertical-line">

			<div class="swiper-container" data-show-items="1">
				<div class="swiper-wrapper">
					<div class="swiper-slide bg-primary-color bg-5" data-mh="slide">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="slider-tabs-vertical-thumb">
										<img src="img/computer.png" alt="student">
									</div>
								</div>
								<div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="crumina-module crumina-heading custom-color c-white">
										<h6 class="heading-sup-title">User Interface</h6>
										<h2 class="heading-title">For Students</h2>
										<div class="heading-text">
                                            <p>
                                                Students get a platform independent view and analytics which helps them to 
                                                have a clear understanding of their perfomances and their position with other batchmates.
                                                <br>
                                            </p>
                                            <h6 class="heading-sup-title">The student receives</h6>
                                            <ul>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    Intutive dashaboard which shows detailed analytics of his/her performance, strengths and weaknesses.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    A detailes time analytics of his/her performances enabling them to plan better for computer based tests.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    Effective mode of communication between teachers and institute, and stay uptodate through notifications and alerts.</li>
                                            </ul>
										</div>
									</div>
                                                                    
								</div>
							</div>
						</div>
					</div>

					<div class="swiper-slide bg-orange-light bg-6" data-mh="slide">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="slider-tabs-vertical-thumb">
										<img src="img/computer.png" alt="faculty">
									</div>
								</div>
								<div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="crumina-module crumina-heading custom-color c-white">
										<h6 class="heading-sup-title">User Interface</h6>
										<h2 class="heading-title">For Faculty</h2>
										<div class="heading-text">
                                            <p>The teachers receive a simple platform to analyse the performance of each student and 
                                            enables them to provide individual care without much effort.
                                            </p>
                                            <h6 class="heading-sup-title"><strong>The faculty receives</strong></h6>
                                            <ul>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    Dashboard which shows the overall perfomance of his/her students.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    A detailed time analytics of performances of his/her students enabling them to guide students more effectively.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    An intutive dashboard to effectively communicate with the students' parent.</li>
                                            </ul>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="swiper-slide bg-red bg-7" data-mh="slide">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="slider-tabs-vertical-thumb">
										<img src="img/computer.png" alt="parents">
									</div>
								</div>
								<div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
									<div class="crumina-module crumina-heading custom-color c-white">
										<h6 class="heading-sup-title">User Interface</h6>
										<h2 class="heading-title">For Parents</h2>
										<div class="heading-text">
                                            <p>Parents receive an intutive platform to view the progresses of their ward and effectively guide 
                                                students, together with teachers.</p>
											<h6 class="heading-sup-title"><strong>The Parent receives</strong></h6>
                                            <ul>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    An easy to use dashboard to monitor the performance of their ward with detailed analytics.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    An easy way to communicate with the teachers and receive updates from the institutes.</li>
                                                <li>
                                                    <a href="#" class="btn-next pull-left" style="margin-right: 10px;" >
                                                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                                                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                                                    </a>
                                                    An easy way to identify the weaknesses of their ward and assist him.</li>
                                            </ul>
                                        </div>
									</div>
                                                                        
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="slider-slides slider-slides--vertical-line">
					<a href="#" class="slides-item">
						<span class="round primary"></span>01.
					</a>

					<a href="#" class="slides-item">
						<span class="round orange"></span>02.
					</a>

					<a href="#" class="slides-item">
						<span class="round red"></span>03.
					</a>

				</div>

			</div>

	</section>

	<!-- ... Slider with vertical tabs -->


	<!-- Video -->

	<section class="bg-9 background-contain medium-padding120">
			<div class="container">
				<div class="row">
					<div class="display-flex info-boxes">
						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<div class="crumina-module crumina-info-box info-box--standard-round icon-right negative-margin-right150">
								<div class="info-box-image">
									<img src="svg-icons/chat.svg" alt="chat" class="utouch-icon">
								</div>
								<div class="info-box-content">
									<h5 class="info-box-title">Effective Communication</h5>
									<p class="info-box-text">Platform provides an easy interface for communication between teachers and parents,
                                                                            enabling them to effectively guide their students.
									</p>
								</div>
							</div>

							<div class="crumina-module crumina-info-box info-box--standard-round icon-right negative-margin-right150">
								<div class="info-box-image">
									<img src="svg-icons/pictures.svg" alt="chat" class="utouch-icon">
								</div>
								<div class="info-box-content">
									<h5 class="info-box-title">Detailed Time Analytics</h5>
									<p class="info-box-text">
										Each test is accompanied by a detailed analytics
                                        which helps the students to evaluate themselves and effectively manage time in a CBT.
									</p>
								</div>
							</div>
						</div>

						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 align-center">
							<img class="particular-image" src="img/image.png" alt="image">
							<a href="register" class="btn btn--red btn--with-shadow js-message-popup cd-nav-trigger">
								Book a Demo Now
							</a>
						</div>

						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<div class="crumina-module crumina-info-box info-box--standard-round negative-margin-left150">
								<div class="info-box-image">
									<img src="svg-icons/clock.svg" alt="chat" class="utouch-icon">
								</div>
								<div class="info-box-content">
									<h5 class="info-box-title">Quick preparation Synopsys</h5>
									<p class="info-box-text">
		                                Synopsys provided for each chapter is a highly concise summary of important points
		                                prepared by experts to help in easy preparation.
                                	</p>
								</div>
							</div>

							<div class="crumina-module crumina-info-box info-box--standard-round negative-margin-left150">
								<div class="info-box-image">
									<img src="svg-icons/calendar.svg" alt="chat" class="utouch-icon">
								</div>
								<div class="info-box-content">
									<h5 class="info-box-title">Track progress</h5>
									<p class="info-box-text">
                                        Teachers, Parents and institute can monitor the progress of their students
                                        anytime anywhere and guide them based on detailed analytics.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<!-- ... end Video -->



	<!-- Counters -->

	<section class="bg-secondary-color background-contain bg-10">

			<div class="container">
				<div class="row">
					<div class="counters">
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="25" data-from="2">25</span>
                                                                        <div class="units">K+</div>
								</div>
								<h5 class="counter-title">Quality Questions</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="40" data-from="2">40</span>
                                                                        <div class="units">+</div>
								</div>
								<h5 class="counter-title">Institutes Benefited</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="50" data-from="2">50</span>
									<div class="units">K+</div>
								</div>
								<h5 class="counter-title">Benefited Students</h5>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<div class="crumina-module crumina-counter-item">
								<div class="counter-numbers counter c-yellow">
									<span data-speed="2000" data-refresh-interval="3" data-to="60" data-from="2">6</span>
									<div class="units">+</div>
								</div>
								<h5 class="counter-title">Books Published</h5>
							</div>
						</div>

						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<h5 class="c-white">Numbers speak of us</h5>
							<p class="c-semitransparent-white"></p>
						</div>

					</div>
				</div>
			</div>

	</section>

	<!-- ... end  Counters -->


	<!-- Pricing Tables -->

	<section class="background-contain bg-13 medium-padding100">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 mb60">
						<div class="crumina-module crumina-heading align-center">
							<h6 class="heading-sup-title">Our Pricing Plans</h6>
                                                        <h2 class="heading-title with-decoration">Pay for only what you use.
                                                        
											<svg class="first-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

											<svg class="second-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

                                                        </h2>
						</div>
					</div>
				</div>
                            <div class="row">
                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 align-center"></div>
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 align-center">
                                    <div class="heading-title" style="width:50%;float:left;">
                                        <svg class="utouch-icon utouch-icon-lnr-star" style="width: 100%;"><use xlink:href="#utouch-icon-lnr-star"></use></svg>
                                    </div>
                                    <div style="width:50%;float:left;">
                                        <p>
                                            Intutive credit based system which enables you to pay only for what you use.
                                        </p>
                                        <h6 class="with-decoration">
                                            Credit Prices as Low as <span class="fa fa-rupee"></span><?= $this->loggedinuser->creditprice() ?>
                                        </h6>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 align-center"></div>
                            </div>
				<div class="row">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 align-center"></div>
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <a href="register" class="btn btn--green btn--with-shadow">
                                                    Register now and Earn Free Credits Worth <span class="fa fa-rupee"></span><?= $this->loggedinuser->creditworth() ?>
                                                </a>
                                        </div>
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 align-center"></div>
				</div>
			</div>
	</section>

	<!-- ... end Pricing Tables -->


	<!-- Subscribe Form -->

	<section class="bg-primary-color background-contain bg-14 crumina-module crumina-module-subscribe-form">
			<div class="container">
				<div class="row">
					<div class="subscribe-form">
						<div class="subscribe-main-content">
							<img class="subscribe-img" src="img/subscribe-img.png" alt="image">

							<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
								<div class="crumina-module crumina-heading">
									<h2 class="heading-title">Interested in ETCWEB ?</h2>
									<p class="heading-text">
                                        Take your institute to the next level Now...
                                        <br>
                                        
                                        Register your institute now to get <strong>FREE CREDITS worth <span class="fa fa-rupee"></span> <?= $this->loggedinuser->creditworth() ?></strong> and 
                                        start coaching now
									</p>
                                                                        
						<a href="register" class="btn btn-small btn--icon-right btn-border btn--with-shadow c-primary">
							<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
							<div class="text">
								<span class="title">Get Started Now</span>
							</div>
						</a>
								</div>

							</div>

						</div>
						<div class="subscribe-layer"></div>
					</div>
				</div>
			</div>
	</section>

	<!-- End Subscribe Form -->


</div>

