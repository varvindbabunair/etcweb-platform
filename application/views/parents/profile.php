<div class="row">
    <div class="col-md-4">
      <div class="card card-profile">
        <div class="card-avatar">
          <a href="#pablo">
            <img class="img" src="<?= base_url() ?>img/man2.png" />
          </a>
        </div>
        <div class="card-body">
          <h4 class="card-title">Student Name</h4>
          <h6 class="card-category text-gray"><strong>Regn No:</strong> 123456</h6>
          <p class="card-description">
              <span class="fa fa-phone"></span> 123456789
              <br>
              <span class="fa fa-envelope"></span> email@email.com
          </p>
        </div>
      </div>
    </div>
    
    <div class="col-md-4">
      <div class="card card-profile">
        <div class="card-avatar">
          <a href="#pablo">
            <img class="img" src="<?= base_url() ?>img/man2.png" />
          </a>
        </div>
        <div class="card-body">
          <h4 class="card-title">Parent Name</h4>
          <p class="card-description">
              <span class="fa fa-phone"></span> 123456789
              <br>
              <span class="fa fa-envelope"></span> email@email.com
          </p>
        </div>
      </div>
    </div>
  </div>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title">Update Password</h4>
            <p class="card-category">Update your password </p>
          </div>
          <div class="card-body table-responsive">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Current Password</label>
                <input class="form-control" type="text">
              </div>
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">New Password</label>
                <input class="form-control" type="text">
              </div>
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Confirm New Password</label>
                <input class="form-control" type="text">
              </div>
                  <button type="submit" class="btn btn-primary pull-right">Update Password<div class="ripple-container"></div></button>
              
          </div>
        </div>
      </div>
</div>