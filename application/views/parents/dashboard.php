<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">contacts</i>
        </div>
        <p class="card-category">Attendance</p>
        <h3 class="card-title">80
          <small>%</small>
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">timeline</i>
          <a href="#pablo">View Full Attendance</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assessment</i>
        </div>
        <p class="card-category">Rank</p>
        <h3 class="card-title">3</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">assignment</i> View all test status
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-info card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment_turned_in</i>
        </div>
        <p class="card-category">Tests Appeared</p>
        <h3 class="card-title">6</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">assignment</i> View all tests
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
          <i class="material-icons">info_outline</i>
        </div>
        <p class="card-category">Pending Tests</p>
        <h3 class="card-title">3</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">assignment</i> View all tests
        </div>
      </div>
    </div>
  </div>

</div>

<div class="row">
   <div class="col-md-4">
    <div class="card card-profile">
      <div class="card-avatar">
        <a href="#pablo">
            <img class="img" src="<?= base_url() ?>img/man2.png">
        </a>
      </div>
      <div class="card-body">
        <h6 class="card-category text-gray">Class Teacher</h6>
        <h4 class="card-title">Alec Thompson</h4>
        <a href="#pablo" class="btn btn-primary btn-round"><i class="material-icons">local_phone</i> Contact</a>
        <a href="#pablo" class="btn btn-primary btn-round"><i class="material-icons">message</i> Message</a>
      </div>
    </div>
  </div>
  <div class="col-lg-8 col-md-12">
    <div class="card">
      <div class="card-header card-header-danger">
          <h4 class="card-title"><span class="fa fa-envelope"></span>  Messages</h4>
      </div>
        <div class="card-body table-responsive" style="height: 300px;">
        <table class="table table-hover">
          <thead class="text-warning">
            <th>From</th>
            <th>Message</th>
          </thead>
          <tbody>
            <tr>
                <td>
                    <div class="msg-thumb pull-left"><img class="img" src="<?= base_url() ?>img/man2.png" /></div>
                    <h5 style="padding-left: 70px;">Alec Thompson</h5>
                </td>
              <td>Message to student that his performance may or may not be satisfactory</td>
            </tr>
            <tr>
                <td>
                    <div class="msg-thumb pull-left"><img class="img" src="<?= base_url() ?>img/man2.png" /></div>
                    <h5 style="padding-left: 70px;">Alec Thompson</h5>
                </td>
              <td>Message to student that his performance may or may not be satisfactory</td>
            </tr>
            <tr>
                <td>
                    <div class="msg-thumb pull-left"><img class="img" src="<?= base_url() ?>img/man2.png" /></div>
                    <h5 style="padding-left: 70px;">Alec Thompson</h5>
                </td>
              <td>Message to student that his performance may or may not be satisfactory</td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
   <div class="col-lg-6 col-md-12">
    <div class="card">
      <div class="card-header card-header-warning">
          <h4 class="card-title"><span class="fa fa-info-circle"></span>  Notifications</h4>
      </div>
        <div class="card-body table-responsive" style="height: 300px;">
        <table class="table table-hover">
          <thead class="text-warning">
            <th>Date</th>
            <th>Notification</th>
          </thead>
          <tbody>
            <tr>
                <td>
                    12 Oct 2018
                </td>
                <td><p>Notifications for student, may be for a branch or public notifications</p></td>
            </tr>
            <tr>
                <td>
                    12 Oct 2018
                </td>
                <td><p>Notifications for student, may be for a branch or public notifications</p></td>
            </tr>
            <tr>
                <td>
                    12 Oct 2018
                </td>
                <td><p>Notifications for student, may be for a branch or public notifications</p></td>
            </tr>
            <tr>
                <td>
                    12 Oct 2018
                </td>
                <td><p>Notifications for student, may be for a branch or public notifications</p></td>
            </tr>
            <tr>
                <td>
                    12 Oct 2018
                </td>
                <td><p>Notifications for student, may be for a branch or public notifications</p></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
    
  <div class="col-lg-6 col-md-12">
    <div class="card">
      <div class="card-header card-header-success">
          <h4 class="card-title"><span class="fa fa-calendar"></span> Importand Dates</h4>
      </div>
        <div class="card-body table-responsive" style="height: 300px;">
        <table class="table table-hover">
          <thead class="text-warning">
            <th>Date</th>
            <th>Message</th>
          </thead>
          <tbody>
            <tr>
                <td>
                    12 October 2018
                </td>
                <td><p>Initial Site launch</p></td>
            </tr>
            <tr>
                <td>
                    15 October 2018
                </td>
                <td><p>Meeting with clients</p></td>
            </tr>
            <tr>
                <td>
                    25 October 2018
                </td>
                <td><p>Atleast one registered client</p></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
