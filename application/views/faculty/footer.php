        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, 
            <a href="#">etcweb.entranceengine.com</a>
          </div>
        </div>
      </footer>
</div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?= base_url() ?>/js/jquery-3.2.0.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>/js/popper.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>/js/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="<?= base_url() ?>/js/plugins/chartist.min.js"></script>
  <script src="<?= base_url() ?>/js/plugins/bootstrap-notify.js"></script>
  <script src="<?= base_url() ?>/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <script src="<?= base_url() ?>/js/demo.js"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
</body>

</html>