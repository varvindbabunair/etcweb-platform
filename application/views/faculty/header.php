<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Dashboard
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?= base_url() ?>/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?= base_url() ?>/css/dashboard.css" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
      <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?= base_url() ?>img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          DSTOTALS
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="<?= base_url() ?>dashboard">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
              <i class="material-icons">view_list</i>
              <p>Syllabus & Synopsys</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
              <i class="material-icons">assignment_turned_in</i>
              <p>Manage Tests</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
              <i class="material-icons">account_balance</i>
              <p>Manage Course</p>
            </a>
          </li>
          
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
              <i class="material-icons">supervisor_account</i>
              <p>Manage Students</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
                <i class="material-icons">message</i>
              <p>Messages</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
                <i class="material-icons">info</i>
              <p>Notifications</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
                <i class="material-icons">calendar_today</i>
              <p>Important Dates</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>profile">
              <i class="material-icons">person</i>
              <p>Profile</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= base_url() ?>logout">
              <i class="fa fa-sign-out "></i>
              <p>Sign Out</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            
            
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
