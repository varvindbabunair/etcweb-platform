<div class="row">

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">supervisor_account</i>
        </div>
        <p class="card-category">Faculties</p>
        <h3 class="card-title course-number"><?= $this->coursemodel->get_faculty_count($this->loggedinuser->user['institute'], 'active') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>

    <div class="col-lg-6 col-md-6 col-sm-6"></div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <br>
        <button class="btn btn-lg btn-primary" style="width: 100%" data-toggle="modal" data-target="#myModal">
            <span class="fa fa-plus"></span>
            Add Faculties by file
        </button>
        <br>
        <button class="btn btn-lg btn-primary" style="width: 100%" data-toggle="modal" data-target="#studAddModal">
            <span class="fa fa-plus"></span>
            Add Faculty
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title">Faculties</h4>
            <p class="card-category">Active Faculties</p>
          </div>
            <div class="card-body table-responsive active-courses" style="">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $faculties = $this->coursemodel->get_faculty($this->loggedinuser->user['institute'],'active');
                        ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($faculties as $faculty){
                                ?>
                                <tr>
                                    <td><?= $faculty['name'] ?><br><strong>Reg No: <?= $faculty['regn_number'] ?></strong></td>
                                    <td>
                                        <span class="fa fa-envelope"></span><?= $faculty['email'] ?><br>
                                        <span class="fa fa-phone"></span><?= $faculty['phone'] ?><br>
                                        <span class="fa fa-map-marker"></span> <?= $faculty['address'] ?>
                                    </td>
                                    <td>
                                        <a href="/instadmin/editfaculty/<?= $faculty['id'] ?>">
                                        <button class="btn btn-primary btn-round btn-fab btn-sm" title="Edit Details"><span class="fa fa-edit"></span></button>
                                        </a>
                                            <button  onclick="resetPass(<?= $faculty['id'] ?>,'<?= $faculty['name'] ?>')" class="btn btn-primary btn-round btn-fab btn-sm" title="Change Password"><span class="fa fa-key"></span></button>
                                        <button class="btn btn-primary btn-round btn-fab btn-sm" title="Revoke Access / Deactivate"><span class="fa fa-lock"></span></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row"><hr></div>
              
              <div class="row stud-list">

              </div>
          
            <div class="card-footer">
                
            </div>
        </div>
      </div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Faculties by file</h4>
      </div>
      <div class="modal-body">
      <?= form_open_multipart(base_url().'instadmin/addfaculties') ?>
            Select File:
            <input type="file" class="" name="faculty-list" />

      <button type="submit" class="btn btn-success" ><span class="fa fa-plus"></span> Add Faculties</button>
      <?= form_close() ?>
          <div class="clearfix"></div>
      <hr>
        <p>
          Click on the button below to download the faculty details upload file csv format. Download the file in csv format and upload it after filling the fields.
        </p>
        <a target="_blank" href="/files/faculty_list.csv"><button type="button" class="btn btn-primary" style="float: left;" >Download Faculty Data Format</button></a>
        <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>
</div>

<!-- Student Add Modal -->
<div id="studAddModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Faculty Individually</h4>
      </div>
      <form id="addIndStudent" action="/instadmin/addfaculty" method="POST">
          <div class="modal-body">
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Name</label>
                <input type="text" required="" name="name" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Email</label>
                <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Phone Number</label>
                <input type="phone" name="phone" class="form-control">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Registration Number</label>
                        <input type="phone" required="" name="regn-number" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label">Date of Birth</label>
                        <input type="date" placeholder="" required="" name="dob" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Address</label>
                <textarea required="" name="address" class="form-control"></textarea>
            </div>
          </div>

          <div class="modal-footer">
            <input class="btn btn-primary add-indvidual-student" type="submit" name="add-individual-student" value="Add Faculty">
          </div>
      </form>

  </div>

</div>
</div>


    <!-- Modal -->
    <div id="passreset" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <form id="passresetform" method="post" action="/instadmin/studpassupdate">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reset Password for <span class="modalname"></span></h4>
                    </div>
                    <div class="modal-body">
                        <p>Set new password for <span class="modalname"></span></p>
                        <input type="hidden" name="studid" class="id" value="" />

                        <div class="form-group">
                            <label class="bmd-label-floating">New Password</label>
                            <input type="password" name="password" class="pass form-control">
                        </div>

                        <div class="form-group">
                            <label class="bmd-label-floating">Confirm Password</label>
                            <input type="text" name="cnf-password" class="cnf-pass form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-smstupassresetbtn">Update Password</button>
                    </div>
                </div>
            </form>
        </div>
    </div>