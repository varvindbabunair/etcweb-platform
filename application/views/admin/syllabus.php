<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title">Syllabus</h4>
            <p class="card-category">Select chapter from the list</p>
          </div>
            
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <?= form_open('/instadmin/syllabuscontent') ?>
                    <select name="syllabus" class="form-control syllabus-select">
                        <option value="">Select Syllabus</option>
                        <option value="eng">Engineering</option>
                        <option value="med">Medical</option>
                        <option value="both">Both Engineering and Medical</option>
                    </select>

                    <?= form_close() ?>
                  </div>
            </div>
        </div>
    </div>
    
  </div>

<div class="row syllabus-container" style="overflow: hidden;">
    <div class="col-lg-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title"><span class="selected-syllabus"></span> Course Syllabus</h4>
            <p class="card-category">Availiable Syllabus for <span class="selected-syllabus"></span> Coaching</p>
          </div>
          <div class="card-body syllabus-content-display">
              <h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>
          </div>
        </div>
      </div>



</div>