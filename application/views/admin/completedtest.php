<div class="row">
    <?php
    $this->db->where(array(
        'id' =>$this->input->get('student')
    ));
    $qry = $this->db->from('user_list')->get();
    $student = $qry->result_array();

    ?>
    <div class="col-lg-2 col-md-3 col-sm-4">
        <img style="width: 100%;" class="img img-thumbnail" src="/images/user/<?= $student[0]['image'] ?>">
    </div>
    <div class="col-lg-10 col-md-9 col-sm-8">
        <h3><strong><?= $student[0]['name'] ?></strong></h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title">Tests</h4>
            <p class="card-category">Submitted Tests</p>
          </div>
            <div class="card-body table-responsive active-courses" style="height: 300px;overflow-y: scroll;">

              <table class="table table-hover">
                  <thead>
                        <th>Test Name</th>
                        <th>Exam Submit Time</th>
                        <th>Options</th>
                  </thead>
                  <tbody class="active-list">
                      <?php
                      $tests = $this->coursemodel->get_attended_test($this->input->get('student'));
                      if($tests){
                      foreach($tests as $test){
                      ?>
                      <tr>
                          <td><?= $test['title'] ?></td>
                          <td><?= date('d M Y H:i:s', strtotime($test['test_time'])) ?></td>
                          <td>
                            <form action="/instadmin/studentperformance" method="POST">
                              <input type="hidden" name="ansid" value="<?= $test['id'] ?>" />
                              <input type="hidden" name="test" value="<?= $test['test'] ?>" />
                              <button class="btn btn-sm btn-primary" type="submit"><span class="fa fa-edit"></span> View Test Performance</button>
                            </form>
                            <form action="/instadmin/answersheet" method="POST">
                              <input type="hidden" name="ansid" value="<?= $test['id'] ?>" />
                              <input type="hidden" name="test" value="<?= $test['test'] ?>" />
                              <input type="hidden" name="answers" value="<?= str_replace("\"","'",$test['answer']) ?>" />
                              <button class="btn btn-sm btn-primary" type="submit"><span class="fa fa-list"></span> View Answersheet</button>
                            </form>
                          </td>
                      </tr>
                      <?php
                      }
                      }else{
                          echo '<tr ><td colspan="4">No tests submitted</td></tr>';
                      }
                      ?>
                  </tbody>
              </table>
          </div>
            <div class="card-footer">
                
            </div>
        </div>
      </div>
</div>





<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p style="text-align:center;"><span class="fa fa-spin fa-spinner"></span>Loading....</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>