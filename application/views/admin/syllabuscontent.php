<?php
if($syllabus == 'eng'){
	?>
	<table class="table table-responsive table-bordered">
		<tr><th colspan="3">Class 11</th><th colspan="3">Class 12</th></tr>
		<tr><th>Physics</th><th>Chemistry</th><th>Mathematics</th><th>Physics</th><th>Chemistry</th><th>Mathematics</th></tr>
		<?php
		$physics11 = $this->syllabusmodel->class11physics();
		$physics12 = $this->syllabusmodel->class12physics();
		$chemistry11 = $this->syllabusmodel->class11chemistry();
		$chemistry12 = $this->syllabusmodel->class12chemistry();
		$maths11 = $this->syllabusmodel->class11maths();
		$maths12 = $this->syllabusmodel->class12maths();

		$max = max(sizeof($physics11),sizeof($physics12),sizeof($chemistry11),sizeof($chemistry12),sizeof($maths11),sizeof($maths12));

		for($row=0;$row<$max;$row++){
			echo "<tr><td>";
			if(isset($physics11[$row])){
				echo '<div >'.$physics11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				// echo '<a href="/instadmin/synopsys" value="'.$physics11[$row]['id'].'" name="view_value" type="submit" target="_blank"><i style="cursor:pointer" class="material-icons" title="View Synopsys" >note</i></a>';
				// echo '<button type="submit"><i class="material-icons" title="View Synopsys" >note</i></button>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry11[$row])){
				echo '<div >'.$chemistry11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($maths11[$row])){
				echo '<div >'.$maths11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$maths11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$maths11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$maths11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$maths11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$maths11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$maths11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($physics12[$row])){
				echo '<div >'.$physics12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry12[$row])){
				echo '<div >'.$chemistry12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($maths12[$row])){
				echo '<div >'.$maths12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$maths12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$maths12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$maths12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$maths12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$maths12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$maths12[$row]['id'].')" class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td></tr>";
		}

		?>
	</table>
	<?php
}else if($syllabus == 'med'){
	?>
	<table class="table table-responsive table-bordered">
		<tr><th colspan="3">Class 11</th><th colspan="3">Class 12</th></tr>
		<tr><th>Physics</th><th>Chemistry</th><th>Biology</th><th>Physics</th><th>Chemistry</th><th>Biology</th></tr>
		<?php
		$physics11 = $this->syllabusmodel->class11physics();
		$physics12 = $this->syllabusmodel->class12physics();
		$chemistry11 = $this->syllabusmodel->class11chemistry();
		$chemistry12 = $this->syllabusmodel->class12chemistry();
		$biology11 = $this->syllabusmodel->class11biology();
		$biology12 = $this->syllabusmodel->class12biology();

		$max = max(sizeof($physics11),sizeof($physics12),sizeof($chemistry11),sizeof($chemistry12),sizeof($biology11),sizeof($biology12));

		for($row=0;$row<$max;$row++){
			echo "<tr><td>";
			if(isset($physics11[$row])){
				echo '<div >'.$physics11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics11[$row]['id'].')" class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry11[$row])){
				echo '<div >'.$chemistry11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($biology11[$row])){
				echo '<div >'.$biology11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$biology11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$biology11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$biology11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$biology11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$biology11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$biology11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($physics12[$row])){
				echo '<div >'.$physics12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry12[$row])){
				echo '<div >'.$chemistry12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($biology12[$row])){
				echo '<div >'.$biology12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$biology12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$biology12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$biology12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$biology12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$biology12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$biology12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td></tr>";
		}

		?>
	</table>
	<?php
}else if($syllabus == 'both'){
	?>
	<table class="table table-responsive table-bordered">
		<tr><th colspan="4">Class 11</th><th colspan="4">Class 12</th></tr>
		<tr><th>Physics</th><th>Chemistry</th><th>Biology</th><th>Mathematics</th><th>Physics</th><th>Chemistry</th><th>Biology</th><th>Mathematics</th></tr>
		<?php
		$physics11 = $this->syllabusmodel->class11physics();
		$physics12 = $this->syllabusmodel->class12physics();
		$chemistry11 = $this->syllabusmodel->class11chemistry();
		$chemistry12 = $this->syllabusmodel->class12chemistry();
		$biology11 = $this->syllabusmodel->class11biology();
		$biology12 = $this->syllabusmodel->class12biology();
		$maths11 = $this->syllabusmodel->class11maths();
		$maths12 = $this->syllabusmodel->class12maths();

		$max = max(sizeof($physics11),sizeof($physics12),sizeof($chemistry11),sizeof($chemistry12),sizeof($biology11),sizeof($biology12),sizeof($maths11),sizeof($maths12));

		for($row=0;$row<$max;$row++){
			echo "<tr><td>";
			if(isset($physics11[$row])){
				echo '<div >'.$physics11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry11[$row])){
				echo '<div >'.$chemistry11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($biology11[$row])){
				echo '<div >'.$biology11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$biology11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$biology11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$biology11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$biology11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$biology11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$biology11[$row]['id'].')" class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($maths11[$row])){
				echo '<div >'.$maths11[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$maths11[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$maths11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$maths11[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$maths11[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$maths11[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$maths11[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($physics12[$row])){
				echo '<div >'.$physics12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$physics12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$physics12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$physics12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$physics12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($chemistry12[$row])){
				echo '<div >'.$chemistry12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$chemistry12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$chemistry12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$chemistry12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$chemistry12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($biology12[$row])){
				echo '<div >'.$biology12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$biology12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$biology12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$biology12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$biology12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$biology12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$biology12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td>";
			echo "<td>";
			if(isset($maths12[$row])){
				echo '<div >'.$maths12[$row]['title'].'</div>';
				echo '<div class="col-lg-12"><div class="pull-right" style="margin-left:10px;" >';
				echo form_open('',array('id'=>'synopsys-form-'.$maths12[$row]['id']));
				echo '<input type="hidden" value="synopsys" name="view_type" />';
				echo '<input type="hidden" value="'.$maths12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewSynopsys(\''.$maths12[$row]['id'].'\')" class="material-icons" title="View Synopsys" >note</i>';
				echo form_close();
				echo '</div><div class="pull-right">';
				echo form_open('instadmin/allquestions',array('id'=>'question-form-'.$maths12[$row]['id']));
				echo '<input type="hidden" value="questions" name="view_type" />';
				echo '<input type="hidden" value="'.$maths12[$row]['id'].'" name="view_value" />';
				echo '<i style="cursor:pointer" onclick="viewQuestions('.$maths12[$row]['id'].')"  class="material-icons" title="View Questions">format_list_numbered</i>';
				echo form_close();
				echo '</div></div>';
			}
			echo "</td></tr>";
		}

		?>
	</table>
	<?php
}
?>

<!-- Modal -->
<div id="syllabusModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title syllabus-modal-title">Modal Header</h4>
      </div>
      <div class="modal-body syllabus-modal-content">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="questionModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title question-modal-title">Modal Header</h4>
      </div>
      <div class="modal-body question-modal-content">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
