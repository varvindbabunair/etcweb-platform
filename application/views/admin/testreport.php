<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <p class="card-category">Tests Completed</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'inactive' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Today</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'today' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Scheduled</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'upcoming' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>


    <div class="col-lg-3 col-md-6 col-sm-6">
        <br>
        <a href="/instadmin/createtest" >
            <button class="btn btn-lg btn-primary" style="width: 100%">
                <span class="fa fa-plus"></span>
                Create Test
            </button>
        </a>
    </div>


</div>

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Scheduled Today</p>
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <thead>
                    <th>#</th>
                    <th>Test</th>
                    <th>Completion Status</th>
                    </thead>
                    <tbody>

                    <?php
                    $cnt = 1;
                    $tests_today = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'today');
                    if(sizeof($tests_today) > 0){

                        foreach ($tests_today as $tests){
                            ?>
                            <tr>
                                <td><?= $cnt ?></td>
                                <td><?= $tests['title'] ?></td>
                                <td>
                                    <?php
                                    $status = $this->coursemodel->get_test_status($tests['id']);
                                    echo $status['attended'] .'/'.$status['total'];
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $cnt++;
                        }
                    }else{
                        echo'<tr><td colspan="4">No Tests scheduled today</td></tr>';
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View Full Test Reports</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Ongoing Tests Status</p>
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <thead>
                    <th>#</th>
                    <th>Test</th>
                    <th>Completion Status</th>
                    </thead>
                    <tbody>

                    <?php
                    $cnt = 1;
                    $tests_today = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'ongoing');
                    if(sizeof($tests_today) > 0){

                        foreach ($tests_today as $tests){
                            ?>
                            <tr>
                                <td><?= $cnt ?></td>
                                <td><?= $tests['title'] ?></td>
                                <td>
                                    <?php
                                    $status = $this->coursemodel->get_test_status($tests['id']);
                                    echo $status['attended'] .'/'.$status['total'];
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $cnt++;
                        }
                    }else{
                        echo'<tr><td colspan="4">No Tests Ongoing</td></tr>';
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View Full Test Reports</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title">Completed Tests</h4>
                <p class="card-category">Reports and Status of Completed Tests</p>
            </div>
            <div class="card-body table-responsive active-courses" style="max-height: 500px;overflow: auto;">

                <table class="table table-hover">
                    <thead>
                    <th>Test Name</th>
                    <th>Time Details</th>
                    <th>Course & Batch</th>
                    <th>Student Attendance</th>
                    <th>Options</th>
                    </thead>
                    <tbody class="active-list">
                    <?php
                    $tests = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'inactive');
                    if($tests){
                        foreach($tests as $test){
                            ?>
                            <tr>
                                <td><?= $test['title'] ?></td>
                                <td>
                                    <span title="Start Time" class="fa fa-hourglass-start"></span>
                                    <?= date('d M Y g:i:s A', strtotime($test['exam_start'])) ?><br>
                                    <span title="End Time" class="fa fa-hourglass-end"></span>
                                    <?= date('d M Y g:i:s A', strtotime($test['exam_entry'])) ?></td>
                                <td><?php
                                    $course = $this->coursemodel->get_course($this->loggedinuser->user['institute'], $test['course'] );
                                    echo $course['course_name'];
                                    ?></td>
                                <td><?php
                                    $status = $this->coursemodel->get_test_status($test['id']);
                                    echo $status['attended'] .'/'.$status['total'];
                                    ?></td>
                                <td>
                                    <?php
                                    if($test['ranklist_published'] == 0 ) {
                                        ?>
                                        <a target="_blank" href="/instadmin/publishranklist?testid=<?= $test['id'] ?>">
                                            <button class="btn btn-sm btn-round btn-primary btn-fab"
                                                    title="Publish Rank List"><span
                                                        class="fa fa-sort-numeric-asc"></span></button>
                                        </a>
                                        <?php
                                    }
                                    ?>
                                    <a target="_blank" href="/instadmin/ranklist?testid=<?= $test['id'] ?>">
                                        <button class="btn btn-sm btn-round btn-primary btn-fab" title="View Rank List"><span class="fa fa-list"></span></button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo '<tr ><td colspan="4">No tests completed so far</td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
