<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-warning">
        <h4 class="card-title">Questions</h4>
        <p class="card-category">Select class from the list</p>
      </div>

      <div class="container">
      <div class="row">
        <?= form_open(base_url().'instadmin/suboption',array('id'=>'sub-list-form')) ?>
        <input type="hidden" name="class_id" id="bth-sub-id" value="" />
        <?= form_close() ?>
      </div>

      <div class="row">
        <?= form_open(base_url().'instadmin/chapoption',array('id'=>'chap-list-form')) ?>
        <input type="hidden" name="subject_id" id="bth-chap-id" value="" />
        <?= form_close() ?>
      </div>                                                                                                                         
      <div class="row">
        <div class="col-lg-3 col-md-3">
          <div class="form-group"> 
            <select name="class_id" class="form-control sub-fetch">
              <option>Select Class</option>
              <?php foreach ($title as $key) { ?>
              <option value="<?= $key['id']; ?>"><?= $key['title']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 pull-left" >
            <div class="form-group subject-list">
            
            </div>
        </div>

        <div class="col-lg-3 col-md-3 pull-left" >
        <div class="form-group">
            
          <?= form_open('/instadmin/questionget',array('style'=>'width:100%','id'=>'question-get-form')) ?>
          <div class="chapter-list"></div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 pull-left question-fetch-btn" style="display: none">
          <button class="btn btn-primary question-fetch">Fetch Qustions</button>
        </div>
        <?= form_close() ?>

      </div>
      <div class="clearfix"></div>
      <!-- <div class="row"><hr></div> -->
        <div class="row question-list">

        </div>

        <div class="card-footer">
                
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
