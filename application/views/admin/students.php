<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">account_balance</i>
        </div>
        <p class="card-category">Active Courses</p>
        <h3 class="card-title course-number"><?= $this->coursemodel->get_course_count($this->loggedinuser->user['institute'], 'active') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">supervisor_account</i>
        </div>
        <p class="card-category">Active Students</p>
        <h3 class="card-title course-number"><?= $this->coursemodel->get_student_count($this->loggedinuser->user['institute'], 'all') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>

    <div class="col-lg-3 col-md-6 col-sm-6"></div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <br>
        <button class="btn btn-lg btn-primary" style="width: 100%" data-toggle="modal" data-target="#myModal">
            <span class="fa fa-plus"></span>
            Add Students by file
        </button>
        <br>
        <button class="btn btn-lg btn-primary" style="width: 100%" data-toggle="modal" data-target="#studAddModal">
            <span class="fa fa-plus"></span>
            Add Student
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title">Students</h4>
            <p class="card-category">Active Courses</p>
          </div>
            <div class="card-body table-responsive active-courses" style="">
              
                <div class="row">
                  
                  <?= form_open(base_url().'instadmin/batchoption',array('id'=>'batch-get-form')) ?>
                  <input type="hidden" name="course-id" id="bth-course-id" value="" />
                  <?= form_close() ?>
                </div>
                <div class="row">
                  <?= form_open(base_url().'instadmin/studentget',array('style'=>'width:100%','id'=>'student-get-form')) ?>
                  <div class="col-lg-3 col-md-3 col-sm-4 pull-left">
                    <select name="course-id" class="form-control batch-fetch" name="course" required=""  >
                      <option>Select Course :</option>
                      <?php
                      $batches = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'], 'active');
                      foreach ($batches as $batch) {
                        ?>
                        <option value="<?= $batch['id'] ?>"><?= $batch['course_name'] ?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-4 batch-list pull-left" >

                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-4 pull-left stud-fetch-btn" style="display: none">
                    <button class="btn btn-primary student-fetch">Fetch Students Information</button>
                  </div>
                  <?= form_close() ?>
                </div>
                <div class="clearfix"></div>
                <div class="row"><hr></div>
              
              <div class="row stud-list">

              </div>
          
            <div class="card-footer">
                
            </div>
        </div>
      </div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Students by file</h4>
      </div>
      <div class="modal-body">
      <?= form_open(base_url().'instadmin/batchlist',array('id'=>'course-list-form')) ?>
      <input type="hidden" name="course-id" id="course-id" value="">
        <?= form_close() ?>
      <?= form_open_multipart(base_url().'instadmin/addstudents') ?>
        <div class="form-group">
            <select name="course-id" class="form-control course-list" name="syllabus" required="" onchange="" style="width: 100%">
              <option value="">Select Course</option>
              <?php
              $batches = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'], 'active');
              foreach ($batches as $batch) {
                ?>
                <option value="<?= $batch['id'] ?>"><?= $batch['course_name'] ?></option>
                <?php
              }
              ?>
        </select>
      </div>
        <div class="clear-fix"></div>
      <div class="batch-content" style="overflow: hidden;margin-top:25px;">

      </div>

      <button type="submit" class="btn btn-success" ><span class="fa fa-plus"></span> Add Students</button>
      <?= form_close() ?>
      <hr>
        <p>
          Click on the button below to download the student details upload file csv format. Download the file in csv format and upload it corresponding to each batches.
        </p>
        <a target="_blank" href="/files/student_list.csv"><button type="button" class="btn btn-primary" style="float: left;" >Download Student Data Format</button></a>
        <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>
</div>

<!-- Student Add Modal -->
<div id="studAddModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <?= form_open(base_url().'instadmin/batchoptionwithoutall',array('id'=>'course-modal-list-form')) ?>
      <input type="hidden" name="course-id" id="course-modal-id" value="" />
      <?= form_close() ?>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Students Individually</h4>
      </div>
      <form id="addIndStudent" action="/instadmin/addstudent" method="POST">
          <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <select name="course-id" class="form-control course-modal-list" name="course" required=""  >
                      <option>Select Course :</option>
                      <?php
                      $batches = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'], 'active');
                      foreach ($batches as $batch) {
                        ?>
                        <option value="<?= $batch['id'] ?>"><?= $batch['course_name'] ?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                <div class="col-md-6 modal-batch-content" >

                </div>

            </div>
            <br>
            <div class="clear-fix"></div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Name of Student</label>
                <input type="text" required="" name="student-name" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Email of Student</label>
                <input type="email" name="student-email" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Phone Number of Student</label>
                <input type="phone" name="student-phone" class="form-control">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Student Registration Number</label>
                        <input type="phone" required="" name="student-regn" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label">Date of Birth of Student</label>
                        <input type="date" placeholder="" required="" name="student-dob" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Student Address</label>
                <textarea required="" name="student-address" class="form-control"></textarea>
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Name of Parent</label>
                <input type="text" required="" name="parent-name" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Email of Parent</label>
                <input type="email" required="" name="parent-email" class="form-control">
            </div>
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Phone Number of Parent</label>
                <input type="phone" required="" name="parent-phone" class="form-control">
            </div>
          </div>

          <div class="modal-footer">
            <input class="btn btn-primary add-indvidual-student" type="submit" name="add-individual-student" value="Add Student">
          </div>
      </form>

  </div>

</div>
</div>



    <!-- Modal -->
    <div id="studpassreset" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <form id="passresetform" method="post" action="/instadmin/studpassupdate">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reset Password for <span class="studmodalname"></span></h4>
                </div>
                <div class="modal-body">
                    <p>Set new password for <span class="studmodalname"></span></p>
                    <input type="hidden" name="studid" class="studid" value="" />

                        <div class="form-group">
                            <label class="bmd-label-floating">New Password</label>
                            <input type="password" name="password" class="pass form-control">
                        </div>

                        <div class="form-group">
                            <label class="bmd-label-floating">Confirm Password</label>
                            <input type="text" name="cnf-password" class="cnf-pass form-control">
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-smstupassresetbtn">Update Password</button>
                </div>
            </div>
            </form>
        </div>
    </div>