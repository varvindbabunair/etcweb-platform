<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
              <h4 class="card-title">Test Details</h4>
              <p class="card-category">The details and settings of the test to be created</p>
            </div>
            <div class="card-body table-responsive active-courses" >
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Name of Test</label>
                            <input name="testname" class="form-control testname" type="text" value=""  />
                        </div>

                        <select syllabus="" class="form-control course-select">
                            <option value="">Select a Course</option>
                            <?php
                            $course_list = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'],'active');
                            foreach ($course_list as $course){
                                echo '<option syllabus="'.$course['syllabus'].'" value="'.$course['id'].'">';
                                echo $course['course_name'];
                                echo '</option>';
                            }
                            ?>
                        </select>

                        <input type="hidden" class="token" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" />
                        <input type="hidden" name="institute" value="<?= $this->loggedinuser->user['institute'] ?>" class="institute" />

                        <select class="form-control exam-difficulty">
                            <option>Select Hardness of exam</option>
                            <option value="easy">Easy</option>
                            <option value="medium">Medium</option>
                            <option value="hard">Hard</option>
                            <option value="all">All</option>
                        </select>
                    </div>

                    <div class="col-lg-4 batch-list">
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <hr>
                        <div class="clear-fix"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        Marks For Correct Answer:
                        <input type="number" value="4" class="form-control correct-mark" />
                    </div>
                    <div class="col-lg-3">
                        Marks For Wrong Answer:
                        <input type="number" value="1" class="form-control negative-mark" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <hr>
                        <div class="clear-fix"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4><strong>Exam Time and Schedule details :</strong></h4>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam Date and Time</label>
                            <input name="exam-start" class="exam-start form-control datetimepicker" type="text" value="<?= date('d/m/Y') ?>"  />
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam entry time</label>
                            <input name="exam-end" class="exam-end form-control datetimepicker" type="text" value="<?= date('d/m/Y') ?>"  />
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam Duration (minutes)</label>
                            <input name="exam-duration" class="exam-duration form-control" type="text" value="30"  />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <hr>
                        <div class="clear-fix"></div>
                    </div>
                </div>
                <div class="row qn-no-list">
                    <div class="col-md-3 physics">
                        <h4><strong>Physics</strong></h4>
                        Number Of Physics Questions :
                        <input class="form-control phy-qn-no" value="0" name="physics-count" />
                        <div class="clear-fix"></div>
                        <hr>
                        Physics chapters from :
                        <select name="physics-class" class="form-control physics-class">
                            <option value="all">Class 11 and 12</option>
                            <option value="11">Class 11</option>
                            <option value="12">Class 12</option>
                            <option value="none">not selected</option>
                        </select>
                        <div class="accordion" id="accordionExample">
                            <div class="card phy11">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="phy11-check" />
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      <strong>Class 11</strong>
                                      <span class="fa fa-caret-right"></span>
                                    </button>
                                </h5>
                              </div>

                              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover phy11">
                                    <?php
                                    $class11phy = $this->syllabusmodel->class11physics();
                                    foreach($class11phy as $phy11){
                                        ?>

                                        <tr><td>
                                                <input class="phy11-chapter" type="checkbox" name="phy11-<?= $phy11['id'] ?>" value="<?= $phy11['id'] ?>" />
                                        <?= $phy11['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                            <div class="card phy12">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="phy12-check" />
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Class 12</strong>
                                      <span class="fa fa-caret-right"></span>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover phy12">
                                    <?php
                                    $class12phy = $this->syllabusmodel->class12physics();
                                    foreach($class12phy as $phy12){
                                        ?>

                                        <tr><td>
                                                <input class="phy12-chapter" type="checkbox" name="phy12-<?= $phy12['id'] ?>" value="<?= $phy12['id'] ?>" />
                                        <?= $phy12['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                         </div>
                        
                    </div>
                    <div class="col-md-3 chemistry">
                        <h4><strong>Chemistry</strong></h4>
                        Number Of Chemistry Questions :
                        <input class="form-control chem-qn-no" value="0" name="chemistry-count" />
                        <div class="clear-fix"></div>
                        <hr>
                        Chemistry chapters from :
                        <select name="chemistry-class" class="form-control chemistry-class">
                            <option value="all">Class 11 and 12</option>
                            <option value="11">Class 11</option>
                            <option value="12">Class 12</option>
                            <option value="none">not selected</option>
                        </select>
                        <div class="accordion" id="accordionExample">
                            <div class="card chem11">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="chem11-check" />
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsechemOne" aria-expanded="true" aria-controls="collapseOne">
                                      <strong>Class 11</strong>
                                      <span class="fa fa-caret-right"></span>
                                    </button>
                                </h5>
                              </div>

                              <div id="collapsechemOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover chem11">
                                    <?php
                                    $class11phy = $this->syllabusmodel->class11chemistry();
                                    foreach($class11phy as $phy11){
                                        ?>

                                        <tr><td>
                                                <input class="chem11-chapter" type="checkbox" name="chem11-<?= $phy11['id'] ?>" value="<?= $phy11['id'] ?>" />
                                        <?= $phy11['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                            <div class="card chem12">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="chem12-check" />
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsechemTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Class 12</strong>
                                      <span class="fa fa-caret-right"></span>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapsechemTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover chem12">
                                    <?php
                                    $class12phy = $this->syllabusmodel->class12chemistry();
                                    foreach($class12phy as $phy12){
                                        ?>

                                        <tr><td>
                                                <input class="chem12-chapter" type="checkbox" name="chem12-<?= $phy12['id'] ?>" value="<?= $phy12['id'] ?>" />
                                        <?= $phy12['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                         </div>
                    
                    </div>
                    <div class="col-md-3 maths">
                        <h4><strong>Maths</strong></h4>
                        Number Of Maths Questions :
                        <input class="form-control maths-qn-no" value="0" name="maths-count" />
                        <div class="clear-fix"></div>
                        <hr>
                        Maths chapters from :
                        <select name="maths-class" class="form-control maths-class">
                            <option value="all">Class 11 and 12</option>
                            <option value="11">Class 11</option>
                            <option value="12">Class 12</option>
                            <option value="none">not selected</option>
                        </select>
                        <div class="accordion" id="accordionExample">
                            <div class="card maths11">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="maths11-check" />
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsemathsOne" aria-expanded="true" aria-controls="collapseOne">
                                      <strong>Class 11</strong>
                                      <span class="fa fa-caret-right"></span>
                                    </button>
                                </h5>
                              </div>

                              <div id="collapsemathsOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover maths11">
                                    <?php
                                    $class11phy = $this->syllabusmodel->class11maths();
                                    foreach($class11phy as $phy11){
                                        ?>

                                        <tr><td>
                                                <input class="maths11-chapter" type="checkbox" name="maths11-<?= $phy11['id'] ?>" value="<?= $phy11['id'] ?>" />
                                        <?= $phy11['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                            <div class="card maths12">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="maths12-check" />
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsemathsTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Class 12</strong>
                                      <span class="fa fa-caret-right"></span>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapsemathsTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover phy12">
                                    <?php
                                    $class12phy = $this->syllabusmodel->class12maths();
                                    foreach($class12phy as $phy12){
                                        ?>

                                        <tr><td>
                                                <input class="maths12-chapter" type="checkbox" name="maths12-<?= $phy12['id'] ?>" value="<?= $phy12['id'] ?>" />
                                        <?= $phy12['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                         </div>
                    
                    </div>
                    <div class="col-md-3 biology">
                        <h4><strong>Biology</strong></h4>
                        Number Of Biology Questions :
                        <input class="form-control bio-qn-no" value="0" name="biology-count" />
                        <div class="clear-fix"></div>
                        <hr>
                        Biology chapters from :
                        <select name="biology-class" class="form-control biology-class">
                            <option value="all">Class 11 and 12</option>
                            <option value="11">Class 11</option>
                            <option value="12">Class 12</option>
                            <option value="none">not selected</option>
                        </select>
                        <div class="accordion" id="accordionExample">
                            <div class="card bio11">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="bio11-check" />
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsebioOne" aria-expanded="true" aria-controls="collapseOne">
                                      <strong>Class 11</strong>
                                      <span class="fa fa-caret-right"></span>
                                    </button>
                                </h5>
                              </div>

                              <div id="collapsebioOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover bio11">
                                    <?php
                                    $class11phy = $this->syllabusmodel->class11biology();
                                    foreach($class11phy as $phy11){
                                        ?>

                                        <tr><td>
                                                <input class="bio11-chapter" type="checkbox" name="bio11-<?= $phy11['id'] ?>" value="<?= $phy11['id'] ?>" />
                                        <?= $phy11['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                            <div class="card bio12">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <input type="checkbox" class="bio12-check" />
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsebioTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Class 12</strong>
                                      <span class="fa fa-caret-right"></span>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapsebioTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-hover bio12">
                                    <?php
                                    $class12phy = $this->syllabusmodel->class12biology();
                                    foreach($class12phy as $phy12){
                                        ?>

                                        <tr><td>
                                                <input class="bio12-chapter" type="checkbox" name="bio12-<?= $phy12['id'] ?>" value="<?= $phy12['id'] ?>" />
                                        <?= $phy12['title'] ?>
                                            </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </table>
                                </div>
                              </div>
                            </div>
                         </div>
                    
                    </div>
                </div>
                
            </div>
            <div class="card-footer">
                <div class="col-lg-4 col-sm-4"></div>
                <div class="col-lg-4 col-sm-4">
                    <button class="btn btn-primary btn-lg test-create-btn" style="width: 100%;">
                        <span class="fa fa-plus"></span> Create Test
                    </button>
                </div>
                <div class="col-lg-4 col-sm-4"></div>
            </div>
        </div>
      </div>
</div>

