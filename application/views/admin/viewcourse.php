<table class="table">
	<tr><td>Course Name :</td><td><?= $course_name ?></td></tr>
	<tr><td>Course Start Date :</td><td><?= date('d M Y', strtotime($course_start)) ?></td></tr>
	<tr><td>Course End Date :</td><td><?= date('d M Y', strtotime($course_end)) ?></td></tr>
	<tr><td>Syllabus :</td><td><?= ($syllabus == 'medical')?'Medical Entrance Coaching':'' ?><?= ($syllabus == 'eng')?'Engineering Entrance Coaching':'' ?><?= ($syllabus == 'both')?'Both Medical and Engineering Entrance Coaching':'' ?></td></tr>
	<tr><td>Batches :</td><td>
		<?php
		$batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$id);
		$str = '';
		foreach ($batches as $bth) {
			$str .= $bth['batch_name'].', ';
		}
            $str = rtrim($str,',');
            $str = ltrim($str,',');
            echo trim($str);
		?>
	</td></tr>
	<tr><td colspan="2">
		<?= form_open(base_url().'instadmin/editcourse', array('class'=>'pull-right')) ?>
          <input type="hidden" name="course_id" value="<?= $id ?>" />
          <button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" type="submit" title="Edit Course"><span class="fa fa-edit"></span></button>
        <?= form_close() ?>
	</td></tr>
</table>