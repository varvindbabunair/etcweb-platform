<div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
              <h4 class="card-title"><span class="fa fa-plus"></span> Edit Course : <?= $course_name ?></h4>
            <p class="card-category">Add New Courses in your Institute</p>
          </div>
            <?= form_open(base_url().'instadmin/editcourse',array('id' => 'coursesubmitform')) ?>
            <div class="card-body table-responsive" style="">
              <div class="form-group">
                <label class="bmd-label-floating">Course Name</label>
                <input class="form-control course-name" name="course-name" value="<?= $course_name ?>" required="" type="text">
              </div>
              <input type="hidden" name="course_id" value="<?= $id ?>" >
              <div class="form-group">
                  <label class="bmd-label-floating ">Course Start Date</label>
                  <input name="course-start" class="course-start form-control datetimepicker" type="text" value="<?= date('d/m/Y', strtotime($course_start)) ?>"  />
              </div>
              <div class="form-group">
                  <label class="bmd-label-floating">Course End Date</label>
                  <input name="course-end" class="course-end form-control datetimepicker" type="text" value="<?= date('d/m/Y', strtotime($course_end)) ?>" />
              </div>

              <div class="form-group">
                  <label class="bmd-label-floating">Add New Batches (Separated by Comma)</label>
                  <input name="batch-names" class="form-control batch-list tagsinput" />
              </div>
              <hr>
              <div class="clearfix"></div>
              <h4 style="border-bottom:1px solid #ccc;">Existing Batches</h4>
              <?php
              $batches = $this->coursemodel->get_batches($institute,$id);
              $cnt = 1;
              foreach ($batches as $batch) {
                echo '<input class="form-control" name="batch'.$cnt.'" value="'.$batch['batch_name'].'" />';
                echo '<input class="form-control" type="hidden" name="batch'.$cnt.'id" value="'.$batch['id'].'" />';
                $cnt++;
              }
              ?>
              <input type="hidden" name="batch-size" value="<?= sizeof($batches) ?>" >
              <hr>
              <small>Syllabus :</small>
              <div class="form-group">

                  <select class="form-control course-syllabus" name="syllabus" required="" style="width: 100%">
                    <option value="">Choose Syllabus</option>
                    <option <?= ($syllabus == 'eng')?'selected':'' ?> value="eng">Engineering Entrance Syllabus</option>
                    <option <?= ($syllabus == 'medical')?'selected':'' ?> value="medical">Medical Entrance Syllabus</option>
                    <option <?= ($syllabus == 'both')?'selected':'' ?> value="both">Both Medical and Engineering</option>
              </select>
                  <label class="bmd-label-floating"></label>
              </div>
              <div class="form-group bmd-form-group" style="margin-bottom: 10px;">
              <button type="submit" class="btn btn-primary pull-right">Save Changes<div class="ripple-container"></div></button>
              </div>
              <div class="clearfix"></div>
          </div>
            <?php form_close() ?>
        </div>
      </div>
</div>