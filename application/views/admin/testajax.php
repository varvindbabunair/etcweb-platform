<?php
$this->db->where('id',$this->input->post('examid'));
$query = $this->db->from('test_list')->get();
$test_query = $query->result_array();
$exam_info = $test_query[0];

$questions = json_decode(preg_replace("/[\r\n]+/", " ", stripslashes($exam_info['questions'])));

// print_r($questions[2]);
// die();
$where_query ='';
$plusmark = array();
$minusmark = array();
foreach($questions as $section_list){
    foreach($section_list as $question){
        $where_query .= " OR id = '".$question[0]."'";
        $plusmark[$question[0]] = $question[1];
        $minusmark[$question[0]] = $question[2];
    }
}

$where_query = trim($where_query," OR ");
$question_get_query = $this->db->query("SELECT id,question_type,question,answer_option FROM question_list WHERE ".$where_query);

$questions_details = array();

$qns = $question_get_query->result_array();

foreach($qns as $question_info ){
    $questions_details[$question_info['id']] = $question_info;
}


?>



<div class="container-fluid" style="margin-top:20px;">
    <?php
    $sections = json_decode($exam_info['section_title']);
    if(!empty($sections)){
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid #ccc;padding-bottom:10px;">
        <p style="margin-top:-10px;background-color: #fff;float: left;padding:0px 10px;">Sections</p>
        <br>
        <?php
            $sectionnum = 0;
            foreach ($sections as $section){
        ?>
        
        
        <div class="btn-group">
            <button section-target="#section-<?php echo implode('-', explode(' ',$section)); ?>" drop-target="#drop-<?php echo implode('-', explode(' ',$section)); ?>" data-target=".section-<?php echo implode('-', explode(' ',$section)); ?>" type="button" class="btn dropdownbtn <?php echo ($sectionnum == 0)? 'btn-primary':'btn-default'; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $section; ?> <span class="caret"></span>
            </button>
            <ul id="drop-<?php echo implode('-', explode(' ',$section)); ?>" class="dropdown-menu">
                <li><a href="#"><button class="btn btn-xs btn-default"></button> Unattended</a></li>
                <li><a href="#"><button class="btn btn-xs btn-danger"></button> Unanswered</a></li>
                <li><a href="#"><button class="btn btn-xs btn-success"></button> Answered</a></li>
                <li><a href="#"><button class="btn btn-xs btn-info"></button> Marked for Review</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#"><strong>Total : <span class="total-cnt"></span></strong></a></li>
            </ul>
          </div>
        <?php
        $sectionnum++;
            }
        ?>
    </div>
    <?php
    }
    ?>
    
    <div style="width:100%;overflow: hidden;border:1px solid #ccc;">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="border-right:1px solid #ccc;padding-top:20px;padding-bottom:20px;">
        <?php
        $sectionnum = 0;
        foreach ($sections as $section){
        ?>
        <div attended="<?php echo ($sectionnum == 0)?'true':'false'; ?>" id="section-<?php echo implode('-', explode(' ',$section)); ?>" class="section question-section section-<?php echo implode('-', explode(' ',$section)); ?>"  style="width:100%;display: <?php echo ($sectionnum == 0)?'block':'none'; ?>" id="section-<?php echo implode('-', explode(' ',$section)); ?>" >
            <?php
            $questionnum = 1;
            // echo 'section Num = '.$sectionnum.'<br>';
            // print_r($questions[2]);

            foreach($questions[$sectionnum] as $question){
            ?>
            
            <div class="question" question-id="<?php echo $question[0]; ?>" time-spent="0" style="width:100%;display: <?php echo ($questionnum == 1)?'block':'none'; ?>" id="question-<?php echo $question[0]; ?>" >
                <div style="width:100%;overflow: hidden;">
                <strong>Question Number : <?php echo $questionnum; ?></strong>
                <strong class="pull-right">Mark for correct Answer : <?php echo $plusmark[$question[0]]; ?></strong>
                <br>
                <strong class="pull-right">Negative Mark : <?php echo $minusmark[$question[0]]; ?></strong>
                
                </div>
                
                <hr style="margin-top:10px;">
                <?php 
                if($questions_details[$question[0]]['question_type'] != 'assertion'){
                        echo $questions_details[$question[0]]['question'];
                }else{
                        $questions_fet = stripslashes($questions_details[$question[0]]['question']);
                        $qns = json_decode($questions_fet);
                        echo '<h4>Statement 1:</h4>';
                        echo $qns[0][0];
                        echo '<h4>Statement 2:</h4>';
                        echo $qns[1][0];
                }
                ?>
<table class="table table-condensed table-hover" style="margin-top:30px;">
                    
<?php
if($questions_details[$question[0]]['question_type'] != 'numeric'){
    $answers = json_decode(preg_replace("/[\r\n]+/", " ", $questions_details[$question[0]]['answer_option']));
    echo '<tr class="answer" data-target="#palette-'.$question[0].'" ><td valign="centre" style="width:20px;" class=""><input name="answer-'.$question[0].'" type="radio" value="A"  /> </td><td valign="centre" class="">'.$answers[0][1].'</td></tr>';
    echo '<tr class="answer" data-target="#palette-'.$question[0].'" ><td valign="centre" style="width:20px;" class=""><input name="answer-'.$question[0].'" type="radio" value="B"  /> </td><td valign="centre" class="">'.$answers[1][1].'</td></tr>';
    echo '<tr class="answer" data-target="#palette-'.$question[0].'" ><td valign="centre" style="width:20px;" class=""><input name="answer-'.$question[0].'" type="radio" value="C"  /> </td><td valign="centre" class="">'.$answers[2][1].'</td></tr>';
    echo '<tr class="answer" data-target="#palette-'.$question[0].'" ><td valign="centre" style="width:20px;" class=""><input name="answer-'.$question[0].'" type="radio" value="D"  /> </td><td valign="centre" class="">'.$answers[3][1].'</td></tr>';
    
}
?>
</table>

            </div>
            <?php
                $questionnum++;
            }
            ?>
        </div>
        <?php
        $sectionnum++;
        }
        ?>
    <div class="row">
    <div class="col-lg-12">    
        <hr>
    </div>
    <div class="col-lg-12">   
        <button id="clear-answer"  class="btn btn-default">Clear Response</button>
        <button id="review-answer"  class="btn btn-default">Mark for Review and Next</button>
        <button id="view-next" class="btn btn-success pull-right">Save and Next</button>
    </div>
    </div>   
    </div>
    <?php $time_rule = json_decode($exam_info['section_rule']); ?>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="border-left:1px solid #ccc;padding-top:20px;">
        <div style="width:100%;font-size:22px;">
            <strong id="countdowntimer" class=""><span class="fa fa-clock-o"></span> Time Left : <span id="left-time"></span> / <?php echo $time_rule[1]; ?> min</strong>
        </div>
        
        <script type="text/javascript" >
    $(function(){
        jQuery('#left-time').countdowntimer({
            minutes : <?php echo $time_rule[1]; ?>,
            seconds : 0,
            timeUp : timeIsUp,
            beforeExpiryTime : "00:00:05:00",
            beforeExpiryTimeFunction :  beforeExpiryFunc
        });
        function timeIsUp() {
            $('#submitExam').html('Submitting Test');
            alert('Your time is up. Test will now submit');
            testExpired();
        }
        function beforeExpiryFunc() {
		$('#countdowntimer').addClass('redText');
	}
    });
            </script>
        <hr>
        
        <?php
        $sectionnum = 0;
        foreach ($sections as $section){
            ?>
        
        <div class="section section-<?php echo implode('-', explode(' ',$section)); ?>" style="width:100%;text-align:center;display: <?php echo ($sectionnum == 0)?'block':'none'; ?>" >
            <h5 style="text-align:left;">You are viewing <b><?php echo $section; ?></b> section </h5><br>
            
            <h5 ><strong>Question Palette : </strong></h5>
        <?php
            $questionnum = 1;
            foreach($questions[$sectionnum] as $question){
        ?>
            <button id="palette-<?php echo $question[0]; ?>" data-target="#question-<?php echo $question[0]; ?>" class="palette-btn btn btn-sm <?php echo ($questionnum == 1)?'btn-danger':'btn-default'; ?>"><?php echo $questionnum; ?></button>
        <?php
                $questionnum++;
            }
            ?>
        </div>
        <?php
        $sectionnum++;
        }
        ?>
        
        <hr>
        <h4><strong>Legends :</strong></h4>
        <div style="width:50%;float:left;">
        <p><button class="btn btn-xs btn-default">&nbsp;&nbsp;</button> Unattended</p>
        <p><button class="btn btn-xs btn-danger">&nbsp;&nbsp;</button> Unanswered </p>
        </div>
        <div style="width:50%;float:right;">
        <p><button class="btn btn-xs btn-success">&nbsp;&nbsp;</button> Answered </p>
        <p><button class="btn btn-xs btn-info">&nbsp;&nbsp;</button> Marked for Review </p>
        </div>
        <div style="width:100%;overflow: hidden">
            <hr>
        </div>
        <div style="margin-bottom:20px;text-align: center;overflow: hidden;width:100%">
            <button  id="submitExam" class="btn btn-primary">Submit</button>
        </div>
        <form action="/student/submittest" method="POST" id="test-submit" >
            <input type="hidden" name="answer-string" value="" />
            <input type="hidden" name="user" value="<?= $this->loggedinuser->user['id'] ?>" />
            <input type="hidden" name="test" value="<?= $this->input->post('examid') ?>" />
        </form>
    </div>
    </div>
</div>
