<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title">Edit Test</h4>
                <p class="card-category">Modify and update the test details.</p>
            </div>
            <div class="card-body table-responsive active-courses" >
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Name of Test</label>
                            <input name="testname" class="form-control testname" type="text" value="<?= $testdetails['title'] ?>"  />
                        </div>

                        <select syllabus="" class="form-control course-select">
                            <option value="">Select a Course</option>
                            <?php
                            $course_list = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'],'active');
                            foreach ($course_list as $course){
                                echo '<option ';
                                echo ($course['id'] == $testdetails['course'])? 'selected=""':'';
                                echo 'syllabus="'.$course['syllabus'].'" value="'.$course['id'].'">';
                                echo $course['course_name'];
                                echo '</option>';
                            }
                            ?>
                        </select>

                        <input type="hidden" class="token" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" />
                        <input type="hidden" name="institute" value="<?= $this->loggedinuser->user['institute'] ?>" class="institute" />

                    </div>

                    <div class="col-lg-4 batch-list">
                        <?php
                        $batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$testdetails['course']);

                        $testsForBatches = $this->coursemodel->get_batches_in_test($testdetails['id']);

                        $str = '';
                        echo '<h5>Batches<h5>';
                        echo '<table class="table table-hover">';
                        foreach ($batches as $batch) {
                            echo '<tr><td><input class="batch-check" type="checkbox" ';
                            if(in_array($batch['id'],$testsForBatches['id'])){
                                echo 'checked';
                            }
                            echo ' name="batch-'.$batch['id'].'" value="'.$batch['id'].'" />';
                            echo ' '.$batch['batch_name'].'</td></tr>';
                            $str .= $batch['id'].',';
                        }
                        echo '</table>';
                        echo '<input type="hidden" value="'.rtrim($str,',').'" />';
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <hr>
                        <div class="clear-fix"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4><strong>Exam Time and Schedule details :</strong></h4>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam Date and Time</label>
                            <input name="exam-start" class="exam-start form-control datetimepicker" type="text" value="<?= date('d/m/Y g:i:s A',strtotime(str_replace('-','/',$testdetails['exam_start']))) ?>"  />
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam entry time</label>
                            <input name="exam-end" class="exam-end form-control datetimepicker" type="text" value="<?= date('d/m/Y g:i:s A',strtotime(str_replace('-','/',$testdetails['exam_entry']))) ?>"  />
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="bmd-label-floating ">Exam Duration (minutes)</label>
                            <input name="exam-duration" class="exam-duration form-control" type="text" value="30"  />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <hr>
                        <div class="clear-fix"></div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <h4><strong>Questions</strong></h4>
                    <input type="hidden" class="qns-final" value="<?= str_replace('"','\'',$testdetails['questions']) ?>" />
                    <input type="hidden" class="testid" value="<?= $testdetails['id'] ?>" />
                </div>
                <?php
                    $sections = json_decode($testdetails['section_title']);
                    $questions = json_decode($testdetails['questions']);
                    $sectioncnt = 0;
                    foreach ($sections as $section){
                        ?>
                <div class="col-lg-12">
                    <div class="accordion" id="accordionExample">
                    <div class="card chem11">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0" data-toggle="collapse" data-target="#collapsechem<?= $sectioncnt ?>" aria-expanded="true" aria-controls="collapseOne">
                                <button class="btn btn-link" type="button" >
                                    <strong><?= $section ?></strong> &nbsp;
                                    <span class="fa fa-caret-right"></span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapsechem<?= $sectioncnt ?>" class="collapse" aria-labelledby="heading<?= $sectioncnt ?>" data-parent="#accordionExample">
                            <div class="col-lg-12">
                            <?php
                            $this->db->from('question_list');
                            foreach($questions[$sectioncnt] as $qnid){
                                $this->db->or_where('id',$qnid[0]);
                            }
                            $qnqry = $this->db->get();
                            $fetched_questions = $qnqry->result_array();

                            echo '<table class="table">';
                            $c=1;
                            foreach($fetched_questions as $question){
                                ?>
                                <tr>
                                    <td><?= $c ?></td>
                                    <td class="replace-<?= $question['id'] ?>"><?= $question['question'] ?></td>
                                    <td>
                                        <button replaceid="<?= $question['id'] ?>" class="btn btn-fab btn-primary btn-round btn-sm replace-qn" onclick="viewChapters('<?= ($section === 'Mathematics')?'Maths':$section ?>',<?= $question['id'] ?>)" replacesubject="<?= strtolower($section) ?>" replaceId="<?= $question['id'] ?>" title="Replace Question"><span class="fa fa-sign-out"></span></button>
                                    </td>
                                </tr>
                            <?php
                                $c++;
                            }
                            echo '</table>';
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                        <?php
                        $sectioncnt++;
                    }
                ?>
            </div>
            <div class="card-footer">
                <div class="col-lg-4 col-sm-4"></div>
                <div class="col-lg-4 col-sm-4">
                    <button class="btn btn-primary btn-lg test-update-btn" style="width: 100%;">
                        Update Test
                    </button>
                </div>
                <div class="col-lg-4 col-sm-4"></div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="questionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title question-modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-6">
                    <select class="form-control chapteropt">

                    </select>
                </div>
                <div class="col-lg-12 question-modal-content">

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="replace-qn" name="replace-qn" value="" />
                <button type="button" class="btn btn-primary qn-replace">Replace With This Question</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

