<?php
$cnt=0;
foreach ($questions as $qn) {
?>
	<div id="<?= $qn['id'] ?>" style="<?= ($cnt!=0)?'display:none;':'' ?>overflow: hidden;" class="question-content qn-<?= $cnt ?>">
        <h4 style="font-weight: bold; text-decoration: underline;">Question :</h4>
        <div class="qnreplace-<?= $qn['id']  ?>">

			<?= ($qn['question_type'] != 'assertion')? $qn['question'] : '<h4>Statement 1</h4>'.json_decode($qn['question'])[0].'<h4>Statement 2<h4>'.json_decode($qn['question'])[1] ?>
			<div class="clear-fix"></div>
		</div>
		<div>

			<h4 style="font-weight: bold; text-decoration: underline;">Answer Options :</h4>
			<?php
			$answer = json_decode(preg_replace("/[\r\n]+/", " ", $qn['answer_option']));
			?>
			<ol type="A">
				<li><?= $answer[0][1] ?></li>
				<li><?= $answer[1][1] ?></li>
				<li><?= $answer[2][1] ?></li>
				<li><?= $answer[3][1] ?></li>
			</ol>
			<div class="clear-fix"></div>
		</div>
		<div >
			<h4 style="font-weight: bold;">Correct Answer: <?= $qn['correct_answer'] ?></h4>
			<div class="clear-fix"></div>
		</div>
			<div class="clear-fix"></div>
	</div>
<?php
	$cnt++;
}
?>


<hr>
<nav aria-label="Page navigation" style="width: 100%;overflow: auto">
<ul class="pagination">
<?php
$count = sizeof($questions);
for ($i = 0; $i < $cnt ; $i++) { 
?>
    <li qn-page="<?= $i ?>" class="page-item <?= ($i==0)?'active':'' ?>">
      <a class="page-link" href="#"><?= $i+1 ?></a>
    </li>

<?php
}
?>
  </ul>
</nav>