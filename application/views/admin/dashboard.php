<div class="row">
  
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <p class="card-category">Tests Scheduled</p>
        <h3 class="card-title">
            <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'active' ) ?>
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">assignment</i>
            <a href="/instadmin/tests">View all tests report</a>
        </div>
      </div>
    </div>
  </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Today</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'today' ) ?>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View all tests report</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">timeline</i>
                </div>
                <p class="card-category">Ongoing Tests</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'ongoing' ) ?>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View all tests report</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <br>
        <a href="/instadmin/createtest" >
            <button class="btn btn-lg btn-primary" style="width: 100%">
                <span class="fa fa-plus"></span>
                Create Test
            </button>
        </a>
    </div>
</div>

<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">account_balance</i>
                </div>
                <p class="card-category">Live Courses</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_course_count($this->loggedinuser->user['institute'],'active' ) ?>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">account_balance</i>
                    <a href="/instadmin/course">Manage Courses</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">supervisor_account</i>
                </div>
                <p class="card-category">Active Students</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_student_count($this->loggedinuser->user['institute'],'all' ) ?>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">supervisor_account</i>
                    <a href="/instadmin/students">Manage Students</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">account_circle</i>
                </div>
                <p class="card-category">Active Faculties</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_faculty_count($this->loggedinuser->user['institute'],'active' ) ?>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">account_circle</i>
                    <a href="/instadmin/faculty">Manage Faculties</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Scheduled Today</p>
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <thead>
                    <th>#</th>
                    <th>Test</th>
                    <th>Completion Status</th>
                    </thead>
                    <tbody>

                <?php
                $cnt = 1;
                $tests_today = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'today');
                if(sizeof($tests_today) > 0){

                foreach ($tests_today as $tests){
                ?>
                <tr>
                    <td><?= $cnt ?></td>
                    <td><?= $tests['title'] ?></td>
                    <td>
                        <?php
                        $status = $this->coursemodel->get_test_status($tests['id']);
                        echo $status['attended'] .'/'.$status['total'];
                        ?>
                    </td>
                </tr>
                <?php
                    $cnt++;
                }
                }else{
                    echo'<tr><td colspan="4">No Tests scheduled today</td></tr>';
                }
                ?>

                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View Full Test Reports</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Ongoing Tests Status</p>
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <thead>
                    <th>#</th>
                    <th>Test</th>
                    <th>Completion Status</th>
                    </thead>
                    <tbody>

                    <?php
                    $cnt = 1;
                    $tests_today = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'ongoing');
                    if(sizeof($tests_today) > 0){

                        foreach ($tests_today as $tests){
                            ?>
                            <tr>
                                <td><?= $cnt ?></td>
                                <td><?= $tests['title'] ?></td>
                                <td>
                                    <?php
                                    $status = $this->coursemodel->get_test_status($tests['id']);
                                    echo $status['attended'] .'/'.$status['total'];
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $cnt++;
                        }
                    }else{
                        echo'<tr><td colspan="4">No Tests Ongoing</td></tr>';
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">timeline</i>
                    <a href="/instadmin/testreport">View Full Test Reports</a>
                </div>
            </div>
        </div>
    </div>
</div>
