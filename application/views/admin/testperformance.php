<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Dashboard
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->


    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->

    <link href="<?= base_url() ?>css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />

    <link href="<?= base_url() ?>css/dashboard.css" rel="stylesheet" />
    <link href="<?= base_url() ?>css/bootstrap-tagsinput.css" rel="stylesheet" />


</head>
<body>
<?php
if(!$this->input->get_post('testid')){
    ?>
    <h4 class="align-middle">Some Error Occured while fetching test report</h4>
    <?php
}else{
    $testid = $this->input->get_post('testid');
    $ranklist = $this->coursemodel->get_ranklist($testid);
    $testdetails = $this->coursemodel->get_test($this->loggedinuser->institute['id'],null,null,$testid);
    ?>
    <table class="table">
        <tr><td colspan="3" style="text-align: center">
                <img style="height: 50px;" src="/images/institutes/<?= $this->loggedinuser->institute['logo'] ?>" />
                <span style="font-weight: bolder;font-size: 20px"><?= $this->loggedinuser->institute['name'] ?></span>

                <h3 style="font-weight: bold"><?= $testdetails['title'] ?></h3>
                <strong>Date: <?= date('d M Y', strtotime($testdetails['exam_start'])) ?></strong>
            </td></tr>
    </table>
    <?php
    if(count($ranklist) > 0) {
        foreach ($ranklist as $rank) {
            echo '<div class="container"><div class="col-lg-12" style="margin:20px;border:1px solid #ddd;-webkit-border-radius: 12px;-moz-border-radius: 12px;border-radius: 12px;">';
            ?>

            <?php
            $answer_get_query = $this->db->query("SELECT * FROM answer_list WHERE test = " . $testdetails['id'] . " AND user = " . $rank['id']);
            $ans = $answer_get_query->result_array();

            $test = $this->input->post('test');
            $answer = str_replace('\'', '"', $ans[0]['answer']);


            $test_get_query = $this->db->query("SELECT questions FROM test_list WHERE id = '" . $ans[0]['test'] . "'");

            $test_writ = $test_get_query->result_array();
            $test_written = $test_writ[0];

            $answered = json_decode($answer);
            $original_answer = json_decode($test_written['questions']);

            $qn_qry = "SELECT id,correct_answer FROM question_list WHERE";
            $section_num = 0;
            foreach ($original_answer as $section) {
                $individual_answer_num = 0;
                foreach ($section as $qn) {
                    $qn_id = $original_answer[$section_num][$individual_answer_num][0];
                    $qn_qry .= " id = '$qn_id' OR";
                    $individual_answer_num++;
                }
                $section_num++;
            }
            $qn_qry = trim($qn_qry, ' OR');

            $question_get_query = $this->db->query($qn_qry);

            $ans_arr = array();
            $qn_infos = $question_get_query->result_array();
            foreach ($qn_infos as $qn_info) {
                $ans_arr[$qn_info['id']] = $qn_info['correct_answer'];
            }


            $positive_marks = 0;
            $negative_marks = 0;

            $questions_answered = 0;
            $correctly_answered = 0;
            $attended_question = 0;

            $time_on_unanswered = 0;
            $time_on_correct = 0;
            $time_on_wrong = 0;
            $total_time = 0;

            $section_num = 0;
            $total_questions = 0;
            foreach ($original_answer as $section_answer) {
                $individual_answer_num = 0;
                foreach ($section_answer as $individual_answer) {
                    $qn_id = $answered[$section_num][$individual_answer_num][0];
                    if ($answered[$section_num][$individual_answer_num][1] == $ans_arr[$qn_id]) {
                        $questions_answered++;
                        $correctly_answered++;
                        $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
                        $positive_marks = $positive_marks + (int)$original_answer[$section_num][$individual_answer_num][1];
                        $attended_question++;
                        $time_on_correct = $time_on_correct + (int)$answered[$section_num][$individual_answer_num][2];
                    } else if ($answered[$section_num][$individual_answer_num][1] == '') {
                        $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
                        if ((int)$answered[$section_num][$individual_answer_num][2] > 0) {
                            $attended_question++;
                            $time_on_unanswered = $time_on_unanswered + (int)$answered[$section_num][$individual_answer_num][2];
                        }
                    } else if ($answered[$section_num][$individual_answer_num][1] != $ans_arr[$qn_id]) {
                        $questions_answered++;
                        $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
                        $negative_marks = $negative_marks + (int)$original_answer[$section_num][$individual_answer_num][2];
                        $attended_question++;
                        $time_on_wrong = $time_on_wrong + (int)$answered[$section_num][$individual_answer_num][2];
                    }
                    $individual_answer_num++;
                    $total_questions++;
                }
                $section_num++;
            }
            ?>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">grade</i>
                            </div>
                            <p class="card-category">Mark</p>
                            <h3 class="card-title"><?= $positive_marks - $negative_marks; ?></h3>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <h4 class="align-middle">Accuracy</h4>

                    <?php

                    $accuracy = ($questions_answered == 0) ? $questions_answered : round(($correctly_answered / $questions_answered) * 100, 2);

                    ?>
                    <div class="card-title accuracy" id="accuracy" value="<?= $accuracy ?>"></div>

                    <script type="text/javascript">
                        google.charts.load('current', {'packages': ['gauge']});
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            var data = google.visualization.arrayToDataTable([
                                ['Label', 'Value'],
                                ['%', <?= $accuracy ?>]
                            ]);

                            var options1 = {
                                maxWidth: '100%',
                                greenFrom: 90, greenTo: 100,
                                yellowFrom: 75, yellowTo: 90,
                                minorTicks: 5
                            };

                            var chart = new google.visualization.Gauge(document.getElementById('accuracy'));

                            chart.draw(data, options1);
                        }
                    </script>


                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <h4>Avg Time / Question</h4>

                    <div class="card-title accuracy" id="timeforqn" value="<?= $accuracy ?>"></div>

                    <script type="text/javascript">
                        google.charts.load('current', {'packages': ['gauge']});
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            var data1 = google.visualization.arrayToDataTable([
                                ['Label', 'Value'],
                                ['seconds', <?= round($total_time / $attended_question, 2) ?>]
                            ]);

                            var options2 = {
                                maxWidth: '100%',
                                greenFrom: 0, greenTo: 50,
                                yellowFrom: 50, yellowTo: 60,
                                redFrom: 60, redTo: 100,
                                max: 70,
                                minorTicks: 5
                            };

                            var chart = new google.visualization.Gauge(document.getElementById('timeforqn'));

                            chart.draw(data1, options2);
                        }
                    </script>


                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h4><strong>Results Analysis</strong></h4>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <script type="text/javascript">
                        google.charts.load("current", {packages: ["corechart"]});
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Answers', 'Number'],
                                ['Correctly Answered', <?= $correctly_answered; ?>],
                                ['Wrong Answers', <?= $questions_answered - $correctly_answered; ?>],
                                ['Attended but Not Answered', <?= $attended_question - $questions_answered ?>],
                                ['Not Attended', <?= $total_questions - $attended_question ?>]
                            ]);

                            var options = {
                                title: 'Questions Overview',
                                legend: 'bottom',
                                maxWidth: '100%',
                                pieHole: 0.4,
                                colors: ['#00e00f', '#e6030b', '#407ff3', '#d5d4ec']
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('qnanalysischart'));
                            chart.draw(data, options);
                        }
                    </script>
                    <div id="qnanalysischart"></div>


                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <script type="text/javascript">
                        google.charts.load("current", {packages: ["corechart"]});
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Answers', 'Number'],
                                ['Correctly Answered', <?= $time_on_correct ?>],
                                ['Wrongly Answered', <?= $time_on_wrong ?>],
                                ['Unanswered', <?= $time_on_unanswered ?>]
                            ]);

                            var options = {
                                title: 'Time Spent on Questions',
                                legend: 'bottom',
                                maxWidth: '100%',
                                pieHole: 0.4,
                                colors: ['#00e00f', '#e6030b', '#d5d4ec']
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('timeanalysischart'));
                            chart.draw(data, options);
                        }
                    </script>
                    <div id="timeanalysischart"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>


            <?php
            echo '</div></div>';
        }
    }else{
        echo '<div class="container">No students attended the test</div>';
    }
}
?>
</body>
</html>