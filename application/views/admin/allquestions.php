
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title">Questions</h4>
                <p class="card-category">Questions from Class <?= $classname ?>, <?= $subjectname ?> - <?= $chaptername ?> </p>
            </div>
            <div class="card-body table-responsive active-courses" >
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Question</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$cnt=1;
foreach ($questions as $qn) {
?>
	<tr>
        <td><?= $cnt; ?></td>
		<td>
			<?= ($qn['question_type'] != 'assertion')? $qn['question'] : '<h4>Statement 1</h4>'.json_decode($qn['question'])[0].'<h4>Statement 2<h4>'.json_decode($qn['question'])[1] ?>

		</td>
        <td>
            <button class="btn btn-primary btn-fab btn-sm btn-round" title="Add to Test"><span class="fa fa-plus"></span></button>
            <button class="btn btn-primary btn-fab btn-sm btn-round" title="Veiw Question"><span class="fa fa-eye"></span></button>
        </td>
    </tr>
<?php
	$cnt++;
}
?>
                    </tbody>
                </table>
        </div>
    </div>
