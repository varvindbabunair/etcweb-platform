<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Dashboard
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->


    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->

    <link href="<?= base_url() ?>css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />

    <link href="<?= base_url() ?>css/dashboard.css" rel="stylesheet" />
    <link href="<?= base_url() ?>css/bootstrap-tagsinput.css" rel="stylesheet" />


</head>
<body>
<?php
if(!$this->input->get_post('testid')){
    ?>
    <h4 class="align-middle">Some Error Occured while fetching ranklist</h4>
<?php
}else{
    $testid = $this->input->get_post('testid');
    $ranklist = $this->coursemodel->get_ranklist($testid);
    $testdetails = $this->coursemodel->get_test($this->loggedinuser->institute['id'],null,null,$testid);
    ?>
    <table class="table">
        <tr><td colspan="3" style="text-align: center">
                <img style="height: 50px;" src="/images/institutes/<?= $this->loggedinuser->institute['logo'] ?>" />
                <span style="font-weight: bolder;font-size: 20px"><?= $this->loggedinuser->institute['name'] ?></span>

                <h3 style="font-weight: bold"><?= $testdetails['title'] ?></h3>
                <strong>Date: <?= date('d M Y', strtotime($testdetails['exam_start'])) ?></strong>
            </td></tr>
        <tr>
        <th>Rank</th>
        <th>Name</th>
        <th>Mark</th>
        </tr>
        <?php
        $rank = 1;
        foreach($ranklist as $ranks){
            ?>
            <tr>
                <td><?= $rank ?></td>
                <td><?= $ranks['name'] ?></td>
                <td><?= $ranks['total_marks'] ?></td>
            </tr>
        <?php
            $rank++;
        }
        ?>
    </table>
<?php
}
?>
</body>
</html>