<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <p class="card-category">Tests Completed</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'inactive' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Today</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'today' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <p class="card-category">Tests Scheduled</p>
                <h3 class="card-title">
                    <?= $this->coursemodel->get_test_count($this->loggedinuser->user['institute'],'upcoming' ) ?>
                </h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <br>
        <a href="/instadmin/createtest" >
        <button class="btn btn-lg btn-primary" style="width: 100%">
            <span class="fa fa-plus"></span>
            Create Test
        </button>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title">Completed Tests</h4>
                <p class="card-category">Reports and Status of Upcoming Tests</p>
            </div>
            <div class="card-body table-responsive active-courses" style="max-height: 500px;overflow: auto;">

                <table class="table table-hover">
                    <thead>
                    <th>Test Name</th>
                    <th>Time Details</th>
                    <th>Course & Batch</th>
                    <th>Options</th>
                    </thead>
                    <tbody class="active-list">
                    <?php
                    $tests = $this->coursemodel->get_institute_test($this->loggedinuser->user['institute'],'upcoming');
                    if($tests){
                        foreach($tests as $test){
                            ?>
                            <tr>
                                <td><?= $test['title'] ?></td>
                                <td>
                                    <span title="Start Time" class="fa fa-hourglass-start"></span>
                                    <?= date('d M Y g:i:s A', strtotime($test['exam_start'])) ?><br>
                                    <span title="End Time" class="fa fa-hourglass-end"></span>
                                    <?= date('d M Y g:i:s A', strtotime($test['exam_entry'])) ?></td>
                                <td><?php
                                    $course = $this->coursemodel->get_course($this->loggedinuser->user['institute'], $test['course'] );
                                    echo $course['course_name'];
                                    ?></td>
                                <td>
                                    <a href="/instadmin/testpreview?testid=<?= $test['id'] ?>">
                                        <button class="btn btn-sm btn-round btn-primary btn-fab" title="Edit Test"><span class="fa fa-edit"></span></button>
                                    </a>

                                    <button class="btn btn-sm btn-round btn-primary btn-fab" title="Remove Test"><span class="fa fa-remove"></span></button>


                                </td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo '<tr ><td colspan="4">No tests completed so far</td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>

