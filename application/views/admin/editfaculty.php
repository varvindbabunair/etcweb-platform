<?= form_open_multipart('/instadmin/facultyupdate') ?>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Edit Faculty</h4>
                <p class="card-category">Update and Edit Faculty Details</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img class="img img-thumbnail" src="/images/user/<?= $faculty_details->image ?>" />
                        <input type="file" name="image" />
                    </div>
                    <input type="hidden" name="facid" value="<?= $faculty_details->id ?>" />
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Name</label>
                                    <input type="text" name="name" class="form-control" value="<?= $faculty_details->name ?>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Date of Birth</label>
                                    <input type="date" name="dob" class="form-control" value="<?= date('Y-m-d',strtotime(str_replace('-','/',$faculty_details->dob))) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email</label>
                                    <input type="text" name="email" class="form-control" value="<?= $faculty_details->email?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Phone</label>
                                    <input type="text" name="phone" class="form-control" value="<?= $faculty_details->phone ?>">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <h6><strong><br><br></strong></h6>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Address</label>
                            <input type="text" name="address" class="form-control" value="<?= $faculty_details->address ?>">
                        </div>
                    </div>
                </div>


                <button type="submit profile-submit" type="submit" class="btn btn-primary pull-right">Update Details</button>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>
</form>
