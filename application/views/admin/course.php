<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-info card-header-icon">
        <div class="card-icon">
          <i class="material-icons">account_balance</i>
        </div>
        <p class="card-category">All Courses</p>
        <h3 class="card-title course-number"><?= $this->coursemodel->get_course_count($this->loggedinuser->user['institute'], 'all') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">account_balance</i>
        </div>
        <p class="card-category">Active Courses</p>
        <h3 class="card-title course-number"><?= $this->coursemodel->get_course_count($this->loggedinuser->user['institute'], 'active') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">account_balance</i>
        </div>
        <p class="card-category">Inactive Courses</p>
        <h3 class="card-title"><?= $this->coursemodel->get_course_count($this->loggedinuser->user['institute'], 'inactive') ?>
        </h3>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title">Courses</h4>
            <p class="card-category">Active Courses</p>
          </div>
            <div class="card-body table-responsive active-courses" style="height: 330px;overflow-y: scroll;">
              <table class="table table-hover">
                  <thead>
                        <th>Course</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Options</th>
                  </thead>
                  <tbody class="active-list">
                      <?php
                      $courses = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'],'active');
                      if($courses){
                      foreach($courses as $course){
                      ?>
                      <tr>
                          <td><?= $course['course_name'] ?></td>
                          <td><?= date('d M Y', strtotime($course['course_start'])) ?></td>
                          <td><?= date('d M Y', strtotime($course['course_end'])) ?></td>
                          <td>
                            <?= form_open(base_url().'instadmin/editcourse', array('id' => 'course-edit-btn','class'=>'pull-left')) ?>
                              <input type="hidden" name="course_id" value="<?= $course['id'] ?>" />
                              <button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" type="submit" title="Edit Course"><span class="fa fa-edit"></span></button>
                            <?= form_close() ?>

                            <?= form_open(base_url().'instadmin/viewcourse', array('id' => 'courseviewform')) ?>
                              <input type="hidden" name="course_id" value="<?= $course['id'] ?>" />
                              <button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" type="submit" title="View Course"><span class="fa fa-columns"></span></button>
                            <?= form_close() ?>
                          </td>
                      </tr>
                      <?php
                      }
                      }else{
                          echo '<tr ><td colspan="4">No course added</td></tr>';
                      }
                      ?>
                  </tbody>
              </table>
          </div>
            <div class="card-footer">
                
            </div>
        </div>
      </div>
    
    <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
              <h4 class="card-title"><span class="fa fa-plus"></span> Add Course</h4>
            <p class="card-category">Add New Courses in your Institute</p>
          </div>
            <?= form_open(base_url().'instadmin/submitcourse',array('id' => 'coursesubmitform')) ?>
            <div class="card-body table-responsive" style="">
              <div class="form-group">
                <label class="bmd-label-floating">Course Name</label>
                <input class="form-control course-name" name="course-name" required="" type="text">
              </div>
              <div class="form-group">
                  <label class="bmd-label-floating ">Course Start Date</label>
                  <input name="course-start" class="course-start form-control datetimepicker" type="text" value="<?= date('d/m/Y') ?>"  />
              </div>
              <div class="form-group">
                  <label class="bmd-label-floating">Course End Date</label>
                  <input name="course-end" class="course-end form-control datetimepicker" type="text" value="<?= date('d/m/Y', strtotime('+1 Year')) ?>" />
              </div>
              <div class="form-group">
                  <label class="bmd-label-floating">Name of Batches (Separated by Comma)</label>
                  <input name="batch-names" class="form-control batch-list tagsinput" required="" />

              </div>
              <div class="form-group">
                  <select class="form-control course-syllabus" name="syllabus" required="" style="width: 100%">
                    <option value="">Choose Syllabus</option>
                    <option value="eng">Engineering Entrance Syllabus</option>
                    <option value="medical">Medical Entrance Syllabus</option>
                    <option value="both">Both Medical and Engineering</option>
              </select>
                  <label class="bmd-label-floating"></label>
              </div>
              <div class="form-group bmd-form-group" style="margin-bottom: 10px;">
              <button type="submit" class="btn btn-primary pull-right"><span class="fa fa-plus"></span> Add Course<div class="ripple-container"></div></button>
              </div>
              <div class="clearfix"></div>
          </div>
            <?php form_close() ?>
        </div>
      </div>
</div>


<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Inactive Courses</h4>
            <p class="card-category">Inactive / Expired Courses</p>
          </div>
            <div class="card-body table-responsive " style="height: 300px;overflow-y: scroll;">
              <table class="table table-hover">
                  <thead>
                        <th>Course</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                  </thead>
                  <tbody>
                      <?php
                      $courses = $this->coursemodel->fetch_course($this->loggedinuser->user['institute'],'inactive');
                      if($courses){
                      foreach($courses as $course){
                      ?>
                      <tr>
                          <td><?= $course['course_name'] ?></td>
                          <td><?= date('d M Y', strtotime($course['course_start'])) ?></td>
                          <td><?= date('d M Y', strtotime($course['course_end'])) ?></td>
                      </tr>
                      <?php
                      }
                      }else{
                          echo '<tr ><td colspan="4">No course added</td></tr>';
                      }
                      ?>
                  </tbody>
              </table>
          </div>
            <div class="card-footer">
                
            </div>
        </div>
      </div>
</div>

<div id="courseViewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Course Details</h4>
      </div>
      <div class="course-view-modal-content modal-body">
        <p>Some error while fetching course details</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>