<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Edit Profile</h4>
                <p class="card-category">Update your profile details</p>
            </div>
            <div class="card-body">
                <?= form_open_multipart('instadmin/institutesubmit') ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <img class="img img-thumbnail" src="/images/institutes/<?= $this->loggedinuser->institute['logo'] ?>" />
                        <input type="file" name="image" />
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Name</label>
                                    <input type="text" name="name" class="form-control" value="<?= $this->loggedinuser->institute['name'] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Tagline</label>
                                    <input type="text" name="tagline" class="form-control" value="<?= $this->loggedinuser->institute['tagline'] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Website</label>
                                    <input type="text" name="website" class="form-control" value="<?= $this->loggedinuser->institute['website'] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email</label>
                                    <input type="text" name="email" class="form-control" value="<?= $this->loggedinuser->institute['email'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Phone</label>
                                    <input type="text" name="phone" class="form-control" value="<?= $this->loggedinuser->institute['phone'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Address</label>
                                    <input type="text" name="address" class="form-control" value="<?= $this->loggedinuser->institute['address'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <button type="submit profile-submit" type="submit" class="btn btn-primary pull-right">Update Profile</button>
                <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-profile">
            <div class="card-body">
                    <h6 class="card-category text-gray">
                        Login Link for students and faculty:
                    </h6>
                    <p>
                        <a href="#">
                            http://etcweb.entranceengine.com/login?instkey=<span class="inst-key"><?= $this->loggedinuser->institute['instkey'] ?></span>
                        </a>
                    </p>
                    <h6>Update Institute login Link</h6>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Institute Login Key</label>
                            <input type="text" name="instkey" class="form-control newinstkey" value="<?= $this->loggedinuser->institute['instkey'] ?>">
                            <span class="submit-msg"></span>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-left instkeyupdate">Update Link</button>
                </div>

        </div>
    </div>
</div>

