<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#ecf5fe" />
        
	<title>ETCWEB - Complete Solution to manage your Education, Training and Curriculum</title>

	<link rel="stylesheet" type="text/css" href="css/theme-styles.css">
	<link rel="stylesheet" type="text/css" href="css/blocks.css">
	<link rel="stylesheet" type="text/css" href="css/widgets.css">


	<!--External fonts-->

	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Paaji" rel="stylesheet">

	<!-- Styles for Plugins -->
	<link rel="stylesheet" type="text/css" href="css/swiper.min.css">

	<!--Styles for RTL-->

	<!--<link rel="stylesheet" type="text/css" href="css/rtl.css">-->


</head>
<body class="crumina-grid">

<!-- Preloader -->

<div id="hellopreloader" style="display: block; position: fixed;z-index: 99999;top: 0;left: 0;width: 100%;height: 100%;min-width: 100%;background: #66b5ff url(svg/preload.svg) center center no-repeat;background-size: 41px;opacity: 1;"></div>

<!-- ... end Preloader -->


<!-- Header -->

<header class="header header--menu-rounded header--blue-lighteen" id="site-header">

	<div class="header-lines-decoration">
		<span class="bg-secondary-color"></span>
		<span class="bg-blue"></span>
		<span class="bg-blue-light"></span>
		<span class="bg-orange-light"></span>
		<span class="bg-red"></span>
		<span class="bg-green"></span>
		<span class="bg-secondary-color"></span>
	</div>

	<div class="container">

		<a href="#" id="top-bar-js" class="top-bar-link"><svg class="utouch-icon utouch-icon-arrow-top"><use xlink:href="#utouch-icon-arrow-top"></use></svg></a>
			<div class="header-content-wrapper">

			<div class="site-logo">
                            <a href="#" class="full-block"></a>
                                <img style="width: 50px" src="/images/institutes/<?= $logo ?>" alt="<?= $name ?>">
				<div class="logo-text">
					<div class="logo-title"><?= $name ?></div>
					<div class="logo-sub-title"><?= $tagline ?></div>
				</div>
			</div>

			<nav id="primary-menu" class="primary-menu" data-dropdown-animation="fade">

				<!-- menu-icon-wrapper -->


				<ul class="primary-menu-menu">
					<li class="">
						<a href="#"></a>
					</li>
				</ul>
				</nav>

			</div>

		</div>

</header>

<div class="header-spacer"></div>

<!-- ... End Header -->