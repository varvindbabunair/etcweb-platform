<div class="content-wrapper">


	<!-- Info Boxes -->

	<section class="bg-blue-lighteen medium-padding120">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-3 col-sm-2 col-xs-12">
				
				</div>
				<div class="col-lg-4 col-md-6 col-sm-8 col-xs-12">
                                    <h4 class="heading-title" style="text-align: center">Login</h4>
                                    <?php
                                    if($param == 'fail'){
                                        ?>
                                    <div class="alert alert-warning alert-dismissible">
                                        <a href="#" class="close pull-right" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Login Failed.</strong> Invalid Credentials.
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <?= form_open(base_url().'login?instkey='.$this->input->get('instkey'),array('id'=>'login-form')) ?>
                                        <div class="with-icon">
                                            <input name="login" placeholder="Register Number" name="login" type="text" required="required">
                                            <svg class="utouch-icon utouch-icon-user"><use xlink:href="#utouch-icon-user"></use></svg>
                                        </div>
                                        <div class="with-icon">
                                            <input name="password" placeholder="Password" name="password" type="password" required="required">
                                            <svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
                                        </div>
                                        <input class="btn btn--green-light form-control" name="login-submit" value="Login" type="submit" />
                                    <?= form_close() ?>
				</div>


			</div>
		</div>
	</section>

	<!-- ... Info Boxes -->
</div>