<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if(!$this->loggedinuser->isLoggedin()){
            redirect(base_url().'login');
        }
        $this->load->model('coursemodel');
    }
    public function index(){
        $data['breadtext'] = 'Dashboard';
        $this->load->view($this->loggedinuser->user['status'].'/header',$data);
        if($this->loggedinuser->user['status'] == 'student'){
            $this->load->model('coursemodel');
            $this->load->view($this->loggedinuser->user['status'].'/upcomingtest');
        }else{
            $this->load->view($this->loggedinuser->user['status'].'/dashboard');
        }
        
        $this->load->view($this->loggedinuser->user['status'].'/footer');
    }
}
