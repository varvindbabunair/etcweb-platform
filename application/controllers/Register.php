<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->input->post('admin-regn') && $this->input->post('admin-regn') == 'Complete Registration'){
            $institute['name'] = addslashes($this->input->post('inst-name'));
            $institute['phone'] = addslashes($this->input->post('inst-phone'));
            $institute['email'] = addslashes($this->input->post('inst-email'));
            $institute['website'] = addslashes($this->input->post('inst-website'));
            $institute['add_date'] = date('Y-m-d');
            $institute['instkey'] = $this->incrementalHash();
            $institute['added_by'] = 0;
            $inst_insert = $this->db->insert('institute_list',$institute);
            $user['institute'] = $this->db->insert_id();
            $user['name'] = addslashes($this->input->post('name'));
            $user['phone'] = addslashes($this->input->post('phone'));
            $user['email'] = addslashes($this->input->post('email'));
            $user['password'] = md5(addslashes($this->input->post('password')));
            $user['status'] = 'admin';
            $user['cookie'] = md5($this->input->post('email').$this->input->post('phone'));
            $user_insert = $this->db->insert('user_list',$user);
            
            //encrypt library
//            $this->load->library('encrypt');
            //sending email via smtp
            //Load email library
            $this->load->library('email');

            //SMTP & mail configuration
            $config = array(
                'protocol'      => 'smtp',
                'smtp_host'     => 'smtp.hostinger.in',
                'smtp_port'     => 587  ,
                'smtp_user'     => 'etc@entranceengine.com',
                'smtp_pass'     => 'P@ssw0rd@123#',
                'mailtype'      => 'html',
                'charset'       => 'utf-8',
                'smtp_crypto'   => 'tls',
                'priority'      => 1
            );
            
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            //Email content
            $htmlContent = '<p>Hello '.$user['name'].',</p>';
            $htmlContent .= '<h3 style="text-align:center">Thank you for registering your institute '.$institute['name'].' with etcweb.entranceengine.com</h3>';
            $htmlContent .= '<p>However, to continue using the etcweb account, you will have to verify your email address. You can verify your email address by clicking on the button below. This button or link can only be used only once to verify your mail id.</p>';
            $htmlContent .= '<h3 style="text-align:center;"><a href="http://etcweb.entranceengine.com/verify/?key='.$user['cookie'].'&email='.$user['email'].'"><button style="padding:15px 100px; border:0px; border-radius:6px; background-color:green; color:#fff;font-weight:600;">Click Here to Verify Email</button></a></h3>';
            $htmlContent .= '<h3 style="text-align:center;">OR</h3>';
            $htmlContent .= '<p>Copy and paste the below link onto your new browser tab. You only need to verify your email only once.</p>';
            $htmlContent .= '<p>http://etcweb.entranceengine.com/verify/?key='.$user['cookie'].'&email='.$user['email'].'</p>';

            $htmlContent .= '<p><small>This is a system generated email. Please donot reply to this mail.</small></p>';
//            $htmlContent .= '<p>You can click herelogin to view the dashboard</p>';

            if($institute['email'] == $user['email']){
                $this->email->to($institute['email']);
            }else{
                $this->email->to($institute['email'] .','. $user['email']);
            }
            
            $this->email->from('etc@entranceengine.com','ETCWEB');
            $this->email->subject('Institute Registration successful | etcweb.entranceengine.com');
            $this->email->message($htmlContent);
            $this->email->set_header('mailed-by','https://webmail.hostinger.com');
            $this->email->set_header('signed-by','https://webmail.hostinger.com');

            
            //Send email
            $this->email->send();
            
            redirect('/register/confirmpage', 'refresh');
            
        }
    }
    public function index(){
        $this->load->view('register/header');
        $this->load->view('register/page');
        $this->load->view('register/footer');
    }

    function incrementalHash($len = 5){
        $charset = "0123456789abcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base){
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }
        return substr($result, -5);
    }

    public function confirmpage()
    {
        $this->load->view('register/header');
        $this->load->view('register/confirmpage');
        $this->load->view('register/footer');
    }
}
