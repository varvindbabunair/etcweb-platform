<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->loggedinuser->user['status'] != 'student'){
            redirect(base_url().'dashboard');
        }
        
    }
    public function index(){
        $data=array();
        $data['breadtext'] = 'Dashboard';
        $this->load->view('student/header',$data);
        // $this->load->view('student/dashboard');
        $this->load->model('coursemodel');
        $this->load->view('student/upcomingtest');
        $this->load->view('student/footer');
    }
    
    public function upcomingtest(){
        $data=array();
        $this->load->model('coursemodel');
        $data['breadtext'] = 'Upcoming Tests';
        $this->load->view('student/header',$data);
        $this->load->view('student/upcomingtest');
        $this->load->view('student/footer');
    }
    public function completedtest(){
        $data=array();
        $this->load->model('coursemodel');
        $data['breadtext'] = 'Inactive / Completed Tests';
        $this->load->view('student/header',$data);
        $this->load->view('student/completedtest');
        $this->load->view('student/footer');
    }
    public function attendedtest(){
        $data=array();
        $this->load->model('coursemodel');
        $data['breadtext'] = 'Attended Tests';
        $this->load->view('student/header',$data);
        $this->load->view('student/completedtest');
        $this->load->view('student/footer');
    }
    public function dashboard(){
        $data=array();
        $data['breadtext'] = 'Dashboard';
        $this->load->view('student/header',$data);
        // $this->load->view('student/dashboard');
        $this->load->model('coursemodel');
        $this->load->view('student/upcomingtest');
        $this->load->view('student/footer');
    }
    public function profile(){
        $data['breadtext'] = 'User Profile';
        $this->load->view('student/header',$data);
        $this->load->view('student/profile');
        $this->load->view('student/footer');
    }

    public function submittest(){
        $data['test'] = $this->input->post('test');
        $data['package'] = 0;
        $data['test_time'] = date('Y-m-d H:i:s');
        $data['session'] ='';
        $data['user'] = $this->loggedinuser->user['id'];
        $data['answer'] = $this->input->post('answer-string');

        $this->db->insert('answer_list',$data);
        redirect('/student/attendedtest', 'refresh');
        
    }

    public function test(){
        $this->load->model('coursemodel');
        $this->load->view('student/test');
    }
    public function ajaxtest(){
        $this->load->model('coursemodel');
        $this->load->view('student/testajax');
    }
    public function testperformance(){
        $data=array();
        $this->load->model('coursemodel');
        $this->load->view('student/testperformance');
    }
    public function answersheet(){
        $data=array();
        $this->load->model('coursemodel');
        $this->load->view('student/answersheet');
    }

    public function syllabus1(){
        $data['breadtext'] = 'Syllabus & Synopsys';
        $this->load->view('student/header',$data);
        $this->load->view('student/syllabus');
        $this->load->view('student/footer');
    }

    public function syllabus(){
        $this->load->model('syllabusmodel');
        $user = $this->loggedinuser->user['course'];
        if($this->syllabusmodel->syllabus($user)){
            $data["syllabus"]= $this->syllabusmodel->syllabus($user);
            $data['breadtext'] = 'Syllabus & Synopsys';
            $this->load->view('student/header',$data);
            // $this->load->view('student/syllabus');
            $this->load->view('student/syllabuscontent',$data);
            $this->load->view('student/footer');
        }else{
            echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Syllabus</h3>
                    <p style="text-align:center">Some error occured and the system failed to load the syllabus. Please contact the etcweb customer support if the problem persists</p>';
        }
    }

    public function synopsys(){
        if($this->input->post('view_value')){
            $this->db->where(array('chapter'=>$this->input->post('view_value')));
            $qry = $this->db->from('synopsys_list')->get();
            if($qry->num_rows() != 0){
                $rslt = $qry->result_array();
                echo $rslt[0]['synopsys'];
            }else{
                echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Synopsys</h3><p style="text-align:center">Some error occured and the system failed to load the synopsys. Please contact the etcweb customer support if the problem persists</p>';
            }
        }else{
            echo '<h3 style="text-align: center;" >Some Error Occured</h3>';
        }
    }
}
