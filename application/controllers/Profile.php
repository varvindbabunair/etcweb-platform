<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
    	$data['breadtext'] = 'User Profile';
    	$userprofile = $this->loggedinuser->user['status'];
        $this->load->view($userprofile.'/header',$data);
        $this->load->view('profile/profile');
        $this->load->view($userprofile.'/footer');
    }
    function profilesubmit(){


        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['dob'] = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('dob'))));

        $this->db->where('id',$this->loggedinuser->user['id']);

        if(!empty($_FILES['image']['name'])){
            $config['upload_path']          = './images/user';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = true;
            $config['file_ext_tolower']     = true;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image')){
                $data['image']=$this->upload->data('file_name');
            }
        }

        $this->db->update('user_list',$data);
        redirect(base_url().'profile');

    }

    function password() {
        $data['breadtext'] = 'Update Password';
        $userprofile = $this->loggedinuser->user['status'];
        $this->load->view($userprofile.'/header',$data);
        $this->load->view('profile/password');
        $this->load->view($userprofile.'/footer');
    }

    function updatepassword(){
        $password = $this->input->post('password');
        $cnfpassword = $this->input->post('cnf-password');
        $data = array();
        if($password === $cnfpassword && trim($password) !== '' && trim($cnfpassword)!== ''){
            $data['password'] = md5($password);
            $this->db->where('id',$this->loggedinuser->user['id']);
            $this->db->update('user_list',$data);
            $data['type'] = 'success';
            $data['message'] = 'Successfully updated password';
        }else{
            $data['type'] = 'danger';
            $data['message'] = 'Some Error occured while updating password';
        }
        echo json_encode($data);
    }
}
