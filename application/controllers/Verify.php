<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller{
    public function index(){
        $this->load->view('verify/header');
        $data = array('verify'=>FALSE);
        if($this->input->get('key') && $this->input->get('email')){
            $this->db->where(array(
                                    'email'=>$this->input->get('email'),
                                    'cookie'=>$this->input->get('key')
                                    ));
            ;

            $user_details = $this->db->from('user_list')->get();

            $user_info = $user_details->result_array();

            if($user_info)
            {
            $this->db->where('id',$user_info[0]['id']);
                $newcookie = md5($user_info[0]['id'].time());
                set_cookie('etcwebuser', $newcookie,'360000');
                if($this->db->update('user_list',array('last_login'=> date('Y-m-d'),'cookie'=>$newcookie)))
                {
                    $data = array('verify'=>TRUE);
                }
            }
        }
        $this->load->view('verify/page',$data);
        $this->load->view('verify/footer');
    }
}
