<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        if($this->loggedinuser->isLoggedin()){
            redirect(base_url().'dashboard');
        }
    }
    
    public function index(){
        if($this->input->get('instkey') != '' ){
            $this->db->where(array('instkey'=>$this->input->get('instkey')));
            $qry = $this->db->from('institute_list')->get();
            if($qry->num_rows() == 1){
                $inst = $qry->result_array();
                
                if($this->input->post('login-submit') == 'Login' && $this->input->post('login') ){
                    $this->load->model('userlogin');
                    $login = $this->input->post('login');
                    $password = md5($this->input->post('password'));
                    $instid = $inst[0]['id'];
                    if($this->userlogin->instlogin($instid,$login,$password)){
                        redirect(base_url().'dashboard');
                    }else{
                        $data['param']='fail';
                        $this->load->view('login/instheader',$inst[0]);
                        $this->load->view('login/instpage',$data);
                        $this->load->view('login/footer');
                    }
                }else{
                    $data['param']=NULL;
                    $this->load->view('login/instheader',$inst[0]);
                    $this->load->view('login/instpage',$data);
                    $this->load->view('login/footer');
                }
            }else{
                $this->adminloginLoad(NULL);
            }
        }else{
            if($this->input->post('login-submit') == 'Login' && $this->input->post('login') ){
                $this->load->model('userlogin');
                $login = $this->input->post('login');
                $password = md5($this->input->post('password'));
                if($this->userlogin->login($login,$password)){
                    redirect(base_url().'dashboard');
                }else{
                    $param='fail';
                    $this->adminloginLoad($param);
                }
            }else{
                $this->adminloginLoad(NULL);
            }
        }
    }

    private function adminloginLoad($param){
        $data['param']=$param;
        $this->load->view('login/header');
        $this->load->view('login/page',$data);
        $this->load->view('login/footer');
    }
    
}


