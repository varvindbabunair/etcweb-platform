<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {
	
	public function index(){
		$this->load->view('web/header');
		$this->load->view('terms/terms');
		$this->load->view('web/footer');
	}
}