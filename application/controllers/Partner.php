<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller {
    public function index(){
        $this->load->view('partner/header');
        $this->load->view('partner/page');
        $this->load->view('partner/footer');
    }
}
