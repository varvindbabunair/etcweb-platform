<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Logout extends CI_Controller{
    public function __construct() {
        parent::__construct();
//        print_r($this->loggedinuser->user);
//        die();
        if($this->loggedinuser->user['status'] == 'admin'){
            delete_cookie('etcwebuser');
            redirect(base_url().'login');
        }else{
            delete_cookie('etcwebuser');
            redirect(base_url().'login?instkey='.$this->loggedinuser->instkey());
        }
        
    }
    public function index() {
        
    }
}