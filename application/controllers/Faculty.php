<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->loggedinuser->user['status'] != 'faculty'){
            redirect(base_url().'dashboard');
        }
    }
    public function index(){
            $this->load->view('admin/header');
            $this->load->view('admin/dashboard');
            $this->load->view('admin/footer');
    }
    
}
