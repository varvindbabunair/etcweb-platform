<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->loggedinuser->user['status'] != 'student'){
            redirect(base_url().'dashboard');
        }
        $this->load->model('coursemodel');
        
    }
    public function index(){
        $data=array();
        $data['breadtext'] = 'Dashboard';
        $this->load->view('student/header',$data);
        // $this->load->view('student/dashboard');
        $this->load->model('coursemodel');
        $this->load->view('student/upcomingtest');
        $this->load->view('student/footer');
    }
    public function suboption() {
        $class_id = $this->input->post('class_id');
        $subjects = $this->coursemodel->get_subjects($class_id);
        $str = '';
        echo '<select name="subject_id" class="chap-fetch form-control">';
        echo '<option>Select Subject</option>';
        foreach ($subjects as $subject) {
            echo '<option value="'.$subject['id'].'">'.$subject['title'].'</option>';
        }
        echo '</select>';
        
    }
    public function instvideo(){
        
        
        $this->load->model('Classmodel');
        $classlist['title'] = $this->Classmodel->selectClass();
        $data['user'] = $this->loggedinuser->user['id'];
        
        $data['breadtext'] = 'Video';
        $this->load->view('student/header',$data);
        $this->load->view('student/instvideo', $classlist);
        $this->load->view('student/footer');
    }
    
    
    public function chapoption() {
        $subject_id = $this->input->post('subject_id');
        $chapters = $this->coursemodel->get_chapters($subject_id);
        // $str = '';
        echo '<select class="form-control" name="chap_id" id="chapter_id">';
        echo '<option>Select Chapter</option>';
        foreach ($chapters as $chapter) {
            echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
        }
        echo '</select>';
        
    }

    public function videoget(){
        $chap_id = $this->input->post('chap_id');
        $this->load->model('syllabusmodel');
        $videos_list = $this->db->get_where('video_list', array('chapter' => $chap_id, 'institute_id' => $this->loggedinuser->user['institute']));
        
        $videos = $videos_list->result_array();
        
        echo '<table class="table table-hover">';
        echo '<tr><th>Sl no.</th><th>Title</th><th>Options</th></tr>';
        $i=0;
        foreach ($videos as $video) {
            echo '<tr><td>'.$i.'</td>';
            echo '<td>'.$video['title'].'</td>';
            echo '<td>';
            echo '<button class="btn btn-primary " title="View Question" data-toggle="modal" data-target="#videoModal'.$i.'">View Video</button>';
            
            $url = $video['video_link'];
            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
            $videoid = $matches[1]; 
            
            echo '<div id="videoModal'.$i.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-body">';
            echo '<iframe width="100%" height="360" src="https://www.youtube.com/embed/' . $videoid . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
            echo '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
            echo '</td></tr>';
            $i++;
        }
        echo '</table>';
    }
    
}
