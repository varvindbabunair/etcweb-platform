<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instadmin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->loggedinuser->user['status'] != 'admin'){
            redirect(base_url().'dashboard');
        }
        $this->load->model('coursemodel');
        
    }
    public function index(){
        $data['breadtext'] = 'Dashboard';
        
        $this->load->view('admin/header',$data);
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }
    public function dashboard(){
        $data['breadtext'] = 'Dashboard';
        
        $this->load->view('admin/header',$data);
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }
    public function syllabus(){
        $data['breadtext'] = 'Syllabus & Synopsys';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/syllabus');
        $this->load->view('admin/footer');
    }
    public function question_list(){
        $this->load->model('Classmodel');
        $classlist['title'] = $this->Classmodel->selectClass();
        
        // echo "<pre>";
        // var_dump ($classlist['title']);die;
        // echo "</pre>";
        
         
        $data['breadtext'] = 'All Questions';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/question_list', $classlist);
        $this->load->view('admin/footer');
    }

    public function suboption()
    {
        $class_id = $this->input->post('class_id');
        $subjects = $this->coursemodel->get_subjects($class_id);
        $str = '';
        echo '<select name="subject_id" class="chap-fetch form-control">';
        echo '<option>Select Subject</option>';
        foreach ($subjects as $subject) {
            echo '<option value="'.$subject['id'].'">'.$subject['title'].'</option>';
        }
        echo '</select>';
        
    }
    
    public function chapoption()
    {
        $subject_id = $this->input->post('subject_id');
        $chapters = $this->coursemodel->get_chapters($subject_id);
        // $str = '';
        echo '<select class="form-control" name="chap_id">';
        echo '<option>Select Chapter</option>';
        foreach ($chapters as $chapter) {
            echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
        }
        echo '</select>';
        
    }

    public function syllabuscontent(){
        $this->load->model('syllabusmodel');
        if($this->input->post('syllabus')){
            $data['syllabus'] = $this->input->post('syllabus');
            $this->load->view('admin/syllabuscontent',$data);
        }else{
            echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Syllabus</h3>
                    <p style="text-align:center">Some error occured and the system failed to load the syllabus. Please contact the etcweb customer support if the problem persists</p>';
        }
    }
    public function synopsys(){
        if($this->input->post('view_value')){
            $this->db->where(array('chapter'=>$this->input->post('view_value')));
            $qry = $this->db->from('synopsys_list')->get();
            if($qry->num_rows() != 0){
                $rslt = $qry->result_array();
                echo $rslt[0]['synopsys'];
            }else{
                echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Synopsys</h3><p style="text-align:center">Some error occured and the system failed to load the synopsys. Please contact the etcweb customer support if the problem persists</p>';
            }
        }else{
            echo '<h3 style="text-align: center;" >Some Error Occured</h3>';
        }
    }

    public function questions(){
        if($this->input->post('view_value')){
            $this->db->where(array('chapter'=>$this->input->post('view_value')));
            $qry = $this->db->from('question_list')->get();
            if($qry->num_rows() != 0){
                $data['questions'] = $qry->result_array();
                $this->load->view('admin/questions',$data);
            }else{
                echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Questions</h3><p style="text-align:center">Some error occured and the system failed to load the questions. Please contact the etcweb customer support if the problem persists</p>';
            }
        }else{
            echo '<h3 style="text-align: center;" >Some Error Occured</h3>';
        }
    }
    public function questionsfrom(){
        $this->load->model('syllabusmodel');
        if($this->input->post('chapter')){
            $this->db->where('chapter', $this->input->post('chapter'));
            $qry = $this->db->from('question_list')->get();
            if($qry->num_rows() != 0){
                $data['questions'] = $qry->result_array();
                $this->load->view('admin/questions',$data);
            }else{
                echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Questions</h3><p style="text-align:center">Some error occured and the system failed to load the questions. Please contact the etcweb customer support if the problem persists</p>';
            }
        }else{
            echo '<h3 style="text-align: center;" >Some Error Occured</h3>';
        }
    }
    public function chaptersfrom(){
        $ret = '<option>Select Chapter</option>';
        $this->load->model('syllabusmodel');
        if($this->input->post('subject')){
            $subject = strtolower($this->input->post('subject'));
            $class11 = 'class11'.$subject;
            $class12 = 'class12'.$subject;
//            $chapters = array_merge(, $this->syllabusmodel->$class12());

            $ret .= '<optgroup label="Class 11">';
            foreach($this->syllabusmodel->$class11() as $cl11){
                $ret .= '<option value="'.$cl11['id'].'">'.$cl11['title'].'</option>';
            }
            $ret .= '</optgroup>';

            $ret .= '<optgroup label="Class 12">';
            foreach($this->syllabusmodel->$class12() as $cl12){
                $ret .= '<option value="'.$cl12['id'].'">'.$cl12['title'].'</option>';
            }
            $ret .= '</optgroup>';
        }
        echo $ret;
    }

    public function allquestions(){
        if($this->input->post('view_value')){
            $this->db->where(array('chapter'=>$this->input->post('view_value')));
            $qry = $this->db->from('question_list')->get();
//            if(!$qry){
                $this->load->model('syllabusmodel');
                $this->syllabusmodel->classNameOfChapter($this->input->post('view_value'));
                $data['classname'] = $this->syllabusmodel->classNameOfChapter($this->input->post('view_value'));
                $data['subjectname'] = $this->syllabusmodel->subjectNameOfChapter($this->input->post('view_value'));
                $data['chaptername'] = $this->syllabusmodel->chapterName($this->input->post('view_value'));
                $data['breadtext'] = 'Questions from Class'.$data['classname'].', '.$data['subjectname'].' - '.$data['chaptername'];
                $this->load->view('admin/header',$data);
                $data['questions'] = $qry->result_array();
                $this->load->view('admin/allquestions',$data);
//            }else{
//                echo '<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Questions</h3><p style="text-align:center">Some error occured and the system failed to load the questions. Please contact the etcweb customer support if the problem persists</p>';
//            }
        }else{
            echo '<h3 style="text-align: center;" >Some Error Occured</h3>';
        }

        $this->load->view('admin/footer');
    }


//Course and its related parts
    public function course(){
        
        $data['breadtext'] = 'Manage Course';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/course');
        $this->load->view('admin/footer');
    }
    public function submitcourse(){
        
        $data['course_name'] = trim($this->input->post('course-name'));
        
        $data['course_start'] = date('Y-m-d', strtotime(strtr($this->input->post('course-start'), '/', '-')));
        $data['course_end'] = date('Y-m-d', strtotime(strtr($this->input->post('course-end'), '/', '-')));
        $data['syllabus'] = trim($this->input->post('syllabus'));
        
        $data['institute'] = $this->loggedinuser->user['institute'];
        $data['added_by'] = $this->loggedinuser->user['id'];
        $data['add_date'] = date('Y-m-d');
        if( $data['course_name'] != '' && $data['syllabus'] != '' ){
            if($this->db->insert('course_list',$data)){
                
                $course_id = $this->db->insert_id();
                
                $batchnames = trim($this->input->post('batch-names'));
                $batchnames = rtrim($this->input->post('batch-names'),',');
                $batchnames = ltrim($this->input->post('batch-names'),',');
                
                $batch_arr = explode(',', $batchnames);
                
                foreach($batch_arr as $batch){
                    $bth['course'] = $course_id ;
                    $bth['batch_name'] = trim($batch);
                    $bth['institute'] = $this->loggedinuser->user['institute'];
                    $this->db->insert('batch_list', $bth);
                }
                
                echo '{"type":"success","message":"Successfully added the course <strong>'.$data['course_start'].'</strong> and the batches in your institute"}';
            }else{
                echo '{"type":"danger","message":"Some Error Occured. The course was not added."}';
            }
        }else{
            echo '{"type":"danger","message":"Failed to add course. Some Fields were missing."}';
        }
    }
    public function viewcourse(){
        if(!$this->input->post('course_id')){
            redirect(base_url().'instadmin/course');
        }
        
        $course = $this->coursemodel->get_course($this->loggedinuser->user['institute'],$this->input->post('course_id'));
        $this->load->view('admin/viewcourse', $course);
    }
    public function editcourse(){
        if(!$this->input->post('course_id')){
            redirect(base_url().'instadmin/course');
        }
        if($this->input->post('course-name') && $this->input->post('course-start') && $this->input->post('course-end') ){
            $data['course_name'] = trim($this->input->post('course-name'));
            $data['course_start'] = date('Y-m-d', strtotime(strtr($this->input->post('course-start'), '/', '-')));
            $data['course_end'] = date('Y-m-d', strtotime(strtr($this->input->post('course-end'), '/', '-')));
            $data['syllabus'] = trim($this->input->post('syllabus'));

            $this->db->where('id',$this->input->post('course_id'));
            $this->db->update('course_list',$data);

            $batchnames = trim($this->input->post('batch-names'));
            $batchnames = rtrim($this->input->post('batch-names'),',');
            $batchnames = ltrim($this->input->post('batch-names'),',');

            if($batchnames != ''){
                $batch_arr = explode(',', $batchnames);
                
                foreach($batch_arr as $batch){
                    $bth['course'] =  $this->input->post('course_id');
                    $bth['batch_name'] = trim($batch);
                    $bth['institute'] = $this->loggedinuser->user['institute'];
                    $this->db->insert('batch_list', $bth);
                }
                
            }

            for($cn=1; $cn <= $this->input->post('batch-size'); $cn++){
                $batch = array();
                $this->db->where('id',$this->input->post('batch'.$cn.'id'));
                $batch['batch_name'] = trim($this->input->post('batch'.$cn));
                if($batch['batch_name'] == ''){
                    $this->db->delete('batch_list');
                }else{
                    $this->db->update('batch_list',$batch);
                }
                
            }


        }
        
        $data['breadtext'] = 'Edit Course';
        $this->load->view('admin/header',$data);
        $data2 = $this->coursemodel->get_course($this->loggedinuser->user['institute'], $this->input->post('course_id'));
        $this->load->view('admin/course-edit',$data2);
        $this->load->view('admin/footer');
    }

//Tests and its related parts
    public function tests(){
        
        $data['breadtext'] = 'Manage Tests';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/test');
        $this->load->view('admin/footer');
    }
    public function ranklist(){
        $this->load->view('admin/ranklist');
    }
    public function testpreview(){
        $data['breadtext'] = 'Edit Test';
        $this->load->model('syllabusmodel');
        $data['testdetails']= $this->coursemodel->get_test_details($this->input->get_post('testid'));

        //selecting batches
        $this->db->select(array('test_batch_list.batch','batch_list.batch_name'));
        $this->db->from('test_batch_list');
        $this->db->join('batch_list','test_batch_list.batch = batch_list.id');
        $this->db->where(array(
            'test_batch_list.test'=>$this->input->get_post('testid')
        ));
        $qry = $this->db->get();
        $data['test_batches'] = $qry->result_array();

        $this->load->view('admin/header',$data);
        $this->load->view('admin/testpreview');
        $this->load->view('admin/footer');
    }
    public function publishranklist(){

        $testid = $this->input->get_post('testid');
        $this->db->where('id',$testid);
        $qry = $this->db->from('test_list')->get();
        $test = $qry->result_array();

        if($this->loggedinuser->institute['id'] === $test[0]['institute'] && ($this->loggedinuser->user['status'] ==='admin' || $this->loggedinuser->user['status'] ==='faculty' )){
            $data['ranklist_published'] =1;
            $this->db->where('id',$testid);
            $this->db->update('test_list',$data);
            redirect(base_url().'/instadmin/ranklist/?testid='.$testid);
        }else{
            redirect(base_url().'dashboard');
        }

    }
    public function testreport(){
        
        $data['breadtext'] = 'Test Reports';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/testreport');
        $this->load->view('admin/footer');
    }
    public function createtest(){
        
        $data['breadtext'] = 'Create Test';
        $this->load->view('admin/header',$data);
        $this->load->model('syllabusmodel');
        $this->load->view('admin/createtest');
        $this->load->view('admin/footer');
    }
    public function testcreate(){
        $err = 0;
        
        $data = array();
        $data2 = array();

        $data['title'] = $this->input->post('testname');
        $data['section_title'] = $this->input->post('sections');
        $data['section_rule'] = '["any","'.$this->input->post('examduration').'"]';
        $data['instruction'] = '';
        $data['exam_start'] = date('Y-m-d H:i:s', strtotime(strtr($this->input->post('examstart'),'/','-')));
        $data['exam_entry'] = date('Y-m-d H:i:s', strtotime(strtr($this->input->post('examentry'),'/','-')));
        

        $data['tags'] = $this->input->post('difficulty');
        $data['institute'] = $this->input->post('institute');

        $chapters = json_decode($this->input->post('chapters'),true);
        
        $qn_query = array();
        $cnt = 0;
        foreach ($chapters as $chaps) {
            $this->db->select('id');
            foreach ($chaps as $chapter) {
                $this->db->or_where('chapter',$chapter);
            }
            if($data['tags'] != 'all'){
                $this->db->where('difficulty',$data['tags']);
            }
            $get = $this->db->from('question_list')->get();
            $qn_query[$cnt] = $get->result_array();
            $cnt++;
        }

        $sections = json_decode($this->input->post('sections'),true);
        //selecting random keys with count
        $qns = array();
        $n = 0;
        foreach ($qn_query as $qn) {
            if($sections[$n] == 'Physics'){
                $qnno = $this->input->post('phyqnno');
                if($qnno > sizeof($qn_query[$n])){
                    $err = 1;
                }else{
                    $qns[$n] = array_rand($qn_query[$n],$qnno);
                }
            }else if($sections[$n] == 'Chemistry'){
                $qnno = $this->input->post('chemqnno');
                if($qnno > sizeof($qn_query[$n])){
                    $err = 1;
                }else{
                    $qns[$n] = array_rand($qn_query[$n],$qnno);
                }
            }else if($sections[$n] == 'Biology'){
                $qnno = $this->input->post('bioqnno');
                if($qnno > sizeof($qn_query[$n])){
                    $err = 1;
                }else{
                    $qns[$n] = array_rand($qn_query[$n],$qnno);
                }
            }else if($sections[$n] == 'Mathematics'){
                $qnno = $this->input->post('mathsqnno');
                if($qnno > sizeof($qn_query[$n])){
                    $err = 1;
                }else{
                    $qns[$n] = array_rand($qn_query[$n],$qnno);
                }
            }
            
            $n++;
        }
        //preparing the questions string
        $m = 0;
        $qn_array = array();
        foreach($qns as $questions){
            $n=0;
            foreach ($questions as $q) {
                $qn_array[$m][$n] = array($qn_query[$m][$q]['id'],$this->input->post('correctmark'),$this->input->post('negativemark'));
                $n++;
            }
            $m++;
        }

        $data['questions'] = json_encode($qn_array, true);

        $data['course'] = $this->input->post('course');


        if($err === 0){
            $this->db->insert('test_list',$data);

            $data2['test'] = $this->db->insert_id();
            $batches = explode(',', rtrim($this->input->post('batches'),','));

            foreach($batches as $batch){
                $data2['batch'] = $batch;
                $this->db->insert('test_batch_list',$data2);
            }
            echo "success";
        }else{
            echo "fail";
        }
        
        

    }
    public function testupdate(){
        $ret['status'] = 'danger';
        $ret['message'] = 'Failed to update Test';

        if($this->input->post('testid')){
            //check if exam started
            $this->db->where(array(
                'id' =>$this->input->post('testid'),
                'exam_start <' => date('Y-m-d H:i:s', strtotime(strtr($this->input->post('examstart'),'/','-')))
            ));
            $qry = $this->db->from('test_list')->get();
            if($qry->num_rows() > 0){
                $ret['status'] = 'danger';
                $ret['message'] = 'Cannot Update already started exam';
            }else{
                $data['title'] = $this->input->post('testname');
                $data['questions'] = str_replace('\'','"',$this->input->post('questions'));
                $data['section_rule']= '["any","'.$this->input->post('examduration').'"]';
                $data['course'] = $this->input->post('course');
                $data['exam_start'] = date('Y-m-d H:i:s', strtotime(strtr($this->input->post('examstart'),'/','-')));
                $data['exam_entry'] = date('Y-m-d H:i:s', strtotime(strtr($this->input->post('examentry'),'/','-')));

                $this->db->where('id',$this->input->post('testid'));

                $this->db->update('test_list',$data);

                $this->db->delete('test_batch_list',array('test'=>$this->input->post('testid')));

                $batches = trim($this->input->post('batches'),',');
                $batch_list = explode(',',$batches);
                $data2 = array();
                $data2['test'] = $this->input->post('testid');
                foreach($batch_list as $batch){
                    $data2['batch'] = $batch;
                    $this->db->insert('test_batch_list',$data2);
                }

                $ret['status'] = 'success';
                $ret['message'] = 'Successfully Updated Test';
            }
        }
        echo json_encode($ret);
    }
    public function batchcheck(){
        
        $course_id = $this->input->post('course_id');
        $batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$course_id);
        $str = '';
        echo '<h5>Batches<h5>';
        echo '<table class="table table-hover">';
        foreach ($batches as $batch) {
            echo '<tr><td><input class="batch-check" type="checkbox" checked name="batch-'.$batch['id'].'" value="'.$batch['id'].'" />';
            echo ' '.$batch['batch_name'].'</td></tr>';
            $str .= $batch['id'].',';
        }
        echo '</table>';
        echo '<input type="hidden" value="'.rtrim($str,',').'" />';
    }
    public function testperformance(){
        $this->load->view('admin/testperformance');
    }

    //students and related functions
    public function students(){
        
        $data['breadtext'] = 'Manage Students';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }
    public function addstudent(){
        $msg = '';
        $err = true;
        $returnvar = array();
        $faculty_add_data = array();
        $faculty_add_data['name'] = $this->input->post('student-name');
        $faculty_add_data['email'] = $this->input->post('student-email');
        $faculty_add_data['dob'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->input->post('student-dob'))));
        $faculty_add_data['password'] =  md5(date('d/m/Y',strtotime(str_replace('/', '-', $this->input->post('student-dob')))));
        $faculty_add_data['phone'] = $this->input->post('student-phone');
        $faculty_add_data['address'] = addslashes($this->input->post('student-address'));
        $faculty_add_data['regn_number'] = $this->input->post('student-regn');
        $faculty_add_data['institute'] = $this->loggedinuser->user['institute'];
        $faculty_add_data['status'] = 'student';
        $faculty_add_data['course'] = $this->input->post('course-id');
        $faculty_add_data['batch'] = $this->input->post('batch');
        $faculty_add_data['last_login']=date('Y-m-d',strtotime('today'));

        $check1 = $this->db->insert('user_list', $faculty_add_data);

        $parent_add_data = array();
        $parent_add_data['name'] = $this->input->post('parent-name');
        $parent_add_data['email'] = $this->input->post('parent-email');
        $parent_add_data['dob'] = null;
        $parent_add_data['password'] =  md5($this->input->post('student-dob'));
        $parent_add_data['phone'] = $this->input->post('parent-phone');
        $parent_add_data['address'] = addslashes($this->input->post('student-address'));
        $parent_add_data['regn_number'] = 'p'.$this->input->post('student-regn');
        $parent_add_data['institute'] = $this->loggedinuser->user['institute'];
        $parent_add_data['status'] = 'parent';
        $parent_add_data['course'] = $this->input->post('course-id');
        $parent_add_data['batch'] = $this->input->post('batch');
        $parent_add_data['last_login']=date('Y-m-d',strtotime('today'));

        $check2 = $this->db->insert('user_list', $parent_add_data);

        if($check1){
            $msg .= 'Successfully added Student details <br>';
            $err = false;
        }else{
            $msg .= 'Failed to add Student details <br>';
            $err = true;
        }

        if($check2){
            $msg .= 'Successfully added Parent details';
            $err = false;
        }else{
            $msg .= 'Failed to add Parents details';
            $err = false;
        }

        $returnvar['status'] = ($err)
            ? 'danger'
            : 'success';
        $returnvar['message'] = $msg;

        echo json_encode($returnvar);
    }
    public function addstudents(){


        if($this->input->post('batches') !=''){
            $batches = explode(',', $this->input->post('batches'));
            foreach ($batches as $batch) {
                if($_FILES['students-'.$batch]["error"] === 0){
                    $h = fopen($_FILES['students-'.$batch]['tmp_name'], "r");
                    $row_cnt = 0;
                    $col_array = array();
                    while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
                    {
                        if($row_cnt === 0){
                            $col_array = $data;
                        }else{
                            $student_det_array = array();
                            $colcnt = 0;
                            foreach ($col_array as $key_element) {
                                $student_det_array[trim($key_element)] = $data[$colcnt];
                                $colcnt++;
                            }
                            $faculty_add_data = array();
                            $faculty_add_data['name'] = addslashes($student_det_array['student_name']);
                            $faculty_add_data['email'] = (trim($student_det_array['student_email']) === '')?$student_det_array['parent_email']:$student_det_array['student_email'];
                            $faculty_add_data['dob'] = date('Y-m-d',strtotime(str_replace('/', '-', $student_det_array['student_dob'])));
                            $faculty_add_data['password'] =  md5($student_det_array['student_dob']);
                            $faculty_add_data['phone'] = (trim($student_det_array['student_phone']) === '')?$student_det_array['parent_phone']:$student_det_array['student_phone'];
                            $faculty_add_data['address'] = addslashes($student_det_array['address']);
                            $faculty_add_data['regn_number'] = $student_det_array['student_regn_number'];
                            $faculty_add_data['institute'] = $this->loggedinuser->user['institute'];
                            $faculty_add_data['status'] = 'student';
                            $faculty_add_data['course'] = $this->input->post('course-id');
                            $faculty_add_data['batch'] = $batch;

                            if(!$this->coursemodel->isuserexist($this->loggedinuser->user['institute'],$faculty_add_data['regn_number'])){
                                $check = $this->db->insert('user_list', $faculty_add_data);

                                $parent_add_data = array();
                                $parent_add_data['name'] = $student_det_array['parent_name'];
                                $parent_add_data['email'] = (trim($student_det_array['parent_email']) === '')?$student_det_array['student_email']:$student_det_array['parent_email'];
                                $parent_add_data['dob'] = null;
                                $parent_add_data['password'] =  md5($student_det_array['student_dob']);
                                $parent_add_data['phone'] = (trim($student_det_array['parent_phone']) === '')?$student_det_array['student_phone']:$student_det_array['parent_phone'];
                                $parent_add_data['address'] = addslashes($student_det_array['address']);
                                $parent_add_data['regn_number'] = 'p'.$student_det_array['student_regn_number'];
                                $parent_add_data['institute'] = $this->loggedinuser->user['institute'];
                                $parent_add_data['status'] = 'parent';
                                $parent_add_data['course'] = $this->input->post('course-id');
                                $parent_add_data['batch'] = $batch;

                                $this->db->insert('user_list', $parent_add_data);

                            }
                        }
                        $row_cnt++;
                    }
                }else{
                    // print_r($_POST);die();
                }
            }
        }

        redirect('/instadmin/students/', 'location');
    }
    public function studentupdate(){
        $studdata['name'] = $this->input->post('name');
        $studdata['email'] = $this->input->post('email');
        $studdata['phone'] = $this->input->post('phone');
        $studdata['address'] = $this->input->post('address');
        $studdata['dob'] = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('dob'))));

        $this->db->where('id',$this->input->post('studid'));

        $config['upload_path']          = './images/user';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 200;
        $config['overwrite']            = true;
        $config['file_ext_tolower']     = true;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('image')){
            $data['image']=$this->upload->data('file_name');
        }
        $this->db->update('user_list',$studdata);

        $pdata['name'] = $this->input->post('pname');
        $pdata['email'] = $this->input->post('pemail');
        $pdata['phone'] = $this->input->post('pphone');
        $pdata['address'] = $this->input->post('address');

        $this->db->where('id',$this->input->post('pid'));
        $this->db->update('user_list',$pdata);

        redirect(base_url().'instadmin/editstudent/'.$this->input->post('studid'));
    }
    public function studpassupdate(){
        $password = $this->input->post('password');
        $cnfpassword = $this->input->post('cnf-password');
        $data = array();
        if($password === $cnfpassword && trim($password) !== '' && trim($cnfpassword)!== ''){
            $data['password'] = md5($password);
            $this->db->where(array(
                'id'=>$this->loggedinuser->user['id'],
                'status'=>'student'
            ));
            $this->db->update('user_list',$data);
            $data['type'] = 'success';
            $data['message'] = 'Successfully updated password';
        }else{
            $data['type'] = 'danger';
            $data['message'] = 'Some Error occured while updating password';
        }
        echo json_encode($data);
    }

    public function batchlist(){
        
        $course_id = $this->input->post('course-id');
        $batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$course_id);
        $str = '';
        echo '<table class="table table-hover">';
        $i = 0;
        foreach ($batches as $batch) {
            echo '<tr><td>'.$batch['batch_name'].'
            <input type="hidden" value="'.$batch['id'].'" name="batch[]" /></td>';
            echo '<td><input type="file" name="students-'.$batch['id'].'" /></td></tr>';
            $str .= $batch['id'].',';
            $i++;
        }
        echo '</table>';
        echo '<input type="hidden" name="batches" value="'.rtrim($str,',').'" />';
    }
    public function batchoption(){
        
        $course_id = $this->input->post('course-id');
        $batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$course_id);
        $str = '';
        echo '<select class="form-control" required="" name="batch">';
        echo '<option value="">Select Batch</option>';
        foreach ($batches as $batch) {
            echo '<option value="'.$batch['id'].'">'.$batch['batch_name'].'</option>';
        }
        echo '<option value="all">All Batches</option>';
        echo '</select>';
    }
    public function batchoptionwithoutall(){
        
        $course_id = $this->input->post('course-id');
        $batches = $this->coursemodel->get_batches($this->loggedinuser->user['institute'],$course_id);
        $str = '';
        echo '<select class="form-control" required="" name="batch">';
        echo '<option value="">Select Batch</option>';
        foreach ($batches as $batch) {
            echo '<option value="'.$batch['id'].'">'.$batch['batch_name'].'</option>';
        }
        echo '</select>';
    }

    public function studentget(){
        
        $course = $this->input->post('course-id');
        $batch = $this->input->post('batch');
        $students = $this->coursemodel->get_students($this->loggedinuser->user['institute'],$course, $batch);
        echo '<table class="table table-hover">';
        echo '<tr><th>Name</th><th>Address</th><th>Contact</th><th>Parents Details</th><th>Options</th></tr>';
        foreach ($students as $student) {
            echo '<tr><td>'.$student['name'].'<br><strong>Reg No:'.$student['regn_number'].'</strong></td>';
            echo '<td>'.$student['address'].'</td>';
            echo '<td>'.$student['email'].'<br>'.$student['phone'].'</td>';
            $this->db->where(array('regn_number' => 'p'.$student['regn_number']));
            $qry = $this->db->from('user_list')->get();
            $parent = $qry->result_array();
            echo '<td>';
            echo (isset($parent[0]['name']))?$parent[0]['name']:'';
            echo '<br>';
            echo (isset($parent[0]['email']))?$parent[0]['email']:'';
            echo '<br>';
            echo (isset($parent[0]['phone']))?$parent[0]['phone']:'';
            echo '</td>';
            echo '<td>';
            echo '<a href="/instadmin/editstudent/'.$student['id'].'"><button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" title="Edit User Details"><span class="fa fa-edit"></span><div class="ripple-container"></div></button></a>';
            echo '<button class="btn btn-sm btn-round btn-primary btn-fab reset-passwd-btn" onclick="studResetPass('.$student['id'].',\''.$student['name'].'\')" title="Reset Password"><span class="fa fa-key"></span><div class="ripple-container"></div></button>';
            echo '<a target="_blank" href="/instadmin/completedtest?student='.$student['id'].'"><button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" title="Test Performance"><span class="fa fa-list"></span><div class="ripple-container"></div></button></a>';
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
    public function editstudent($id){
        $data['breadtext'] = 'Edit Student Details';
        //student details
        $this->db->where('id',$id);
        $stud = $this->db->from('user_list')->get();
        $data['student_details'] = $stud->row();

        //parent details
        $this->db->where('regn_number','p'.$data['student_details']->regn_number);
        $parent = $this->db->from('user_list')->get();
        $data['parent_details'] = $parent->row();

        $this->load->view('admin/header',$data);
        $this->load->view('admin/editstudent');
        $this->load->view('admin/footer');
    }
    public function completedtest(){
        $data['breadtext'] = 'Student Performance';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/completedtest');
        $this->load->view('admin/footer');
    }

    public function studentperformance(){
        $this->load->view('student/testperformance');
    }
    public function answersheet(){
        $this->load->view('student/testperformance');
    }

    //message, its content and parts
    public function message(){
        
        $data['breadtext'] = 'Messages';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }
    public function notification(){
        $this->load->model('batchmodel');
        $data['breadtext'] = 'Manage Students';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }
    public function dates(){
        $this->load->model('batchmodel');
        $data['breadtext'] = 'Manage Students';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }
    public function institutes(){
        $this->load->model('batchmodel');
        $data['breadtext'] = 'Manage Students';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }
    public function credits(){
        $this->load->model('batchmodel');
        $data['breadtext'] = 'Manage Credits';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/students');
        $this->load->view('admin/footer');
    }

    //faculty and relatedparts
    public function faculty(){
        
        $data['breadtext'] = 'Manage Faculty';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/faculty');
        $this->load->view('admin/footer');
    }
    public function editfaculty($id){
        $data['breadtext'] = 'Edit Faculty Details';
        //faculty details
        $this->db->where('id',$id);
        $stud = $this->db->from('user_list')->get();
        $data['faculty_details'] = $stud->row();

        $this->load->view('admin/header',$data);
        $this->load->view('admin/editfaculty');
        $this->load->view('admin/footer');
    }
    public function facultyupdate(){
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['dob'] = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('dob'))));

        $this->db->where('id',$this->input->post('facid'));

        $config['upload_path']          = './images/user';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 200;
        $config['overwrite']            = true;
        $config['file_ext_tolower']     = true;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('image')){
            $data['image']=$this->upload->data('file_name');
        }

        $this->db->update('user_list',$data);

        redirect(base_url().'instadmin/editfaculty/'.$this->input->post('facid'));
    }
    public function addfaculties(){
        

        if($_FILES['faculty-list']["error"] === 0){
            $h = fopen($_FILES['faculty-list']['tmp_name'], "r");
            $row_cnt = 0;
            $col_array = array();
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
            {
                if($row_cnt === 0){
                    $col_array = $data;
                }else{
                    $student_det_array = array();
                    $colcnt = 0;
                    foreach ($col_array as $key_element) {
                        $student_det_array[trim($key_element)] = $data[$colcnt];
                        $colcnt++;
                    }
                    $faculty_add_data = array();
                    $faculty_add_data['name'] = addslashes($student_det_array['faculty_name']);
                    $faculty_add_data['email'] = trim($student_det_array['faculty_email']);
                    $faculty_add_data['dob'] = date('Y-m-d',strtotime(str_replace('/', '-', $student_det_array['faculty_dob'])));
                    $faculty_add_data['password'] =  md5($student_det_array['faculty_dob']);
                    $faculty_add_data['phone'] = trim($student_det_array['faculty_phone']);
                    $faculty_add_data['address'] = addslashes($student_det_array['address']);
                    $faculty_add_data['regn_number'] = $student_det_array['faculty_regn_number'];
                    $faculty_add_data['institute'] = $this->loggedinuser->user['institute'];
                    $faculty_add_data['status'] = 'faculty';

                    if(!$this->coursemodel->isuserexist($this->loggedinuser->user['institute'],$faculty_add_data['regn_number'])){
                        $check = $this->db->insert('user_list', $faculty_add_data);
                    }
                }
                $row_cnt++;
            }

        }

        redirect('/instadmin/faculty/', 'location');
    }
    public function addfaculty(){
        $msg = '';
        $err = true;
        $returnvar = array();
        $faculty_add_data = array();
        $faculty_add_data['name'] = $this->input->post('name');
        $faculty_add_data['email'] = $this->input->post('email');
        $faculty_add_data['dob'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->input->post('dob'))));
        $faculty_add_data['password'] =  md5(date('d/m/Y',strtotime(str_replace('/', '-', $this->input->post('dob')))));
        $faculty_add_data['phone'] = $this->input->post('phone');
        $faculty_add_data['address'] = addslashes($this->input->post('address'));
        $faculty_add_data['regn_number'] = $this->input->post('regn-number');
        $faculty_add_data['institute'] = $this->loggedinuser->user['institute'];
        $faculty_add_data['status'] = 'faculty';
        $faculty_add_data['last_login']=date('Y-m-d',strtotime('today'));

        $check1 = $this->db->insert('user_list', $faculty_add_data);

        if($check1){
            $msg .= 'Successfully added faculty details <br>';
            $err = false;
        }else{
            $msg .= 'Failed to add Faculty details <br>';
            $err = true;
        }


        $returnvar['status'] = ($err)
            ? 'danger'
            : 'success';
        $returnvar['message'] = $msg;

        echo json_encode($returnvar);
    }

    //institute and all its related functions
    public function institute(){
        $data['breadtext'] = 'Institute Profile';
        $this->load->view('admin/header',$data);
        $this->load->view('admin/institute');
        $this->load->view('admin/footer');
    }
    public function instkeysubmit(){
        $ret = array(
            'type'=>'fail'
        );
        if($this->input->post('instkey')){
            $instkey = $this->input->post('instkey');

            //existing link check
            $this->db->where('instkey',$instkey);
            $existkey = $this->db->from('institute_list')->get();
            if($existkey->num_rows() === 0){
                $ret = array(
                    'type'=>'success'
                );
            }
        }
        echo json_encode($ret);
    }

    public function questionget(){
        $chap_id = $this->input->post('chap_id');
        $this->load->model('questionmodel');
        $questions = $this->questionmodel->get_questions($chap_id);
        
        // echo "<pre>";
        // print_r ($questions);
        // echo "</pre>";
        // die;
        
        echo '<table class="table table-hover">';
        echo '<tr><th>Sl no.</th><th>Question</th><th>Options</th></tr>';
        foreach ($questions as $question) {
            echo '<tr><td>'.$question['id'].'</td>';
            echo '<td>'.$question['question'].'</td>';
            echo '<td><a href="/instadmin/viewquestion/'.$question['id'].'"><button class="btn btn-sm btn-round btn-primary btn-fab course-edit-btn" title="View Question"><span class="fa fa-eye"></span></button></a>';
            echo '</td></tr>';
        }
        echo '</table>';
    }
    public function instkeysave(){
        $ret = array(
            'type'=>'fail'
        );
        if($this->input->post('instkey')){
            $instkey = $this->input->post('instkey');

            //existing link check
            $this->db->where('instkey',$instkey);
            $existkey = $this->db->from('institute_list')->get();
            if($existkey->num_rows() === 0){
                $this->db->where('id',$this->loggedinuser->institute['id']);
                $this->db->update('institute_list',array('instkey'=>$instkey));
                $ret = array(
                    'type'=>'success'
                );
            }
        }
        echo json_encode($ret);
    }
    function institutesubmit(){
        $data['name'] = $this->input->post('name');
        $data['tagline'] = $this->input->post('tagline');
        $data['website'] = $this->input->post('website');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');

        $this->db->where('id',$this->loggedinuser->institute['id']);

        if(!empty($_FILES['image']['name'])){
            $config['upload_path']          = './images/institutes';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = true;
            $config['file_ext_tolower']     = true;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image')){
                $data['logo']=$this->upload->data('file_name');
            }
        }

        $this->db->update('institute_list',$data);
        redirect(base_url().'instadmin/institute');

    }
    
}
