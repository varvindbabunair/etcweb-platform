<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Password extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['breadtext'] = 'Update Password';
        $userprofile = $this->loggedinuser->user['status'];
        $this->load->view($userprofile.'/header',$data);
        $this->load->view('profile/password');
        $this->load->view('student/footer');
    }
}
