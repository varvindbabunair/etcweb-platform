<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Loggedinuser extends CI_Model{
    public $user;
    public $cookie;
    public $institute;

    public function __construct() {
        parent::__construct();
        $this->cookie = get_cookie('etcwebuser');
        $this->user = array();
        $this->db->where('cookie', $this->cookie);
        $query = $this->db->from('user_list')->get();
        if($query->num_rows() != 0){
            $us = $query->result_array();
            $this->user = $us[0];
            $user_det = $us[0];
            $newcookie = md5($user_det['id'].time());
            $data = array(
                'cookie' => $newcookie,
                'last_login' => date('Y-m-d')
            );
            $this->db->where('id', $user_det['id']);
            $this->db->update('user_list',$data);
            set_cookie('etcwebuser', $newcookie,'360000');

            $this->db->where('id',$us[0]['institute']);
            $qry = $this->db->from('institute_list')->get();
            $inst = $qry->result_array();
            $this->institute = $inst[0];
        }

        
    }
    
    public function details() {
        return $this->user;
    }
    public function instkey() {
        $this->db->where(array('id'=>$this->user['institute']));
        $qry = $this->db->from('institute_list')->get();
        $result = $qry->result_array();
        return $result[0]['instkey'];
    }
    
    public function isLoggedin() {
        if(count($this->user) != 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function creditprice() {
        $this->db->where(array('name'=>'price'));
        $qry = $this->db->from('credits_meta')->get();
        $result = $qry->result_array();
        return $result[0]['value'];
    }
    public function creditworth() {
        $this->db->where(array('name'=>'free-credits'));
        $qry = $this->db->from('credits_meta')->get();
        $result = $qry->result_array();
        return $result[0]['value'] * $this->creditprice();
    }
}