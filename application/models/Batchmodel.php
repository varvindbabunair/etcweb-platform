<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Batchmodel extends CI_Model{
    
    public function fetch_course($institute, $param) {
        if($param === 'all'){
            $this->db->where(array(
                'institute' =>  $institute
                    ));
        }elseif ($param === 'active') {
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end >'=> date('Y-m-d')
                    ));
        }elseif ($param === 'inactive') {
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end <'=> date('Y-m-d')
                    ));
        }else{
            $this->db->where(array(
                'institute' =>  0
                    ));
        }
        $this->db->order_by('id', 'desc');
        $get = $this->db->from('course_list')->get();
        return $get->result_array();
    }
    
    public function get_course_count($institute, $param){
        if($param === 'all'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array(); 
            return $arr['total'];  
        }elseif($param === 'active'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end >'=> date('Y-m-d')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array(); 
            return $arr['total'];  
        }elseif($param === 'inactive'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end <'=> date('Y-m-d')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array(); 
            return $arr['total'];  
        }
    }
}