<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Classmodel extends CI_Model{
    public function selectClass()
    {
        $class_list = $this->db->get('class_list');
        return $class_list->result_array();
    }
}
    