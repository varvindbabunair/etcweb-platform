<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Syllabusmodel extends CI_Model {
    public $physics = array(1,5);
    public $chemistry = array(2,6);
    public $biology = array(3,8);
    public $maths = array(4,9);

    public function class11physics(){
    	$this->db->where(array(
    		'subject' => 1
    	));
    	$this->db->from('chapter_list');
		$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class12physics(){
    	$this->db->where(array(
    		'subject' => 5
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class11chemistry(){
    	$this->db->where(array(
    		'subject' => 2
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class12chemistry(){
    	$this->db->where(array(
    		'subject' => 6
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class11biology(){
    	$this->db->where(array(
    		'subject' => 3
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class12biology(){
    	$this->db->where(array(
    		'subject' => 8
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class11maths(){
    	$this->db->where(array(
    		'subject' => 4
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function class12maths(){
    	$this->db->where(array(
    		'subject' => 9
    	));
    	$this->db->from('chapter_list');
    	$qry = $this->db->get();
    	return $qry->result_array();
    }

    public function classNameOfChapter($chapterId){
        $this->db->select('class_list.title');
        $this->db->from('chapter_list');
        $this->db->join('subject_list','chapter_list.subject = subject_list.id');
        $this->db->join('class_list','class_list.id = subject_list.class');
        $this->db->where('chapter_list.id',$chapterId);
        $qry = $this->db->get();
        $ret = $qry->result_array();
        return $ret[0]['title'];
    }

    public function subjectNameOfChapter($chapterId){
        $this->db->select('subject_list.title');
        $this->db->from('chapter_list');
        $this->db->join('subject_list','chapter_list.subject = subject_list.id');
        $this->db->where('chapter_list.id',$chapterId);
        $qry = $this->db->get();
        $ret = $qry->result_array();
        return $ret[0]['title'];
    }

    public function chapterName($chapterId){
        $this->db->where('id',$chapterId);
        $this->db->select('title');
        $qry = $this->db->from('chapter_list')->get();
        $ret = $qry->result_array();
        return $ret[0]['title'];
	}
	
	public function syllabus($user){
		$this->db->where('id', $user);
		$this->db->select('syllabus');
		$query = $this->db->get('course_list');
		return $query->result_array();
	}
}
