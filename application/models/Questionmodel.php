<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Questionmodel extends CI_Model{
    public function get_questions($chap_id)
    {
        $question_list = $this->db->get_where('question_list', array('chapter' => $chap_id));
        return $question_list->result_array();
    }
}