<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Coursemodel extends CI_Model{

    public function fetch_course($institute, $param) {
        if($param === 'all'){
            $this->db->where(array(
                'institute' =>  $institute
                    ));
        }elseif ($param === 'active') {
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end >'=> date('Y-m-d')
                    ));
        }elseif ($param === 'inactive') {
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end <'=> date('Y-m-d')
                    ));
        }else{
            $this->db->where(array(
                'institute' =>  0
                    ));
        }
        $this->db->order_by('id', 'desc');
        $get = $this->db->from('course_list')->get();
        return $get->result_array();
    }

    public function get_course($institute,$id){
        $this->db->where(array(
                'institute' =>  $institute,
                'id'=> $id ));
        $get = $this->db->from('course_list')->get();
        $rslt = $get->result_array();
        return $rslt[0];
    }

    public function get_batches($institute,$course){
        $this->db->where(array(
                'institute' =>  $institute,
                'course'=> $course ));
        $get = $this->db->from('batch_list')->get();
        $rslt = $get->result_array();
        return $rslt;
    }

    public function get_batch($institute,$id){
        $this->db->where(array(
                'institute' =>  $institute,
                'id'=> $id ));
        $get = $this->db->from('batch_list')->get();
        $rslt = $get->row_array();
        return $rslt;
    }

    public function get_batches_in_test($testid){
        $this->db->select(array('test_batch_list.batch','batch_list.batch_name'));
        $this->db->from('test_batch_list');
        $this->db->join('batch_list','test_batch_list.batch = batch_list.id');
        $this->db->where('test',$testid);
        $qry = $this->db->get();
        $batches = $qry->result_array();
        $ret = array();
        $cnt = 0;
        foreach($batches as $batch){
            $ret['id'][$cnt] = $batch['batch'];
            $ret['name'][$cnt] = $batch['batch_name'];
            $cnt++;
        }
        return $ret;
    }

    public function get_students($institute,$course,$batch){
        if($batch == 'all'){
            $this->db->where(array(
                'institute' =>  $institute,
                'status' =>  'student',
                'regn_number !=' =>  '',
                'regn_number NOT LIKE' =>  'p%',
                'course'=> $course ));
        }else{
            $this->db->where(array(
                'institute' =>  $institute,
                'status' =>  'student',
                'regn_number !=' =>  '',
                'regn_number NOT LIKE' =>  'p%',
                'course'=> $course,
                'batch' => $batch ));
        }
        $get = $this->db->from('user_list')->get();
        $rslt = $get->result_array();
        return $rslt;
    }

    public function isuserexist($institute,$regnumber){
        $this->db->where(array('regn_number'=>$regnumber,'institute'=>$institute));
        $get = $this->db->from('user_list')->get();
        if($get->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_ranklist($testid){

        $this->db->where(array('id'=>$testid));
        $query = $this->db->from('test_list')->get();
        $ans   = $query->row_array();
        $test_details = $ans;

        $questions = json_decode($test_details['questions']);

        $this->db->select(array('id','correct_answer'));
        $this->db->from('question_list');

        // $qn_qry = "SELECT id,correct_answer FROM question_list WHERE";

        $negative_mark_of = array();
        $positive_mark_of = array();
        $section_num = 0;
        foreach ($questions as $section){
            $individual_answer_num =0;
            foreach ($section as $qn){
                // $qn_id = $questions[$section_num][$individual_answer_num][0];
                $qn_id = $qn[0];
                // $qn_qry .= " id = '$qn_id' OR";
                $this->db->or_where('id', $qn_id);
                $individual_answer_num++;

                $positive_mark_of[$qn[0]] = $qn[1];
                $negative_mark_of[$qn[0]] = $qn[2];
            }
            $section_num++;
        }
        // $qn_qry = trim($qn_qry,' OR');

        $question_get_query = $this->db->get();
        $qn_details = $question_get_query->result_array();

        $correct_choice_of = array();
        foreach ($qn_details as $qns) {
            $correct_choice_of[$qns['id']] = $qns['correct_answer'];
        }


        $this->db->where(array('test'=>$testid));
        $test_get_query = $this->db->from('answer_list')->get();
        $test_attended = $test_get_query->result_array();

        $student_marks = array();
        $student_sort = array();
        $stud_cnt = 0;
        foreach ($test_attended as $test_response) {
            $student_response = json_decode($test_response['answer']);

            $positive_marks = 0;
            $negative_marks = 0;


            $section_num = 0;
            foreach ($student_response as $section_answer){
                $individual_answer_num =0;
                foreach ($section_answer as $individual_answer){

                    if($correct_choice_of[$individual_answer[0]] == $individual_answer[1]){

                        $positive_marks = $positive_marks + (int)$positive_mark_of[$individual_answer[0]];

                    }else if($individual_answer[1] == ""){
                        $positive_marks = $positive_marks + 0;
                    }
                    else if($correct_choice_of[$individual_answer[0]] != $individual_answer[1]){
                        $negative_marks = $negative_marks + (int)$negative_mark_of[$individual_answer[0]];
                    }
                    $individual_answer_num++;
                }
                $section_num++;
            }

            

//            $student_marks[$stud_cnt]['id'] = $test_response['user'];
            $this->db->where('id',$test_response['user']);
            $qry = $this->db->from('user_list')->get();
            $stud = $qry->result_array();
            foreach($stud[0] as $key=>$value){
                $student_marks[$stud_cnt][$key] = $value;
            }
            $student_marks[$stud_cnt]['positive_marks'] = $positive_marks;
            $student_marks[$stud_cnt]['negative_marks'] = $negative_marks;
            $student_marks[$stud_cnt]['total_marks'] = $positive_marks-$negative_marks;
            $student_sort[$stud[0]['id']] = $positive_marks-$negative_marks;
            $stud_cnt++;
        }

        //sorting
        $temp=array();
        for($i=0;$i<count($student_marks);$i++){
            $temp = $student_marks[$i];
            for($j=$i+1; $j<count($student_marks); $j++){
                if($student_marks[$j]['total_marks'] > $temp['total_marks']){
                    $student_marks[$i] = $student_marks[$j];
                    $student_marks[$j] = $temp;
                    $temp = $student_marks[$i];
                }
            }
        }
        return $student_marks;
    }

    public function get_course_count($institute, $param){
        if($param === 'all'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'active'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end >'=> date('Y-m-d')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'inactive'){
            $this->db->select('count(*) as total');
            $this->db->from('course_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'course_end <'=> date('Y-m-d')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }
    }

    public function get_student_count($institute, $param){
        if($param === 'all'){
            $this->db->select('count(*) as total');
            $this->db->from('user_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'status'    =>  'student'
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'active'){
            $this->db->select('count(*) as total');
            $this->db->from('user_list');
            $this->db->join('course_list', 'course_list.institute = user_list.institute');
            $this->db->where(array(
                'user_list.institute' =>  $institute,
                'user_list.status' => 'student',
                'course_list.course_end >'=> date('Y-m-d')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'inactive'){
            $this->db->select('count(*) as total');
            $this->db->from('user_list');
            $this->db->join('course_list', 'course_list.institute = user_list.institute');
            $this->db->where(array(
                'user_list.institute' =>  $institute,
                'user_list.status' => 'student',
                'course_list.course_end <'=> date('Y-m-d')
            ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }
    }

    public function get_test($institute, $course=null, $batch=null, $param=null){
        if($param === 'active'){
            $this->db->select('test_list.*, test_batch_list.batch');
            $this->db->from('test_list');
            $this->db->join('test_batch_list','test_batch_list.test = test_list.id');
            $this->db->where(array(
                'test_list.institute' =>  $institute,
                'test_list.course' =>  $course,
                'test_list.exam_entry >' => date('Y-m-d H:i:s'),
                'test_batch_list.batch' => $batch
                    ));
            $query = $this->db->get();
            return $query->result_array();
        }elseif($param === 'inactive'){
            $this->db->select('test_list.*, test_batch_list.batch');
            $this->db->from('test_list');
            $this->db->join('test_batch_list','test_batch_list.test = test_list.id');
            $this->db->where(array(
                'test_list.institute' =>  $institute,
                'test_list.course' =>  $course,
                'test_list.exam_entry <' => date('Y-m-d H:i:s'),
                'test_batch_list.batch' => $batch
                    ));
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            return $query->result_array();

        }elseif(is_numeric($param)){
            $this->db->from('test_list');
            $this->db->where(array(
                'id' =>  $param
            ));
            $query = $this->db->get();
            $ret_array = $query->result_array();
            return $ret_array[0];
        }
    }

    public function get_unattended_test($user,$institute, $course, $batch){
        $this->db->from('answer_list');
        $this->db->where('user',$user);
        $ans_get = $this->db->get();
        $answer_list = $ans_get->result_array();

        $this->db->select('test_list.*');
        $this->db->from('test_list');
        $this->db->join('test_batch_list','test_batch_list.test = test_list.id');
        foreach($answer_list as $ansid){
            $this->db->where('test_list.id !=',$ansid['test']);
        }
        $this->db->where(array(
            'test_list.course' => $course,
            'test_list.institute' => $institute,
            'test_list.exam_entry >' => date('Y-m-d H:i:s'),
            'test_batch_list.batch' => $batch
        ));
        $unattended_query = $this->db->get();
        return $unattended_query->result_array();
    }

    public function get_attended_test($user){
        $this->db->from('answer_list');
        $this->db->where('user',$user);
        $this->db->join('test_list','answer_list.test = test_list.id');
        $this->db->select('answer_list.id, answer_list.test, answer_list.test_time, answer_list.answer, test_list.title');
        $return = $this->db->get();
        return $return->result_array();
    }

    public function get_institute_test($institute, $param){
        if($param === 'active'){
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry >'=> date('Y-m-d H:i:s')
                    ));
            $query = $this->db->get();
            return $query->result_array();
        }elseif($param === 'inactive'){
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry <'=> date('Y-m-d H:i:s')
                    ));
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            return $query->result_array();

        }elseif($param === 'today'){
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry >'=> date('Y-m-d '.'00:00:00'),
                'exam_entry <'=> date('Y-m-d '.'23:59:59')
            ));
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            return $query->result_array();

        }elseif($param === 'ongoing'){
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry <'=> date('Y-m-d '.'00:00:00'),
                'exam_entry >'=> date('Y-m-d '.'23:59:59')
            ));
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            return $query->result_array();

        }elseif($param === 'upcoming'){
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry >'=> date('Y-m-d H:i:s')
            ));
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            return $query->result_array();

        }
    }

    public function get_test_status($testId){
        $status = array();
        //total students

        $this->db->select('count(*) as total');
        $this->db->from('user_list');
        $this->db->join('test_batch_list','test_batch_list.batch = user_list.batch');
        $this->db->where(array(
            'test_batch_list.test' =>  $testId,
            'status' => 'student'
        ));
        $query = $this->db->get();
        $arr   = $query->row_array();
        $status['total'] = $arr['total'];

        //attended students
        $this->db->select('count(*) as total');
        $this->db->from('answer_list');
        $this->db->where('test',$testId);
        $query = $this->db->get();
        $arr   = $query->row_array();
        $status['attended'] = $arr['total'];

        return $status;
    }

    public function get_test_count($institute, $param){
        if($param === 'all'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'active'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry <'=> date('Y-m-d H:i:s')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif($param === 'inactive'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry <'=> date('Y-m-d H:i:s')
                    ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif ($param === 'today'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_entry <'=> date('Y-m-d '.'00:00:00'),
                'exam_entry >'=> date('Y-m-d '.'23:59:59'),
            ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif ($param === 'ongoing'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_start <'=> date('Y-m-d H:i:s'),
                'exam_entry >'=> date('Y-m-d H:i:s'),
            ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }elseif ($param === 'upcoming'){
            $this->db->select('count(*) as total');
            $this->db->from('test_list');
            $this->db->where(array(
                'institute' =>  $institute,
                'exam_start >'=> date('Y-m-d H:i:s'),
            ));
            $query = $this->db->get();
            $arr   = $query->row_array();
            return $arr['total'];
        }
    }

    public function get_faculty($institute,$param){
        if($param == 'active'){
            $this->db->where(array(
                'institute' =>  $institute,
                'status' =>  'faculty',
                'regn_number !=' =>  '',
                'last_login !='=> NULL ));
        }else if($param == 'inactive'){
            $this->db->where(array(
                'institute' =>  $institute,
                'status' =>  'faculty',
                'regn_number !=' =>  '',
                'last_login'=> NULL ));
        }
        $get = $this->db->from('user_list')->get();
        $rslt = $get->result_array();
        return $rslt;
    }

    public function get_faculty_count($institute,$param){
        return sizeof($this->get_faculty($institute,$param));
    }

    public function get_admins($institute){
        $this->db->where(array(
            'institute' =>  $institute,
            'status' =>  'admin',
            'regn_number !=' =>  '',
            'last_login'=> NULL ));
        $get = $this->db->from('user_list')->get();
        $rslt = $get->result_array();
        return $rslt;
    }

    public function get_test_details($testid){
        $this->db->where('id',$testid);
        $qry = $this->db->from('test_list')->get();
        $res = $qry->result_array();
        return $res[0];
    }

    public function get_subjects($class_id){
        $this->db->where(array(
                'class'=> $class_id));
        $get = $this->db->from('subject_list')->get();
        $rslt = $get->result_array();
        // var_dump($rslt);die;
        return $rslt;
    }

    public function get_chapters($subject_id){
        $this->db->where(array(
                'subject'=> $subject_id));
        $get = $this->db->from('chapter_list')->get();
        $rslt = $get->result_array();
        // var_dump($rslt);die;
        return $rslt;
    }

}
