<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Userlogin extends CI_Model{
    public $user;
    
    public function login($login,$password){
        $this->db->where(array('regn_number'=>'','phone'=>$login,'password'=>$password, 'last_login !=' => NULL));
        $query = $this->db->from('user_list')->get();
        if($query->num_rows() == 1){
            $this->user = $query->row_array();
            $newcookie = md5($this->user['id'].time());
            $data = array(
                'cookie' => $newcookie
            );
            $this->db->where('id',$this->user['id']);
            $this->db->update('user_list',$data);
            setcookie('etcwebuser',$newcookie,time() +2592000,'/');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function instlogin($instid,$login,$password){
        $this->db->where(array('regn_number'=>$login,'password'=>$password,'institute'=>$instid));
        $query = $this->db->from('user_list')->get();
        if($query->num_rows() == 1){
            $this->user = $query->row_array();
            $newcookie = md5($this->user['id'].time());
            $data = array(
                'cookie' => $newcookie
            );
            $this->db->where('id',$this->user['id']);
            $this->db->update('user_list',$data);
            setcookie('etcwebuser',$newcookie,time() +2592000,'/');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    
}