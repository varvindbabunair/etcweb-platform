$(document).ready(function(){
    $('.chemistry, .physics, .maths, .biology').hide();
    $('.course-select').change(function(){
        var syllabus = $(this).children('option:selected').attr('syllabus');
        if(syllabus == 'eng'){
            $('.chemistry, .physics, .maths').show();
            $('.biology').hide();
        }else if(syllabus == 'medical'){
            $('.chemistry, .physics, .biology').show();
            $('.maths').hide();
        }else if(syllabus == 'both'){
            $('.chemistry, .physics, .maths, .biology').show();
        }
        $.ajax({
           type: 'POST',
           url: '/instadmin/batchcheck',
           data: {
               course_id : $(this).val(),
               token : $('.token').val()
           },
           success: function(data){
               $('.batch-list').html(data);
           },
           error: function(data){
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
    });
    
    $('.chemistry-class').change(function(){
        if($(this).val() == 'all'){
            $('.chem11, .chem12').show();
            if($('.chem11-chapter').prop('checked')){
                $('.chem-qn-no').val(30);
            }
        }else if($(this).val() == '11'){
            $('.chem11').show();
            $('.chem12').hide();
        }else if($(this).val() == '12'){
            $('.chem12').show();
            $('.chem11').hide();
        }else if($(this).val() == 'none'){
            $('.chem11, .chem12').hide();
        }
    });
    
    $('.physics-class').change(function(){
        if($(this).val() == 'all'){
            $('.phy11, .phy12').show();
            if($('.phy11-chapter').prop('checked')){
                $('.phy-qn-no').val(30);
            }
        }else if($(this).val() == '11'){
            $('.phy11').show();
            $('.phy12').hide();
        }else if($(this).val() == '12'){
            $('.phy12').show();
            $('.phy11').hide();
        }else if($(this).val() == 'none'){
            $('.phy11, .phy12').hide();
            $('.phy-qn-no').val(0);
        }
    });
    
    $('.biology-class').change(function(){
        if($(this).val() == 'all'){
            $('.bio11, .bio12').show();
            if($('.bio11-chapter').prop('checked')){
                $('.bio-qn-no').val(30);
            }
        }else if($(this).val() == '11'){
            $('.bio11').show();
            $('.bio12').hide();
        }else if($(this).val() == '12'){
            $('.bio12').show();
            $('.bio11').hide();
        }else if($(this).val() == 'none'){
            $('.bio11, .bio12').hide();
        }
    });
    
    $('.maths-class').change(function(){
        if($(this).val() == 'all'){
            $('.maths11, .maths12').show();
            if($('.maths11-chapter').prop('checked')){
                $('.maths-qn-no').val(30);
            }
        }else if($(this).val() == '11'){
            $('.maths11').show();
            $('.maths12').hide();
        }else if($(this).val() == '12'){
            $('.maths12').show();
            $('.maths11').hide();
        }else if($(this).val() == 'none'){
            $('.maths11, .maths12').hide();
        }
    });
    
    $('.phy11-check').change(function(){
        if ($(this).prop('checked')) {
            $('.phy11-chapter').prop('checked',true);
            if($('.phy-qn-no').val() == 0){
                $('.phy-qn-no').val(30);
            }
        }else{
            $('.phy11-chapter').prop('checked',false);
            $('.phy-qn-no').val(0);
        }
    });
    $('.phy11-chapter').change(function(){
        if($('.phy11-chapter').prop('checked')){
            if($('.phy-qn-no').val() == 0){
                $('.phy-qn-no').val(30);
            }
        }else{
            $('.phy-qn-no').val(0);
        }
    });
    
    $('.phy12-check').change(function(){
        if ($(this).prop('checked')) {
            $('.phy12-chapter').prop('checked',true);
            if($('.phy-qn-no').val() == 0){
                $('.phy-qn-no').val(30);
            }
        }else{
            $('.phy12-chapter').prop('checked',false);
            $('.phy-qn-no').val(0);
        }
    });
    
    $('.chem11-check').change(function(){
        if ($(this).prop('checked')) {
            $('.chem11-chapter').prop('checked',true);
            if($('.chem-qn-no').val() == 0){
                $('.chem-qn-no').val(30);
            }
        }else{
            $('.chem11-chapter').prop('checked',false);
            $('.chem-qn-no').val(0);
        }
    });
    $('.chem11-chapter').change(function(){
        if($('.chem11-chapter').prop('checked')){
            if($('.chem-qn-no').val() == 0){
                $('.chem-qn-no').val(30);
            }
        }else{
            $('.chem-qn-no').val(0);
        }
    });
    
    $('.chem12-check').change(function(){
        if ($(this).prop('checked')) {
            $('.chem12-chapter').prop('checked',true);
            if($('.chem-qn-no').val() == 0){
                $('.chem-qn-no').val(30);
            }
        }else{
            $('.chem12-chapter').prop('checked',false);
            $('.chem-qn-no').val(0);
        }
    });
    
    $('.bio11-check').change(function(){
        if ($(this).prop('checked')) {
            $('.bio11-chapter').prop('checked',true);
            if($('.bio-qn-no').val() == 0){
                $('.bio-qn-no').val(30);
            }
        }else{
            $('.bio11-chapter').prop('checked',false);
            $('.bio-qn-no').val(0);
        }
    });
    $('.bio11-chapter').change(function(){
        if($('.bio11-chapter').prop('checked')){
            if($('.bio-qn-no').val() == 0){
                $('.bio-qn-no').val(30);
            }
        }else{
            $('.bio-qn-no').val(0);
        }
    });
    
    $('.bio12-check').change(function(){
        if ($(this).prop('checked')) {
            $('.bio12-chapter').prop('checked',true);
            if($('.bio-qn-no').val() == 0){
                $('.bio-qn-no').val(30);
            }
        }else{
            $('.bio12-chapter').prop('checked',false);
            $('.bio-qn-no').val(0);
        }
    });
    
    $('.maths11-check').change(function(){
        if ($(this).prop('checked')) {
            $('.maths11-chapter').prop('checked',true);
            if($('.maths-qn-no').val() == 0){
                $('.maths-qn-no').val(30);
            }
        }else{
            $('.maths11-chapter').prop('checked',false);
            $('.maths-qn-no').val(0);
        }
    });
    $('.maths11-chapter').change(function(){
        if($('.maths11-chapter').prop('checked')){
            if($('.maths-qn-no').val() == 0){
                $('.maths-qn-no').val(30);
            }
        }else{
            $('.maths-qn-no').val(0);
        }
    });
    
    $('.maths12-check').change(function(){
        if ($(this).prop('checked')) {
            $('.maths12-chapter').prop('checked',true);
            if($('.maths-qn-no').val() == 0){
                $('.maths-qn-no').val(30);
            }
        }else{
            $('.maths12-chapter').prop('checked',false);
            $('.maths-qn-no').val(0);
        }
    });
    
    
    
    $('.exam-start').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        format: 'DD/MM/YYYY hh:mm:ss A',
        sideBySide: true,
        date:new Date(),
        minDate: new Date()
    });
    $('.exam-end').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        format: 'DD/MM/YYYY hh:mm:ss A',
        sideBySide: true,
        date:moment(new Date).add(30,'minutes')
    });
    
    $('.exam-start').on('dp.change', function (e) {
        $('.exam-end').data('DateTimePicker').minDate(e.date.add(30,'minutes'));
    });
    
    //test create button
    $('.test-create-btn').click(function(){
        $('.global-message').html('Please Wait... Creating Test...');
        $('.message-loader').show();
        var err = 0;
        //beginning the data acquisition and verification steps
        var course = $('.course-select').val();
        if(course == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please select a course');
            return;
        }
        var testname = $('.testname').val();
        if(testname == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please provide a name for the test');
            return;
        }

        var difficulty = $('.exam-difficulty').val();
        if(difficulty ==''){
            showNotification('top','center','danger', 'Please select difficulty of test to be conducted');
            return;
        }
        
        var batches = '';
        $('.batch-check').each(function() {
            if($(this).prop('checked')){
                batches = batches + $(this).val() + ',';
            }
        });
        if(batches == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please select batches for whom exam has to be conducted');
            return;
        }
        
        var examstart = $('.exam-start').val();
        var examend = $('.exam-end').val();
        var examduration = $('.exam-duration').val();
        
        if(examstart == '' || examend =='' || examduration == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please provide time details for the exam.');
            return;
        }
        
        var syllabus = $('.course-select').children('option:selected').attr('syllabus');
        
        var sections = [];
        if(syllabus == 'medical'){
            if($('.phy-qn-no').val() != '0' && $('.phy-qn-no').val() != '' ){
                sections.push('Physics');
            }
            if($('.chem-qn-no').val() != '0' && $('.chem-qn-no').val() != '' ){
                sections.push('Chemistry');
            }
            if($('.bio-qn-no').val() != '0' && $('.bio-qn-no').val() != '' ){
                sections.push('Biology');
            }
        }else if(syllabus == 'eng'){
            if($('.phy-qn-no').val() != '0' && $('.phy-qn-no').val() != '' ){
                sections.push('Physics');
            }
            if($('.chem-qn-no').val() != '0' && $('.chem-qn-no').val() != '' ){
                sections.push('Chemistry');
            }
            if($('.maths-qn-no').val() != '0' && $('.maths-qn-no').val() != '' ){
                sections.push('Mathematics');
            }
        }else if(syllabus == 'both'){
            if($('.phy-qn-no').val() != '0' && $('.phy-qn-no').val() != '' ){
                sections.push('Physics');
            }
            if($('.chem-qn-no').val() != '0' && $('.chem-qn-no').val() != '' ){
                sections.push('Chemistry');
            }
            if($('.bio-qn-no').val() != '0' && $('.bio-qn-no').val() != '' ){
                sections.push('Biology');
            }
            if($('.maths-qn-no').val() != '0' && $('.maths-qn-no').val() != '' ){
                sections.push('Mathematics');
            }
        }
        
        var section_val = JSON.stringify(sections);
        if(section_val == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please provide the number of questions in Physics, Chemistry and Biology.');
            return;
        }
        
        var chapters = [];
        $.each(sections,function(i,section){
            
            var chap = [];
            if(section == 'Physics'){
                $('.phy11-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
                $('.phy12-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
            }
            if(section == 'Chemistry'){
                $('.chem11-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
                $('.chem12-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
            }
            if(section == 'Biology'){
                $('.bio11-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
                $('.bio12-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
            }
            if(section == 'Mathematics'){
                $('.maths11-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
                $('.maths12-chapter').each(function() {
                    if($(this).prop('checked')){
                        chap.push($(this).val());
                    }
                });
            }
            chapters.push(chap);
        });
        
        var chapter_val = JSON.stringify(chapters);
        if(chapter_val == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please select the appropriate chapters needed for the test.');
            return;
        }
        
        var correctmark = $('.correct-mark').val();
        var negativemark = $('.negative-mark').val();
        
        if(correctmark == ''|| correctmark < 0 || negativemark == ''|| negativemark < 0 ){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please specify the marks to be given for each correct and wrong answer');
            return;
        }
        
        $.ajax({
           type: 'POST',
           url: '/instadmin/testcreate',
           data: {
               token : $('.token').val(),
               course : course,
               batches : batches,
               testname : testname,
               examstart : examstart,
               examentry : examend,
               examduration : examduration,
               sections : section_val,
               chapters : chapter_val,
               correctmark : correctmark,
               negativemark : negativemark,
               phyqnno : $('.phy-qn-no').val(),
               chemqnno : $('.chem-qn-no').val(),
               bioqnno : $('.bio-qn-no').val(),
               mathsqnno : $('.maths-qn-no').val(),
               difficulty : difficulty,
               institute: $('.institute').val()
           },
           success: function(data){
                $('.message-loader').hide();
                if(data == 'success'){
                    
                    showNotification('top','center','success', 'Successfully created test '+testname);
                }else{
                    // showNotification('top','center','success', data);
                    showNotification('top','center','danger', 'Some error occured while creating the test '+testname);
                }
                setTimeout(function(){
                    window.location('/instadmin/tests');
                }, 2000);
           },
           error: function(data){
                $('.message-loader').hide();
                showNotification('top','center','danger', 'Some Error Occured.');
           }
        });
        
        
    });
        
});

function showNotification(from, align,type,message) {
    types = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
    $.notify({
      icon: "add_alert",
      message: message
    }, {
      type: type,
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }