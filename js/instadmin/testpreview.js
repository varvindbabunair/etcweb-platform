$(document).ready(function(){
    $('body').on('click', '.page-item', function (){
        $('.page-item').removeClass('active');
        $(this).addClass('active');
        var pageid = $(this).attr('qn-page');
        $('.question-content').hide();
        $('.qn-' + pageid).show();
    });

    $('.chapteropt').change(function(){
        viewQuestions($(this).val());
    });

    $('.qn-replace').click(function(){
        var qnToReplace = $('.replace-qn').val();
        var qnNew = $('.question-content:visible').attr('id');

        $('td.replace-'+qnToReplace).html($('div.qnreplace-'+qnNew).html());

        if($('.qns-final').val().split("['"+qnNew+"','4','1']").length > 1){
            alert('Question already exists')
        }else{
            var str = $('.qns-final').val().replace("['"+qnToReplace+"','4','1']","['"+qnNew+"','4','1']");
            $('.qns-final').val(str);
            $('#questionModal').modal('hide');
        }

    });

    $('.course-select').change(function(){
        var syllabus = $(this).children('option:selected').attr('syllabus');
        if(syllabus == 'eng'){
            $('.chemistry, .physics, .maths').show();
            $('.biology').hide();
        }else if(syllabus == 'medical'){
            $('.chemistry, .physics, .biology').show();
            $('.maths').hide();
        }else if(syllabus == 'both'){
            $('.chemistry, .physics, .maths, .biology').show();
        }
        $.ajax({
            type: 'POST',
            url: '/instadmin/batchcheck',
            data: {
                course_id : $(this).val(),
                token : $('.token').val()
            },
            success: function(data){
                $('.batch-list').html(data);
            },
            error: function(data){
                showNotification('top','center','danger', 'Some Error Occured.');
            }
        });
    });

    //test create button
    $('.test-update-btn').click(function(){
        $('.global-message').html('Please Wait... Updating Test...');
        $('.message-loader').show();
        var err = 0;
        //beginning the data acquisition and verification steps
        var testid = $('.testid').val();
        var course = $('.course-select').val();
        if(course == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please select a course');
            return;
        }
        var testname = $('.testname').val();
        if(testname == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please provide a name for the test');
            return;
        }

        var batches = '';
        $('.batch-check').each(function() {
            if($(this).prop('checked')){
                batches = batches + $(this).val() + ',';
            }
        });
        if(batches == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please select batches for whom exam has to be conducted');
            return;
        }

        var examstart = $('.exam-start').val();
        var examend = $('.exam-end').val();
        var examduration = $('.exam-duration').val();

        if(examstart == '' || examend =='' || examduration == ''){
            $('.message-loader').hide();
            showNotification('top','center','danger', 'Please provide time details for the exam.');
            return;
        }

        var questions = $('.qns-final').val();


        $.ajax({
            type: 'POST',
            url: '/instadmin/testupdate',
            data: {
                token : $('.token').val(),
                course : course,
                batches : batches,
                testname : testname,
                examstart : examstart,
                examentry : examend,
                examduration : examduration,
                questions: questions,
                testid: testid
            },
            success: function(data){
                $('.message-loader').hide();
                var datas = jQuery.parseJSON(data);
                showNotification('top','center',datas['type'], datas['message']);

            },
            error: function(data){
                $('.message-loader').hide();
                showNotification('top','center','danger', 'Some Error Occured.');
            }
        });


    });


});

function viewQuestions(chapter){
    $.ajax({
        type: 'POST',
        url: '/instadmin/questionsfrom',
        data: {
            'chapter': chapter
        },
        success: function(result){
            $('.question-modal-content').html(result);
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        },
        error:function(){
            $('.question-modal-content').html('<h3 class="" style="text-align: center;">' +
                '<span class="fa fa-warning"></span> Failed to load Questions</h3>' +
                '<p style="text-align:center">Some error occured and the system failed to load the questions. ' +
                'Please contact the etcweb customer support if the problem persists</p>');
        }
    });
}

function viewChapters(subject,question){
    $.ajax({
        type: 'POST',
        url: '/instadmin/chaptersfrom',
        data: {
            'subject': subject
        },
        beforeSend: function(){
            $('#questionModal').modal();
            $('.question-modal-title').html('Questions from '+subject);
            $('.question-modal-content').html('');
        },
        success: function(result){
            $('.chapteropt').html(result);
        },
        error:function(){
            $('.question-modal-content').html('<h3 class="" style="text-align: center;">' +
                '<span class="fa fa-warning"></span> Failed to load Questions</h3>' +
                '<p style="text-align:center">Some error occured and the system failed to load the questions. ' +
                'Please contact the etcweb customer support if the problem persists</p>');
        }
    });
    $('.replace-qn').val(question);
}
