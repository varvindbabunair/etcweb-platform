$(document).ready(function(){

    $('#course-list-form').submit(function(e){
        e.preventDefault();
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           success: function(data){
               $('.batch-content').html(data);
           },
           error: function(data){
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
    });
    $('.course-list').change(function(){
      $('#course-id').val($(this).val());
      $('#course-list-form').submit();
    });

    $('#batch-get-form').submit(function(e){
        e.preventDefault();
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           success: function(data){
               $('div.batch-list').html(data);
               $('div.stud-fetch-btn').show();

           },
           error: function(data){
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
    });
    $('.batch-fetch').change(function(){
        $('#bth-course-id').val($(this).val());
        $('#batch-get-form').submit();
    });


    $('#course-modal-list-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data){
                $('.modal-batch-content').html(data);
            },
            error: function(data){
                showNotification('top','center','danger', 'Some Error Occured.');
            }
        });
    });
    $('.course-modal-list').change(function(){
        $('#course-modal-id').val($(this).val());
        $('#course-modal-list-form').submit();
    });

    $('#student-get-form').submit(function(e){
        e.preventDefault();
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           success: function(data){
               $('div.stud-list').html(data);
           },
           error: function(data){
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
    });
    
    $('.add-indvidual-student').click(function(e){
        e.preventDefault();
        $.ajax({
           type: 'POST',
           url: $(this).parent('div').parent('form').attr('action'),
           data: $(this).parent('div').parent('form').serialize(),
           success: function(data){
               $('#studAddModal').modal('hide');
               var returndata = JSON.parse(data);
               showNotification('top','center',returndata['status'], returndata['message']);
               $('#addIndStudent')[0].reset();
           },
           error: function(data){
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
    });

    $('#passresetform').submit(function(e){
        e.preventDefault();
        var pass = $('.pass').val();
        var cnfpass = $('.cnf-pass').val();
        if(pass === cnfpass && pass != '' && cnfpass != '' ){
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function(data){
                    $('.global-message').html('Resetting Password');
                    $('.message-loader').show();
                },
                success: function(data){
                    var datas = jQuery.parseJSON(data);
                    $('.message-loader').hide();
                    showNotification('top','center',datas['type'], datas['message']);
                    $('#studpassreset').modal('hide');
                },
                error: function(data){
                    $('.message-loader').hide();
                    showNotification('top','center','danger', 'Some Error Occured while resetting password.');
                    $('#studpassreset').modal('hide');
                }
            });
        }else{
            showNotification('top','center','danger', 'Password and Confirm Password do not match .');
        }
    });
    
});

function showNotification(from, align,type,message) {
    types = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
    $.notify({
      icon: "add_alert",
      message: message
    }, {
      type: type,
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }

function studResetPass(studid, studname){
    $('.studmodalname').html(studname);
    $('.studid').val(studid);
    $('#studpassreset').modal();
}