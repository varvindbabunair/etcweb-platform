$('document').ready(function(){
    $('.course-start').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        format: 'DD/MM/YYYY',
        date:new Date()
    });
    $('.course-end').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        },
        format: 'DD/MM/YYYY',
        date:moment(new Date).add(1,'year')
    });
    
    $('.course-start').on('dp.change', function (e) {
        $('.course-end').data('DateTimePicker').minDate(e.date.add(1,'days'));
    });
        
    
    $('#coursesubmitform').submit(function(e){
        e.preventDefault();
        
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           beforeSend: function(data){
               $('.global-message').html('Adding Course...');
               $('.message-loader').show();
           },
           success: function(data){
               var datas = jQuery.parseJSON(data);
               $('.message-loader').hide();
               showNotification('top','center',datas['type'], datas['message']);
               if(datas['type'] == 'success'){
                   $('.course-number').html(parseInt($('.course-number').html())+1);
                   $('.active-list').html('<tr><td>'+ $('.course-name').val() +'</td><td></td><td></td><td><button class="btn btn-sm btn-round btn-primary btn-fab reload" title="Reload for Options"><span class="fa fa-refresh"></span></button></td></tr>' + $('.active-list').html());
                   $('.course-name').val('');
                   $('.course-syllabus').val('');
                   $('.batch-list').val('');
               }
           },
           error: function(data){
               $('.message-loader').hide();
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
        
    });

    $('#courseviewform').submit(function(e){
        e.preventDefault();
        
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           beforeSend: function(data){
               $('.global-message').html('Fetching Course Details...');
               $('.message-loader').show();
           },
           success: function(data){
               $('.message-loader').hide();
               $('#courseViewModal').modal();
               $('.course-view-modal-content').html(data);
           },
           error: function(data){
               $('.message-loader').hide();
               showNotification('top','center','danger', 'Some Error Occured.');
           }
         });
        
    });
    
    $('body').on('click', '.reload', function (){
        location.reload();
    });
    
    $('.tagsinput').tagsinput();
    
});

function showNotification(from, align,type,message) {
    types = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
    $.notify({
      icon: "add_alert",
      message: message
    }, {
      type: type,
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }