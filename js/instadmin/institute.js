$(document).ready(function(){
    $('.newinstkey').keyup(function () {
        var instkey = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/instadmin/instkeysubmit',
            data: {
                instkey: instkey
            },
            beforeSend: function(data){
                $('.submit-msg').html('<span class="fa fa-spin fa-spinner"></span>Checking link validity');
            },
            success: function(data){
                var datas = jQuery.parseJSON(data);
                if(datas['type'] === 'success'){
                    $('.submit-msg').html('<green>Update Link to save changes</green>');
                    $('.inst-key').html(instkey);
                }else{
                    $('.submit-msg').html('<red>Link Unavailiable</red>');
                }
            },
            error: function(data){
                $('.message-loader').hide();
                showNotification('top','center','danger', 'Some Error Occured while resetting your password.');
            }
        });
    });

    $('.instkeyupdate').click(function(){
        var instkey = $('.newinstkey').val();
        $.ajax({
            type: 'POST',
            url: '/instadmin/instkeysubmit',
            data: {
                instkey: instkey
            },
            beforeSend: function(data){
                $('.submit-msg').html('<span class="fa fa-spin fa-spinner"></span>Checking link validity');
            },
            success: function(data){
                var datas = jQuery.parseJSON(data);
                if(datas['type'] === 'success'){
                    $.ajax({
                        type: 'POST',
                        url: '/instadmin/instkeysave',
                        data: {
                            instkey: instkey
                        },
                        success: function(data){
                            var datas = jQuery.parseJSON(data);
                            if(datas['type'] === 'success'){
                                $('.submit-msg').html('<green>Successfully changed the link</green>');
                            }else{
                                $('.submit-msg').html('<red>Save Failed. Link Unavailiable</red>');
                            }
                        },
                        error: function(data){
                            $('.message-loader').hide();
                            showNotification('top','center','danger', 'Some Error Occured while resetting your password.');
                        }
                    });
                }else{
                    $('.submit-msg').html('<red>Link Unavailiable</red>');
                }
            },
            error: function(data){
                $('.message-loader').hide();
                showNotification('top','center','danger', 'Some Error Occured while resetting your password.');
            }
        });
    });
});