$(document).ready(function(){
    $('form').submit(function(e){
        e.preventDefault();
        var pass = $('.pass').val();
        var cnfpass = $('.cnf-pass').val();
        if(pass === cnfpass && pass != '' && cnfpass != '' ){
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function(data){
                    $('.global-message').html('Resetting Password');
                    $('.message-loader').show();
                },
                success: function(data){
                    var datas = jQuery.parseJSON(data);
                    $('.message-loader').hide();
                    showNotification('top','center',datas['type'], datas['message']);

                },
                error: function(data){
                    $('.message-loader').hide();
                    showNotification('top','center','danger', 'Some Error Occured while resetting your password.');
                }
            });
        }else{
            showNotification('top','center','danger', 'Password and Confirm Password do not match .');
        }
    });
});

function showNotification(from, align,type,message) {
    types = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
    $.notify({
        icon: "add_alert",
        message: message
    }, {
        type: type,
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}