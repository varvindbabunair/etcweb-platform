$(document).ready(function(){

  $('#sub-list-form').submit(function(e){
    e.preventDefault();
    $.ajax({
       type: 'POST',
       url: $(this).attr('action'),
       data: $(this).serialize(),
       success: function(data){
           $('div.subject-list').html(data);
       },
       error: function(data){
           showNotification('top','center','danger', 'Some Error Occured.');
       }
     });
  });

  $('.sub-fetch').change(function(){
    $('#bth-sub-id').val($(this).val());
    $('#sub-list-form').submit();
  });
});

$(document).ajaxSuccess(function() {
  $('#chap-list-form').submit(function(e){
    e.preventDefault();
    $.ajax({
       type: 'POST',
       url: $(this).attr('action'),
       data: $(this).serialize(),
       success: function(data){
           $('div.chapter-list').html(data);
           $('div.question-fetch-btn').show();

       },
       error: function(data){
           showNotification('top','center','danger', 'Some Error Occured.');
       }
     });
  });

  $('.chap-fetch').change(function(){
    $('#bth-chap-id').val($(this).val());
    $('#chap-list-form').submit();
  });

  $('#question-get-form').submit(function(e){
    e.preventDefault();
    $.ajax({
       type: 'POST',
       url: $(this).attr('action'),
       data: $(this).serialize(),
       success: function(data){
           $('div.question-list').html(data);
       },
       error: function(data){
           showNotification('top','center','danger', 'Some Error Occured.');
       }
     });
  });
});