$('document').ready(function(){
 $('#sub-list-form').submit(function(e){
        e.preventDefault();
        $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(data){
                    $('div.subject-list').html(data);
                },
                error: function(data){
                    showNotification('top','center','danger', 'Some Error Occured.');
                }
        });
    });

    $('.sub-fetch').change(function(){
        $('#bth-sub-id').val($(this).val());
        $('#sub-list-form').submit();
    });
  
  
    //  $(document).ajaxSuccess(function() {
    $('#chap-list-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data){
                $('div.chapter-list').html(data);
                $('div.syllabus-fetch-btn').show();
    
            },
            error: function(data){
                showNotification('top','center','danger', 'Some Error Occured.');
            }
        });
    });
  
    $(document.body).on('change', '.chap-fetch' ,function(){
    //   $('.chap-fetch').change(function(){
        $('#bth-chap-id').val($(this).val());
        $('#chap-list-form').submit();
    });
    
    $('#synopsis-get-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data){
                $('div.syllabus-list').html(data);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            },
            error: function(data){
                showNotification('top','center','danger', 'Some Error Occured.');
            }
        });
    });
  
  $(document.body).on('change', '#chapter_id' ,function(){
    // $('.syllabus-fetch').click(function(){
        $('#synopsis-get-form').submit();
    });


});

// function viewSynopsys(id){
// 	$.ajax({
//       type: 'POST',
//       url: '/student/synopsys',
//       data: $('#synopsys-form-'+id).serialize(),
//       beforeSend: function(data){
//       		$('#syllabusModal').modal();
//           $('.syllabus-modal-title').html('Synopsys');
//           $('.syllabus-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>');
//       },
//       success: function(result){
//           $('.syllabus-modal-content').html(result);
//           MathJax.Hub.Queue(["Typeset", MathJax.Hub]);

//       },
//       error:function(){
// 	    	$('.syllabus-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Synopsys</h3><p style="text-align:center">Some error occured and the system failed to load the synopsys. Please contact the etcweb customer support if the problem persists</p>');
// 	    }
//      });
// }

// function viewQuestions(id){
// 	$.ajax({
//       type: 'POST',
//       url: '/instadmin/questions',
//       data: $('#question-form-'+id).serialize(),
//       beforeSend: function(data){
//       		$('#questionModal').modal();
//           $('.question-modal-title').html('Questions');
//           $('.question-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>');
//       },
//       success: function(result){
//           $('.question-modal-content').html(result);
//           MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
//       },
//       error:function(){
// 	    	$('.question-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Questions</h3><p style="text-align:center">Some error occured and the system failed to load the questions. Please contact the etcweb customer support if the problem persists</p>');
// 	    }
//      });
// }
