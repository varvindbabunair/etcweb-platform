$(document).ready(function(){
	$('form').submit(function(e){
		e.preventDefault();
	    $.ajax({
	        url: $(this).attr('action'),
	        type:'POST',
	        data:$(this).serialize(),
	        beforeSend:function(){
	        	$('#myModal').modal();
	        },
	        success:function(result,status){
	            $('.modal-body').html(result);
	            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

	            var s1 = [$('.accuracy').attr('value')];
 
               var plot3 = $.jqplot('accuracy',[s1],{
                   seriesDefaults: {
                       renderer: $.jqplot.MeterGaugeRenderer,
                       rendererOptions: {
                           min: 0,
                           max: 100,
                           intervals:[20, 40, 60, 80],
                           intervalColors:['#cc6666','#E7E658','#93b75f','#66cc66']
                       }
                   }
               });

               plot3.replot();
	        },
	        error:function(){
	            $('.modal-body').html('<p><span class="fa fa-warning"></span>Some Error occured... Please try Later</p>');
	        }
	    });
	});
});