$('document').ready(function(){
	if($('.syllabus-select').val() == ''){
		$('.syllabus-container').hide();
	}else{
		$('.syllabus-container').show();
	}

	$('.syllabus-select').change(function(){
		if($('.syllabus-select').val() != ''){
			$('.syllabus-container').show();
			if($('.syllabus-select').val() == 'eng'){
				$('.selected-syllabus').html('Engineering');
			}else if ($('.syllabus-select').val()=='med') {
				$('.selected-syllabus').html('Medical');
			}else if ($('.syllabus-select').val()=='both') {
				$('.selected-syllabus').html('Engineering and Medical');
			}
			$('form').submit();
		}else{
			$('.syllabus-container').hide();
		}
	});

	$('form').submit(function(e){
        e.preventDefault();
        
        $.ajax({
           type: 'POST',
           url: $(this).attr('action'),
           data: $(this).serialize(),
           beforeSend: function(data){
               $('.syllabus-content-display').html('<h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>');
           },
           success: function(result){
               $('.syllabus-content-display').html(result);
               MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
           },
           error:function(){
		    	$('.syllabus-content-display').html('<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Syllabus</h3><p style="text-align:center">Some error occured and the system failed to load the syllabus. Please contact the etcweb customer support if the problem persists</p>');
		    }
         });
        
    });

    $('body').on('click', '.page-item', function (){
        $('.page-item').removeClass('active');
        $(this).addClass('active');
        var pageid = $(this).attr('qn-page');
        $('.question-content').hide();
        $('.qn-' + pageid).show();
    });

    


});

function viewSynopsys(id){
	$.ajax({
       type: 'POST',
       url: '/student/synopsys',
       data: $('#synopsys-form-'+id).serialize(),
       beforeSend: function(data){
       		$('#syllabusModal').modal();
           $('.syllabus-modal-title').html('Synopsys');
           $('.syllabus-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>');
       },
       success: function(result){
           $('.syllabus-modal-content').html(result);
           MathJax.Hub.Queue(["Typeset", MathJax.Hub]);

       },
       error:function(){
	    	$('.syllabus-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Synopsys</h3><p style="text-align:center">Some error occured and the system failed to load the synopsys. Please contact the etcweb customer support if the problem persists</p>');
	    }
     });
}

function viewQuestions(id){
	$.ajax({
       type: 'POST',
       url: '/instadmin/questions',
       data: $('#question-form-'+id).serialize(),
       beforeSend: function(data){
       		$('#questionModal').modal();
           $('.question-modal-title').html('Questions');
           $('.question-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</h3>');
       },
       success: function(result){
           $('.question-modal-content').html(result);
           MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
       },
       error:function(){
	    	$('.question-modal-content').html('<h3 class="" style="text-align: center;"><span class="fa fa-warning"></span> Failed to load Questions</h3><p style="text-align:center">Some error occured and the system failed to load the questions. Please contact the etcweb customer support if the problem persists</p>');
	    }
     });
}
