
$('document').ready(function(){
    $('.message-loader').hide();
    
    $('a').click(function(evt){

        if($(this).attr('href').charAt(0) != '#' && !evt.ctrlKey && !evt.shiftKey  && $(this).attr('target') != '_blank' ){
            $('.global-message').html('');
            $('.message-loader').show();
        }

    });

    $(window).on('pageshow', function(){
        $('.message-loader').hide();
    });

});

$('document').load(function(){
	$('.message-loader').show();
});
